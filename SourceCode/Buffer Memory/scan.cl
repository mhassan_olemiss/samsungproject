
#define WG_SIZE 64
#define HALF_WG_SIZE (WG_SIZE/2)
__kernel
void scan(__global int *input, 
          __global int *output, 
                     int  rows,
                     int  cols) {

    // Each work group is responsible for scanning a row

    // If we add additional elements to local memory (half the
    // work group size) and fill them with zeros, we don't need
    // conditionals in the code to check for out-of-bounds
    // accesses
    __local float lData[WG_SIZE+HALF_WG_SIZE];
    __local float lData2[WG_SIZE+HALF_WG_SIZE];

    int myGlobalY = get_global_id(1);
    int myLocalX = get_local_id(0);
	int getglobal=get_global_id(0);
    // Initialize out-of-bounds elements with zeros
    if(myLocalX < HALF_WG_SIZE) {
        lData[myLocalX] = 0.0f;
        lData2[myLocalX] = 0.0f;
    }


/*if(getglobal==1 && myGlobalY==0)
{printf("kernel1 : %d\n", input[4]);
printf("kernel0 : %d\n", input[0]);}*/
    
    // Start past out-of-bounds elements
    myLocalX += HALF_WG_SIZE;
    
    // This value needs to be added to the local data.  It's the highest
    // value from the prefix scan of the previous elements in the row
    float prevMaxVal = 0;

    // March down a row WG_SIZE elements at a time
    int iterations = cols/WG_SIZE;
    if(cols % WG_SIZE != 0) 
    {
        // If cols are not an exact multiple of WG_SIZE, then we need to do
        // one more iteration 
        iterations++;
    }
   
    for(int i = 0; i < iterations; i++) {
                
        int columnOffset = i*WG_SIZE + get_local_id(0);

        // Don't do anything if a thread's index is past the end of the row.  We still need
        // the thread available for memory barriers though.
        if(columnOffset < cols) {
            // Cache the input data in local memory
            lData[myLocalX] = input[myGlobalY*cols + columnOffset];
        }
        else {
            // Otherwise just store zeros 
            lData[myLocalX] = 0.0f;
        }
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        // 1
        lData2[myLocalX] = lData[myLocalX] + lData[myLocalX-1];
        barrier(CLK_LOCAL_MEM_FENCE);
        
        // 2
        lData[myLocalX] = lData2[myLocalX] + lData2[myLocalX-2];
        barrier(CLK_LOCAL_MEM_FENCE);
 
        // 4
        lData2[myLocalX] = lData[myLocalX] + lData[myLocalX-4];
        barrier(CLK_LOCAL_MEM_FENCE);
 
        // 8
        lData[myLocalX] = lData2[myLocalX] + lData2[myLocalX-8];
        barrier(CLK_LOCAL_MEM_FENCE);
 
        // 16
        lData2[myLocalX] = lData[myLocalX] + lData[myLocalX-16];
        barrier(CLK_LOCAL_MEM_FENCE);
 
        // 32
        lData[myLocalX] = lData2[myLocalX] + lData2[myLocalX-32];
        barrier(CLK_LOCAL_MEM_FENCE);
 
        // Write data out to global memory
        if(columnOffset < cols) {
            output[myGlobalY*cols + columnOffset] = lData[myLocalX] + prevMaxVal;
        }
        
        // Copy the value from the highest thread in the group to my local index
        prevMaxVal += lData[WG_SIZE+HALF_WG_SIZE-1];
    }
}


__kernel
void transpose(__global int* iImage,
               __global int* oImage,
                        int    inRows,
                        int    inCols) {
                        
    __local float tmpBuffer[256];

    // Work groups will perform the transpose (i.e., an entire work group will be moved from
    // one part of the image to another) 
    int myWgInX = get_group_id(0);
    int myWgInY = get_group_id(1);

    // The local index of a thread should not change (threads with adjacent IDs 
    // within a work group should always perform adjacent memory accesses).  We will 
    // account for the clever indexing of local memory later.
    int myLocalX = get_local_id(0);
    int myLocalY = get_local_id(1);
    
    int myGlobalInX = myWgInX*16 + myLocalX;
    int myGlobalInY = myWgInY*16 + myLocalY;

    // Don't read out of bounds
    if(myGlobalInX < inCols && myGlobalInY < inRows) {
        
        // Cache data to local memory (coalesced reads)
        tmpBuffer[myLocalY*16 + myLocalX] = iImage[myGlobalInY*inCols + myGlobalInX];

    }

    barrier(CLK_LOCAL_MEM_FENCE);
    
    // This avoids some confusion with dimensions, but otherwise aren't needed
    int outRows = inCols;
    int outCols = inRows;

    // Swap work group IDs for their location after the transpose
    int myWgOutX = myWgInY;
    int myWgOutY = myWgInX;

    int myGlobalOutX = myWgOutX*16 + myLocalX;
    int myGlobalOutY = myWgOutY*16 + myLocalY;

    // When writing back to global memory, we need to swap image dimensions (for the
    // transposed size), and also need to swap thread X/Y indices in local memory
    // (to achieve coalesced memory writes)

    // Don't write out of bounds
    if(myGlobalOutX >= 0 && myGlobalOutX < outCols && 
       myGlobalOutY >= 0 && myGlobalOutY < outRows) {

       // The read from tmpBuffer is going to conflict, but the write should be coalesced
       oImage[myGlobalOutY*outCols + myGlobalOutX] = tmpBuffer[myLocalX*16 + myLocalY]; 
    }

    return;
} 
