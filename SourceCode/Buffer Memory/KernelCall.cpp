/*
 * KernelCall.cc
 *
 *  Created on: Jan 9, 2014
 *      Author: mainul
 */
#include "precomp.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <sys/time.h>
#include <sys/types.h>
#include <string>

#include <stdio.h>
#include <stdio.h>

#include "opencv2/ocl/ocl.hpp"
#include <iostream>
using namespace std;

using namespace cv::ocl;
using namespace cv;

cl_context cxGPUContext;        // OpenCL context
cl_command_queue cqCommandQueue;// OpenCL command que
cl_platform_id cpPlatform;      // OpenCL platform
cl_device_id cdDevice;          // OpenCL device
cl_program cpProgram;           // OpenCL program
cl_kernel ckKernel;             // OpenCL kernel
cl_mem cmDevSrcA;               // OpenCL device source buffer A
cl_mem cmDevSrcB;               // OpenCL device source buffer B
cl_mem cmDevDst;                // OpenCL device destination buffer
size_t szGlobalWorkSize;        // 1D var for Total # of work items
size_t szLocalWorkSize;		// 1D var for # of work items in the work group
size_t szParmDataBytes;		// Byte size of context information
size_t szKernelLength;		// Byte size of kernel code
cl_int cerror, ciErr2,ciErr1;		// Error code var
char* cPathAndName = NULL;      // var for full paths to data, src, etc.
char* cSourceCL = NULL;         // Buffer to hold source for compilation


unsigned int roundUp(unsigned int value, unsigned int multiple);
int convertToString(char *filename, std::string& s)
{
    size_t size;
    char*  str;

    std::fstream f(filename, (std::fstream::in | std::fstream::binary));

    if(f.is_open())
    {
        size_t fileSize;
        f.seekg(0, std::fstream::end);
        size = fileSize = (size_t)f.tellg();
        f.seekg(0, std::fstream::beg);

        str = new char[size+1];
        if(!str)
        {
            f.close();
            return 1;
        }

        f.read(str, fileSize);
        f.close();
        str[size] = '\0';

        s = str;
        delete[] str;
        return 0;
    }
    printf("Error: Failed to open file %s\n", filename);
    return 1;
}





void ExecuteScanKernel( Mat& img,string kernelsrc,string kernelName,cv::ocl::Context* clCxt)
{

	cl_int cerror;
	int* scan_src_buffer;

	/*std::stringstream strFname4;
	strFname4 << "Src_image.txt";
					const char* filenames2=strFname4.str().c_str();
					FILE *fp2 = fopen(filenames2, "w");

					fprintf(fp2,"Columnn %2d Rows: %d\n: ", img.cols,img.rows);
					for (int i = 0; i < img.rows; i++)
					{
						fprintf(fp2,"\nrow %2d: ", i);
						for (int j = 0; j < img.cols; j++)
						{
							fprintf(fp2," %5d ", img.at<uchar>(i,j));
						}
					}
					fclose(fp2);
*/

	scan_src_buffer=(int*)malloc(sizeof(int)*img.cols*img.rows);

	int* scan_output_buffer;
	scan_output_buffer=(int*)malloc(sizeof(int)*img.cols*img.rows);

	for (int i = 0; i < img.rows; i++)
	{

		for (int j = 0; j < img.cols; j++)
		{
			scan_src_buffer[j+i*img.cols]= img.at<uchar>(i,j);
			//cout<<"source : "<<srcHost.at<uchar>(i,j)<<endl;
			//printf(" %5d ", srcHost.at<uchar>(i,j));
			//printf(" %5d ", scan_src_buffer[j+i*srcHost.cols]);
			//cout<<"scan_source: "<<scan_src_buffer[j+i*img.cols]<<endl;
		}
	}
	ciErr1=clGetPlatformIDs(1, &cpPlatform, NULL);
	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clGetPlatformID, Line %u in file %s !!!\n\n", __LINE__, __FILE__);

	}


	ciErr1 = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &cdDevice, NULL);
	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clGetDeviceIDs, Line %u in file %s !!!\n\n", __LINE__, __FILE__);

	}
	else
	printf("*** Got device\n");
	//printf("*** Got device*****************************************\n");
	//printf("************** %d********************************\n",cdDevice);

	cxGPUContext = clCreateContext(0, 1, &cdDevice, NULL, NULL, &ciErr1);
	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clCreateContext, Line %u in file %s !!!\n\n", __LINE__, __FILE__);

	}
	else
	printf("*** Got context\n");



	cqCommandQueue = clCreateCommandQueue(cxGPUContext, cdDevice,  CL_QUEUE_PROFILING_ENABLE, &ciErr1);
	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clCreateCommandQueue, Line %u in file %s !!!\n\n", __LINE__, __FILE__);

	}
	else
	printf("*** Got commandqueue\n");


	cl_mem scan_source  = clCreateBuffer(cxGPUContext,CL_MEM_READ_WRITE,sizeof(int)*img.rows*img.cols,NULL,&cerror);
	if (cerror != CL_SUCCESS) {
		printf("Error in creating buffers, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	}
	cl_mem scan_output  = clCreateBuffer(cxGPUContext,CL_MEM_READ_WRITE,sizeof(int)*img.rows*img.cols,NULL,&cerror);
	if (cerror != CL_SUCCESS) {
		printf("Error in creating buffers, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	}

	cl_mem scan_output_xpose  = clCreateBuffer(cxGPUContext,CL_MEM_READ_WRITE,sizeof(int)*img.rows*img.cols,NULL,&cerror);
	if (cerror != CL_SUCCESS) {
			printf("Error in creating buffers, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

		}
	//cl_mem scan_output  = ocl::openCLCreateBuffer(clCxt,CL_MEM_READ_WRITE,sizeof(int)*img.rows*img.cols);


	cerror = clEnqueueWriteBuffer(cqCommandQueue, scan_source, CL_TRUE, 0, sizeof(int)*img.rows*img.cols, scan_src_buffer, 0, NULL, NULL);
	//cerror = clEnqueueWriteBuffer(qu, scan_output, CL_TRUE, 0, sizeof(int)*src.rows*src.cols, scan_output_buffer, 0, NULL, NULL);

	if (cerror != CL_SUCCESS) {
		printf("Error in creating buffers, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	}


	std::string  sourceStr;
		ciErr1 = convertToString("scan.cl", sourceStr);
		if(ciErr1 != CL_SUCCESS) {
			printf("Error in convertToString, Line %u in file %s !!!\n\n", __LINE__, __FILE__);

		}
		const char * source    = sourceStr.c_str();
		size_t sourceSize[]    = { strlen(source) };


	cpProgram = clCreateProgramWithSource(cxGPUContext, 1, &source, sourceSize, &ciErr1);
	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clCreateProgramWithSource, Line %u in file %s %d !!!\n\n", __LINE__, __FILE__,ciErr1);

	}
	else
	{		printf("*** Got createprogramwithsource\n");

	//cout<<source<<endl;

//	(clCxt->impl->devices[clCxt->impl->devnum])
	ciErr1 =  clBuildProgram(cpProgram, 1, &cdDevice, NULL, NULL, NULL);
		if (ciErr1 != CL_SUCCESS) {
				printf("Error in building, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,ciErr1);

			}

	}
	printf("*** Got buildprogram\n");

	//cout<<kernelName<<endl;

	ckKernel = clCreateKernel(cpProgram, "scan", &ciErr1);

	if (ciErr1 != CL_SUCCESS) {
		printf("Error in clCreateKernel, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,ciErr1);

	}
	//cout<<"All successful!!"<<endl;
	cerror = clSetKernelArg(ckKernel, 0, sizeof(cl_mem), (void*)&scan_source);
	cerror |= clSetKernelArg(ckKernel, 1, sizeof(cl_mem), (void*)&scan_output);

	cerror |= clSetKernelArg(ckKernel, 2, sizeof(cl_int), (void*)&img.rows);
	cerror |= clSetKernelArg(ckKernel, 3, sizeof(cl_int), (void*)&img.cols);



	if (cerror != CL_SUCCESS) {
		printf("Error in creating args, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	}

	size_t localWorkSize1[2]={64, 1};
	size_t globalWorkSize1[2]={64, img.rows};
	cl_event event = NULL;
	//clFinish(cqCommandQueue);

	static double total_execute_time = 0;
	static double total_kernel_time = 0;
	int i = 0;
	//for(i = 0; i < 100; i++)
	{


		//	double execute_time = 0;
		//	 total_time   = 0;
		cerror = clEnqueueNDRangeKernel(cqCommandQueue, ckKernel,2, NULL, globalWorkSize1, localWorkSize1, 0, NULL, &event);
		 if (cerror != CL_SUCCESS) {
			    		    	printf("Error executing kernel, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

			    		    }
		clWaitForEvents(1 , &event);

		cl_ulong time_start, time_end,queue_time;
		double total_time;
		double execute_time = 0;

		clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
		clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
		clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED,sizeof(queue_time), &queue_time, 0);

		execute_time = (double)(time_end - time_start)/(1000.0*1000.0);
		total_time = (double)(time_end - queue_time) / (1000.0 * 1000.0);
		total_execute_time += execute_time;
		total_kernel_time += total_time;
		//printf("\nExecution time in time_end = %ld ms\n", time_end );
		//printf("\nExecution time in time_start = %ld ms\n", time_start );
		clReleaseEvent(event);


	 }
		clFlush(cqCommandQueue);
		clReleaseKernel(ckKernel);
	    cout << "average Scan kernel excute time: " << (total_execute_time / 100.0) << "ms" << endl; // "ms" << endl;
	    cout << "average Scan kernel total time:  " << (total_kernel_time / 100.0) << endl; //
		clReleaseMemObject(scan_source);
		clReleaseMemObject(scan_output);

	   /* cerror = clEnqueueReadBuffer(cqCommandQueue, scan_output, CL_TRUE, 0, sizeof(int)*img.rows*img.cols, scan_output_buffer, 0, NULL, NULL);
	    if (cerror != CL_SUCCESS) {
	    	printf("Error reading buffer, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	    }
	    std::stringstream strFname1;
	    strFname1 << "Scan_result_Native.txt";
	    const char* filename1=strFname1.str().c_str();
	    FILE *fp1 = fopen(filename1, "w");


	    for (int i = 0; i < img.rows; i++)
	    {
	    	fprintf(fp1,"\nrow %2d: ", i);
	    	for (int j = 0; j < img.cols; j++)
	    	{
	    		//scan_src_buffer[j+i*srcHost.cols]= srcHost.at<uchar>(i,j);
	    		//cout<<"source : "<<srcHost.at<uchar>(i,j)<<endl;
	    		//printf(" %5d ", srcHost.at<uchar>(i,j));
	    		fprintf(fp1," %5d ", scan_output_buffer[j+i*img.cols]);
	    		//cout<<"scan_source: "<<scan_src_buffer[j+i*srcHost.cols]<<endl;
	    	}
	    }

	   /* for (int i = 0; i < img.rows*img.cols; i++)
	    	    {

	    	    		//scan_src_buffer[j+i*srcHost.cols]= srcHost.at<uchar>(i,j);
	    	    		//cout<<"source : "<<srcHost.at<uchar>(i,j)<<endl;
	    	    		//printf(" %5d ", srcHost.at<uchar>(i,j));
	    	    		fprintf(fp1," %5d ", scan_output_buffer[i]);
	    	    		//cout<<"scan_source: "<<scan_src_buffer[j+i*srcHost.cols]<<endl;

	    	    }*/

	   // fclose(fp1);



	    //________________execute Transpose Kernel_________________________//

		/*ckKernel = clCreateKernel(cpProgram, "transpose", &ciErr1);

		if (ciErr1 != CL_SUCCESS) {
			printf("Error in clCreateKernel, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,ciErr1);

		}

	    cerror = clSetKernelArg(ckKernel, 0, sizeof(cl_mem), (void*)&scan_output);
	    cerror = clSetKernelArg(ckKernel, 1, sizeof(cl_mem), (void*)&scan_output_xpose);

	    cerror |= clSetKernelArg(ckKernel, 2, sizeof(cl_int), (void*)&img.rows);
	    cerror |= clSetKernelArg(ckKernel, 3, sizeof(cl_int), (void*)&img.cols);




	    if (cerror != CL_SUCCESS) {
	    	printf("Error in creating args, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	    }
	    size_t localWorkSize2[]={16, 16};
	    size_t globalWorkSize2[]={roundUp(img.cols,16), roundUp(img.rows,16)};


	    event = NULL;
	    clFinish(cqCommandQueue);

	    total_execute_time = 0;
	    total_kernel_time = 0;
	    i = 0;
	    for(i = 0; i < 100; i++)
	    {


	    	//	double execute_time = 0;
	    	//	 total_time   = 0;
	    	cerror = clEnqueueNDRangeKernel(cqCommandQueue, ckKernel,2, NULL, globalWorkSize2, localWorkSize2, 0, NULL, &event);
	    	 if (cerror != CL_SUCCESS) {
	    		    	printf("Error executing kernel, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	    		    }
	    	clWaitForEvents(1 , &event);

	    	cl_ulong time_start, time_end,queue_time;
	    	double total_time;
	    	double execute_time = 0;
	    	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	    	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
	    	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED,sizeof(queue_time), &queue_time, 0);

	    	execute_time = (double)(time_end - time_start)/(1000.0*1000.0);
	    	total_time = (double)(time_end - queue_time) / (1000.0 * 1000.0);
	    	total_execute_time += execute_time;
	    	total_kernel_time += total_time;
	    	//printf("\nExecution time in time_end = %ld ms\n", time_end );
	    	//printf("\nExecution time in time_start = %ld ms\n", time_start );
	    	clReleaseEvent(event);


	    }
	    clFlush(cqCommandQueue);
	    clReleaseKernel(ckKernel);
	    cout << "average Transpose kernel excute time: " << (total_execute_time / 100.0) << "ms" << endl; // "ms" << endl;
	    cout << "average Transpose kernel total time:  " << (total_kernel_time / 100.0) << endl; //

	    //______________print transposed output______________________//


	    cerror = clEnqueueReadBuffer(cqCommandQueue, scan_output_xpose, CL_TRUE, 0, sizeof(int)*img.rows*img.cols, scan_output_buffer, 0, NULL, NULL);
	    if (cerror != CL_SUCCESS) {
	   	    		    	printf("Error reading buffer, Line %u in file %s error NO: %d!!!\n\n", __LINE__, __FILE__,cerror);

	   	    		    }
	    std::stringstream strFname1;
	    strFname1 << "Xposed_Scan_result_Native.txt";
	    const char* filename1=strFname1.str().c_str();
	    FILE *fp1 = fopen(filename1, "w");


	    	for (int i = 0; i < img.rows; i++)
	    	{
	    		fprintf(fp1,"\nrow %2d: ", i);
	    		for (int j = 0; j < img.cols; j++)
	    		{
	    			//scan_src_buffer[j+i*srcHost.cols]= srcHost.at<uchar>(i,j);
	    			//cout<<"source : "<<srcHost.at<uchar>(i,j)<<endl;
	    			//printf(" %5d ", srcHost.at<uchar>(i,j));
	    			fprintf(fp1," %5d ", scan_output_buffer[j+i*img.cols]);
	    			//cout<<"scan_source: "<<scan_src_buffer[j+i*srcHost.cols]<<endl;
	    		}
	    	}
	    	fclose(fp1);



	    //printf("\nExecution time in milliseconds = %0.3f ms\n", (execute_time ) );
	    //	cout<<"All successful!!"<<endl;*/


}
unsigned int roundUp(unsigned int value, unsigned int multiple) {

   unsigned int remainder = value % multiple;

   // Make the value a multiple of multiple
   if(remainder != 0) {
      value += (multiple-remainder);
   }

   return value;
}

