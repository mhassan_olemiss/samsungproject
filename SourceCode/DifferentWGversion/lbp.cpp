#include "precomp.hpp" 
#include <stdio.h> 
#include <string> 
 #include "safe_call.hpp"
//#include "opencv2/imgproc.hpp" 
//#include "/opencv2/objdetect/objdetect_c.h" 
#include "/opencv-2.4.6.1/modules/ocl/include/opencv2/ocl/ocl.hpp" 
//#include "opencv2/core/utility.hpp" 
#include "opencv2/imgproc/imgproc_c.h" 
#include "opencv2/core/core_c.h" 
#include "/opencv-2.4.6.1/modules/ocl/include/opencv2/ocl/private/util.hpp" 
#include <sys/time.h> 
#include <sys/types.h> 
#include <string> 
#include <iostream> 
#include <stdio.h>
//#include "KernelCall.cpp"


using namespace cv;
using namespace std;
int64 work_begin1 = 0;
int64 work_end1 = 0;
//#define WSIZE_256
//#define WSIZE_128
//#define WSIZE_64
//#define lbpWSIZE_256
//#define lbpWSIZE_128
//#define lbpWSIZE_64
#define WSIZE_32
#define lbpWSIZE_32
//#define DEBUG_HOST_CODE
//#define DEBUG_INTEGRAL_COLS
//#define DEBUG_SCALED_SRC
//#define DEBUG_INTEGRAL_ROWS
#define clSurfKernel
//#define opencvIntegral
////////////////////////OpenCL kernel strings////////////
//extern const char *lbpobjectdetect;

const char* lbpobjectdetect="#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable\n"
		//"#define _localvariableValue_ 1000\n"
		"__kernel void helloworld(__global char* in, __global char* out)\n"
		"{\n"
		"int num = get_global_id(0);\n"
		"out[num] = in[num] + 1;\n"
		"}\n"
		//------------small local memory kernel begins here--------------------//

		//"#define NUM_BANKS_256 32\n"
		//"#define GET_CONFLICT_OFFSET256(lid) ((lid) >> LOG_NUM_BANKS_256)\n"

		"#define LSIZE 32\n"
				"#define LSIZE_LOOP_VALUE 15\n" //formula=(LSIZE>>1)-1
				"#define LSIZE_SHIFT_VALUE 4\n" //formula= 2th power value to represent LSIZE/2. e.g for LSIZE=256, LSIZE_SHIFT_VALUE=7
				"#define LSIZE_1 31\n"
				"#define LSIZE_2 30\n"
				"#define HF_LSIZE 8\n"
				"#define LOG_LSIZE 8\n"
				"#define LOG_NUM_BANKS 2\n"
				"#define NUM_BANKS 4\n"
				"#define GET_CONFLICT_OFFSET(lid) ((lid) >> LOG_NUM_BANKS)\n"

/*				"#define LSIZE 256\n"
				"#define LSIZE_LOOP_VALUE 127\n" //formula=(LSIZE>>1)-1
				"#define LSIZE_SHIFT_VALUE 7\n" //formula= 2th power value to represe
				"#define LSIZE_1 255\n"
				"#define LSIZE_2 254\n"
				"#define HF_LSIZE 128\n"
				"#define LOG_LSIZE 8\n"
				"#define LOG_NUM_BANKS 5\n"
				"#define NUM_BANKS 32\n"
				"#define GET_CONFLICT_OFFSET(lid) ((lid) >> LOG_NUM_BANKS)\n"*/

/*		"#define LSIZE 128\n"
		"#define LSIZE_LOOP_VALUE 63\n" //formula=(LSIZE>>1)-1
		"#define LSIZE_SHIFT_VALUE 6\n" //formula= 2th power value to represe
		"#define LSIZE_1 127\n"
		"#define LSIZE_2 126\n"
		"#define HF_LSIZE 64\n"
		"#define LOG_LSIZE 8\n"
		"#define LOG_NUM_BANKS 5\n"
		"#define NUM_BANKS 16\n"   //LSIZE/8
		"#define GET_CONFLICT_OFFSET(lid) ((lid) >> LOG_NUM_BANKS)\n"*/

/*		"#define LSIZE 64\n"
		"#define LSIZE_LOOP_VALUE 31\n" //formula=(LSIZE>>1)-1
		"#define LSIZE_SHIFT_VALUE 5\n" //formula= 2th power value to represe
		"#define LSIZE_1 63\n"
		"#define LSIZE_2 62\n"
		"#define HF_LSIZE 32\n"
		"#define LOG_LSIZE 8\n"
		"#define LOG_NUM_BANKS 5\n"
		"#define NUM_BANKS 8\n"   //LSIZE/8
		"#define GET_CONFLICT_OFFSET(lid) ((lid) >> LOG_NUM_BANKS)\n"*/


		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) v2_integral_cols(__global uchar *src,__global int *sum,\n"
				"int src_offset,int pre_invalid,int rows,int cols,int src_step,int dst_step)\n"
				"{\n"
				"unsigned int lid = get_local_id(0);\n"
				"unsigned int gid = get_group_id(0);\n"
				"unsigned int grp_id = get_group_id(0);\n"
				"unsigned int global_id = get_global_id(0);\n"
				//"if(lid<=1)\n"
				"{\n"
				//"printf(\" global id................................. %d \\n\",global_id);\n"
				"}\n"
				"int src_t[2], sum_t[2];\n"
				"__local int lm_sum[2][LSIZE + LOG_LSIZE];\n"
				"__local int* sum_p;\n"
				"gid = gid << 1;\n"
				"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
				"{\n"
					"src_t[0] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid, (uint)cols - 1)] : 0);\n"
					"src_t[1] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid + 1, (uint)cols - 1)] : 0);\n"
					"sum_t[0] = (i == 0 ? 0 : lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"    /////
					"sum_t[1] =  (i == 0 ? 0 : lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n" /////

					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"

					//"int bf_loc = lid ;\n"
					"lm_sum[0][bf_loc] = src_t[0];\n"
					"lm_sum[1][bf_loc] = src_t[1];\n"
					"int offset = 1;\n"


					////
					"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
					"{\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"
						"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"

						"ai += GET_CONFLICT_OFFSET(ai);\n"
						"bi += GET_CONFLICT_OFFSET(bi);\n"


						"if((lid & LSIZE_LOOP_VALUE) < d)\n" //this only works upto 16 elements
						"{\n"

							"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
//							"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//										"{\n"
//											"printf(\" first if................................. %d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]);\n"
//
//										"}\n"
						"}\n"
						"offset <<= 1;\n"
					"}\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"if(lid < 2)\n"
					"{\n"
							"lm_sum[lid][LSIZE_2 + LOG_LSIZE] = 0;\n"
//							"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//												"{\n"
//													"printf(\" second if................................. %d \\n\",lm_sum[lid][LSIZE_2 + LOG_LSIZE]);\n"
//
//												"}\n"
					"}\n"
					"for(int d = 1;  d < LSIZE; d <<= 1)\n"
					"{\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"
						"offset >>= 1;\n"
						"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
						"ai += GET_CONFLICT_OFFSET(ai);\n"
						"bi += GET_CONFLICT_OFFSET(bi);\n"
						//=======================================================
						"if((lid & LSIZE_LOOP_VALUE) < d)\n"
						"{\n"
							"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"

//							"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//								"{\n"
//									"printf(\" first if................................. %d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]);\n"
//
//								"}\n"

						"}\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"

						"if((lid & LSIZE_LOOP_VALUE) < d)\n"
						"{\n"
								"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"

//									"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//									"{\n"
//											"printf(\" second if................................. %d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][ai]);\n"
//
//									"}\n"
							"}\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"
					//=======================================================
					"}\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"int loc_s0 = gid * dst_step + i + lid - 1 - pre_invalid * dst_step , loc_s1 = loc_s0 + dst_step;\n"
					"if(lid > 0 && (i+lid) <= rows)\n"
						"{\n"
							"lm_sum[0][bf_loc] += sum_t[0];\n"  //////
							"lm_sum[1][bf_loc] += sum_t[1];\n"  //////


							//"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
							"if ((gid < cols + pre_invalid && gid >= pre_invalid))\n"
								"{\n"
									"sum[loc_s0] =lm_sum[0][bf_loc] ;\n"//sum_p[0];\n
//									"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//										"{\n"
//											"printf(\" first if................. %d \\n\",sum[loc_s0]);\n"
//
//										"}\n"
								"}\n"
							//"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
							"if(gid + 1 < cols + pre_invalid)\n"
								"{\n"
									"sum[loc_s1] =lm_sum[1][bf_loc] ;\n" //sum_p[0];\n"
//								"if(grp_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//												"{\n"
//													"printf(\" second if................. %d \\n\",sum[loc_s1]);\n"
//
//												"}\n"

								"}\n"
						"}\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"}\n"
			"}\n"

		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) v2_integral_cols_global(__global uchar *src,__global int *sum,\n"
						"int src_offset,int pre_invalid,int rows,int cols,int src_step,int dst_step,__global int* lm_sum, int groupsize)\n"
						"{\n"
						"unsigned int lid = get_local_id(0);\n"
						"unsigned int gid = get_group_id(0);\n"
						"unsigned int group_id = get_group_id(0);\n"
						"unsigned int global_id = get_global_id(0);\n"
						//"if(lid<=1)\n"
						"{\n"
						//"printf(\" global id................................. %d \\n\",global_id);\n"
						"}\n"
						"int src_t[2], sum_t[2];\n"
					"gid = gid << 1;\n"
						"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
						"{\n"
						"src_t[0] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid, (uint)cols - 1)] : 0);\n"
						"src_t[1] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid + 1, (uint)cols - 1)] : 0);\n"
						"sum_t[0] = (i == 0 ? 0 : lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+(LSIZE_2 + LOG_LSIZE)]);\n"    /////
						"sum_t[1] =  (i == 0 ? 0 : lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+(LSIZE_2 + LOG_LSIZE)]);\n" ////


						"barrier(CLK_LOCAL_MEM_FENCE);\n"
						"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"
						//"int bf_loc = lid ;\n"
						"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[0];\n"
						//"lm_sum[groupsize*group_id+1+bf_loc] = src_t[1];\n"
						"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[1];\n"
						//	"lm_sum[0][bf_loc] = src_t[0];\n"
							//"lm_sum[1][bf_loc] = src_t[1];\n"

						"int offset = 1;\n"

					"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
						"{\n"
							"barrier(CLK_LOCAL_MEM_FENCE);\n"
							"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
							//"printf(\" ai value before offset %d \\n\",ai);\n"
							//"printf(\" bi value before offset %d \\n\",bi);\n"

							"ai += GET_CONFLICT_OFFSET(ai);\n"
							"bi += GET_CONFLICT_OFFSET(bi);\n"


							"if((lid & LSIZE_LOOP_VALUE) < d)\n" //this only works upto 16 elements
							"{\n"

								//"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
								"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]  +=  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"
//								"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//								"{\n"
//									"printf(\" global first if................................. %d \\n\",lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]);\n"
//
//								"}\n"


							"}\n"
							"offset <<= 1;\n"
						"}\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"
						"if(lid < 2)\n"
						"{\n"
								"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+lid*(LSIZE + LOG_LSIZE) +(LSIZE_2 + LOG_LSIZE)] = 0;\n"

//								"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//										"{\n"
//												"printf(\" global second inside if condition %d \\n\",lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+lid*(LSIZE + LOG_LSIZE) +(LSIZE_2 + LOG_LSIZE)]);\n"
//										"}\n"
						"}\n"


						"for(int d = 1;  d < LSIZE; d <<= 1)\n"
						"{\n"
								"barrier(CLK_LOCAL_MEM_FENCE);\n"
								"offset >>= 1;\n"
								"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
								"ai += GET_CONFLICT_OFFSET(ai);\n"
								"bi += GET_CONFLICT_OFFSET(bi);\n"
									//=======================================================
								"if((lid & LSIZE_LOOP_VALUE) < d)\n"
								"{\n"
										//"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
										"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]  +=  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"
//										"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//										"{\n"
//											"printf(\" global first if................................ %d \\n\",lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]);\n"
//
//										"}\n"


								"}\n"
								"barrier(CLK_LOCAL_MEM_FENCE);\n"
								"if((lid & LSIZE_LOOP_VALUE) < d)\n"
								"{\n"

										//"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
										"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai] =  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]-lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"


//									"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//										"{\n"
//											"printf(\" global second if................................. %d \\n\",lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai]);\n"
//
//										"}\n"
////

								"}\n"
								//"barrier(CLK_LOCAL_MEM_FENCE);\n"
									//=======================================================
						"}\n"
						"barrier(CLK_LOCAL_MEM_FENCE);\n"
						"int loc_s0 = gid * dst_step + i + lid - 1 - pre_invalid * dst_step , loc_s1 = loc_s0 + dst_step;\n"
						"if(lid > 0 && (i+lid) <= rows)\n"
						"{\n"
							//"lm_sum[0][bf_loc] += sum_t[0];\n"  //////
							//"lm_sum[1][bf_loc] += sum_t[1];\n"  //////
								"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc] += sum_t[0];\n"
								"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc] += sum_t[1];\n"

							//"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
							"if ((gid < cols + pre_invalid && gid >= pre_invalid))\n"
							"{\n"
								//"sum[loc_s0] = lm_sum[0][bf_loc];\n"//sum_p[0];\n"
								"sum[loc_s0] = lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc];\n"//sum_p[0];\n"
//								"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//								"{\n"
//									"printf(\" global first if................. %d \\n\",sum[loc_s0]);\n"
//
//								"}\n"

							"}\n"
							//"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
							"if(gid + 1 < cols + pre_invalid)\n"
							"{\n"
								//"sum[loc_s1] = lm_sum[1][bf_loc];\n" //sum_p[0];\n"
								"sum[loc_s1] = lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc];\n" //sum_p[0];\n"
//								"if(group_id==1 && global_id==33 && lid==1 && gid==2 )\n"
//												"{\n"
//													"printf(\" global second if................. %d \\n\",sum[loc_s1]);\n"
//
//
//												"}\n"


							"}\n"
						"}\n"
							"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"}\n"
			"}\n"

		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) v2_integral_rows(__global int *srcsum,__global int *sum ,\n"
		"int rows,int cols,int src_step,int sum_step,int sum_offset)\n"
		"{\n"
		"unsigned int lid = get_local_id(0);\n"
		"unsigned int gid = get_group_id(0);\n"
		"int src_t[2], sum_t[2];\n"
		"__local int lm_sum[2][LSIZE + LOG_LSIZE];\n"
		"__local int *sum_p;\n"
		"src_step = src_step >> 2;\n"    //yuli1009
		"gid = gid << 1;\n"                //yuli1009
		"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
		"{\n"
		"src_t[0] = i + lid < rows ? srcsum[(lid+i) * src_step + gid] : 0;\n"
		"src_t[1] = i + lid < rows ? srcsum[(lid+i) * src_step + gid + 1] : 0;\n"
		"sum_t[0] =  (i == 0 ? 0 : lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"
		"sum_t[1] =  (i == 0 ? 0 : lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"
		"lm_sum[0][bf_loc] = src_t[0];\n"
		"lm_sum[1][bf_loc] = src_t[1];\n"
		"int offset = 1;\n"
		"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
		"{\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
		"ai += GET_CONFLICT_OFFSET(ai);\n"
		"bi += GET_CONFLICT_OFFSET(bi);\n"

		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"offset <<= 1;\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"if(lid < 2)\n"
		"{\n"
		"lm_sum[lid][LSIZE_2 + LOG_LSIZE] = 0;\n"
		"}\n"
		"for(int d = 1;  d < LSIZE; d <<= 1)\n"
		"{\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"offset >>= 1;\n"
		"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
		"ai += GET_CONFLICT_OFFSET(ai);\n"
		"bi += GET_CONFLICT_OFFSET(bi);\n"

		//======================================================
		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"

		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		//======================================================
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int loc_s0 = sum_offset + gid * sum_step + sum_step + i + lid, loc_s1 = loc_s0 + sum_step ;\n"
		"if(lid > 0 && (i+lid) <= rows)\n"
		"{\n"
		"lm_sum[0][bf_loc] += sum_t[0];\n"
		"lm_sum[1][bf_loc] += sum_t[1];\n"
		"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
		"if(gid < cols)\n"
		"{\n"
		"sum[loc_s0] = sum_p[0];\n"
		"}\n"
		"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
		"if(gid + 1 < cols)\n"
		"{\n"
		"sum[loc_s1] = sum_p[0];\n"
		"}\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"}\n"
		"}\n"


		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) v2_integral_rows_global(__global int *srcsum,__global int *sum ,\n"
		"int rows,int cols,int src_step,int sum_step,int sum_offset,__global int* lm_sum)\n"
		"{\n"
			"unsigned int lid = get_local_id(0);\n"
			"unsigned int gid = get_group_id(0);\n"
			"unsigned int group_id = get_group_id(0);\n"
			"unsigned int global_id = get_global_id(0);\n"

			"int src_t[2], sum_t[2];\n"
			//"__local int lm_sum[2][LSIZE + LOG_LSIZE];\n"
			//"__local int *sum_p;\n"
			"src_step = src_step >> 2;\n"    //yuli1009
			"gid = gid << 1;\n"                //yuli1009
			"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
			"{\n"
				"src_t[0] = i + lid < rows ? srcsum[(lid+i) * src_step + gid] : 0;\n"
				"src_t[1] = i + lid < rows ? srcsum[(lid+i) * src_step + gid + 1] : 0;\n"
				//"sum_t[0] =  (i == 0 ? 0 : lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"
				//"sum_t[1] =  (i == 0 ? 0 : lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n"
				"sum_t[0] = (i == 0 ? 0 : lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+(LSIZE_2 + LOG_LSIZE)]);\n"    /////
				"sum_t[1] =  (i == 0 ? 0 : lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+(LSIZE_2 + LOG_LSIZE)]);\n" ////

				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"
				//"lm_sum[0][bf_loc] = src_t[0];\n"
				//"lm_sum[1][bf_loc] = src_t[1];\n"
//				"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[0];\n"
//				"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[1];\n"

				"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[0];\n"
				"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc] = src_t[1];\n"


				"int offset = 1;\n"
				"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
				"{\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
					"ai += GET_CONFLICT_OFFSET(ai);\n"
					"bi += GET_CONFLICT_OFFSET(bi);\n"

					"if((lid & LSIZE_LOOP_VALUE) < d)\n"
					"{\n"
						//"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
						"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]  +=  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"

					"}\n"
					"offset <<= 1;\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"if(lid < 2)\n"
				"{\n"
					//"lm_sum[lid][LSIZE_2 + LOG_LSIZE] = 0;\n"
					"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+lid*(LSIZE + LOG_LSIZE) +(LSIZE_2 + LOG_LSIZE)] = 0;\n"

				"}\n"
				"for(int d = 1;  d < LSIZE; d <<= 1)\n"
				"{\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"offset >>= 1;\n"
					"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
					"ai += GET_CONFLICT_OFFSET(ai);\n"
					"bi += GET_CONFLICT_OFFSET(bi);\n"
//					/*"if((lid & LSIZE_LOOP_VALUE) < d)\n"
//					"{\n"
//					"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
//					"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
//					"}\n"*/
//					//======================================================
					"if((lid & LSIZE_LOOP_VALUE) < d)\n"
					"{\n"
						//"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
						"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]  +=  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"


					"}\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"

					"if((lid & LSIZE_LOOP_VALUE) < d)\n"
					"{\n"
						//"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
						"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai] =  lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+bi]-lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+(lid >> LSIZE_SHIFT_VALUE)*(LSIZE + LOG_LSIZE)+ai];\n"

					"}\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
//					//======================================================
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"int loc_s0 = sum_offset + gid * sum_step + sum_step + i + lid, loc_s1 = loc_s0 + sum_step ;\n"
				"if(lid > 0 && (i+lid) <= rows)\n"
				"{\n"
//					//"lm_sum[0][bf_loc] += sum_t[0];\n"
//					//"lm_sum[1][bf_loc] += sum_t[1];\n"
					"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc] += sum_t[0];\n"
				 	"lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc] += sum_t[1];\n"


//					//"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
					"if(gid < cols)\n"
					"{\n"
						//"sum[loc_s0] = sum_p[0];\n"
						"sum[loc_s0] = lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+0*(LSIZE + LOG_LSIZE)+bf_loc];\n"//sum_p[0];\n"


					"}\n"
//					//"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
					"if(gid + 1 < cols)\n"
					"{\n"
						//"sum[loc_s1] = sum_p[0];\n"
						"sum[loc_s1] = lm_sum[group_id*(LSIZE + LOG_LSIZE)*2+1*(LSIZE + LOG_LSIZE)+bf_loc];\n" //sum_p[0];\n"



					"}\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
			"}\n"
		"}\n"

		//------------small local memory kernel ends here--------------------//
		"typedef struct Stage\n"
		"{\n"
		"int    first;\n"
		"int    ntrees;\n"
		"float  threshold;\n"
		"}Stage;\n"
		"typedef struct ClNode\n"
		"{\n"
		"int   left;\n"
		"int   right;\n"
		"int   featureIdx;\n"
		"}ClNode;\n"

		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) lbp_cascade(\n"
				"global Stage* stages,\n"
				"int nstages,\n"
				"global ClNode* nodes,\n"
				"global float* leaves,\n"
				"global int*subsets,\n"
				"__constant uchar* features,\n"
				"int subsetSize, \n"
				"int frameW, \n"
				"int frameH, \n"
				"int windowW, \n"
				"int windowH, \n"
				"float scale, \n"
				"const float factor,\n"
				"const int total, \n"
				"global int* integral, \n"
				"const int pitch, \n"
				"global int4* objects, \n"
				"int object_col,\n"
				"global unsigned int* classified)\n" //start from here
				//"global  int* test_classified_value)\n"
				"{\n"
				"int ftid = get_group_id(0) * get_local_size(0) + get_local_id(0);\n"

				//mad24 (get_group_id(0), get_local_size(0),get_local_id(0));\n"//get_group_id(0) * get_local_size(0) + get_local_id(0);\n" //block1

				"bool ftid_check=(ftid >= total);\n"
				"if (ftid_check) return;\n"

				"int step = (scale <= 2.f);\n"
				"int windowsForLine = ((int)( frameW / scale + 0.5f) - windowW ) >> step;\n"
				"int stotal = windowsForLine * (( (int)(frameH / scale + 0.5f) - windowH) >> step);\n"
				"int wshift = 0;\n"
				"int scaleTid = ftid;\n"


				"while (scaleTid >= stotal)\n"                               //block2
				"{\n"
				"scaleTid -= stotal;\n"
				"wshift += (int)(frameW / scale + 0.5f) + 1;\n"
				"scale *= factor;\n"
				"step = (scale <= 2.f);\n"
				"windowsForLine = ( ((int)(frameW / scale + 0.5f) - windowW) >> step);\n"
				"stotal = windowsForLine * ( (int)(frameH / scale + 0.5f) - windowH) >> step;\n"
				"}\n"                                                          //block2
				"int y = scaleTid / windowsForLine;\n"   //added 1018         //block3
				"int x = scaleTid % windowsForLine;\n"
				"x <<= step;\n"
				"y <<= step;\n"
				"bool bcalc = true;\n"  //true
				"int xc = x + wshift;\n"
				"int current_node = 0;\n"
				"int current_leave = 0;\n"
				"int s=0;\n"

				"while (s < nstages && bcalc)\n"                        //block3
				//"for (int s = 0; s < nstages && bcalc; ++s)\n"   //b4
				"{\n"

				"float sum = 0;\n"
				"Stage stage = stages[s];\n"


				//"#pragma unroll stage.ntrees \n"
				"for (int t = 0; t < stage.ntrees; t++)\n"
				"{\n"
				"ClNode node = nodes[current_node];\n"
				"uchar4 feature;\n"
				"feature.x = features[4 * node.featureIdx];\n"
				"feature.y = features[4 * node.featureIdx + 1];\n"
				"feature.z = features[4 * node.featureIdx + 2];\n"
				"feature.w = features[4 * node.featureIdx + 3];\n"

				"int shift;\n"

				"int ty =(y + feature.y) * pitch + xc + feature.x;\n"
				"int temp =y* pitch + xc;\n"
				//mad24((y + feature.y) , pitch , xc + feature.x);\n"//(y + feature.y) * pitch + xc + feature.x;\n"  //////////////////////////////////
				"int fh = feature.w * pitch;\n"
				//mul24(feature.w , pitch);\n" //feature.w * pitch;
				"int fw = feature.z;\n"




				"int anchors0=2;\n"
				"int anchors1=3;\n"
				"int anchors2=6;\n"
				"int anchors3=9;\n"
				"int anchors4=1;\n"
				"int anchors5=7;\n"
				"int anchors6=7;\n"
				"int anchors7=5;\n"
				"int anchors8=4;\n"
				"anchors0  = integral[ty];\n"
				"anchors1  = integral[ty + fw];\n"
				"anchors0 -= anchors1;\n"
				"anchors2  = integral[ty + fw * 2];\n"

				"anchors1 -= anchors2;\n"
				"anchors2 -= integral[ty + fw * 3];\n"

				"ty += fh;\n"
				"anchors3  = integral[ty];\n"
				"anchors4  = integral[ty + fw];\n"
				"anchors3 -= anchors4;\n"
				"anchors5  = integral[ty + fw * 2];\n"

				"anchors4 -= anchors5;\n"
				"anchors5 -= integral[ty + fw * 3];\n"

				"anchors0 -= anchors3;\n"
				"anchors1 -= anchors4;\n"
				"anchors2 -= anchors5;\n"
				"ty += fh;\n"
				"anchors6  = integral[ty];\n"
				"anchors7  = integral[ty + fw];\n"
				"anchors6 -= anchors7;\n"
				"anchors8  = integral[ty + fw * 2];\n"

				"anchors7 -= anchors8;\n"
				"anchors8 -= integral[ty + fw * 3];\n"

				"anchors3 -= anchors6;\n"
				"anchors4 -= anchors7;\n"
				"anchors5 -= anchors8;\n"
				"anchors0 -= anchors4;\n"
				"anchors1 -= anchors4;\n"
				"anchors2 -= anchors4;\n"
				"anchors3 -= anchors4;\n"
				"anchors5 -= anchors4;\n"
				"int response = (~(anchors0 >> 31)) & 4;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"response |= (~(anchors1 >> 31)) & 2;;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"response |= (~(anchors2 >> 31)) & 1;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"shift = (~(anchors5 >> 31)) & 16;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift00: %d\\n\",shift);\n"
				"shift |= (~(anchors3 >> 31)) & 1;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift01: %d\\n\",shift);\n"
				"ty += fh;\n"
				"anchors0  = integral[ty];\n"
				"anchors1  = integral[ty + fw];\n"
				"anchors0 -= anchors1;\n"
				"anchors2  = integral[ty + fw * 2];\n"

				"anchors1 -= anchors2;\n"
				"anchors2 -= integral[ty + fw * 3];\n"

				"anchors6 -= anchors0;\n"
				"anchors7 -= anchors1;\n"
				"anchors8 -= anchors2;\n"
				"anchors6 -= anchors4;\n"
				"anchors7 -= anchors4;\n"
				"anchors8 -= anchors4;\n"
				"shift |= (~(anchors6 >> 31)) & 2;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift1: %d\\n\",shift);\n"
				"shift |= (~(anchors7 >> 31)) & 4;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift2 : %d\\n\",shift);\n"
				"shift |= (~(anchors8 >> 31)) & 8;\n" ///////////////////////////////////
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift3 : %d\\n\",shift);\n"
				"int c = response;\n"
				"int idx =  (subsets[ current_node * subsetSize + c] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"

				"sum += leaves[idx];\n"

				"current_node += 1;\n"
				"current_leave += 2;\n"

				"}\n"
				"bool sum_check=(sum < stage.threshold);\n"
				"if (sum_check)\n"
				"bcalc = false;\n"

				"++s;\n"

				"}\n"   //while loop ends                                         //b4
				"if (bcalc)\n"
				"{\n"
				"bool window_check=(x >= (int)(frameW / scale + 0.5f) - windowW);\n"
				"if(window_check) return;\n"
				"int4 rect;\n"
				"rect.x = (int)(x * scale + 0.5f);\n"
				"rect.y = (int)(y * scale + 0.5f);\n"
				"rect.z = (int)(windowW * scale + 0.5f);\n"
				"rect.w = (int)(windowH * scale + 0.5f);\n"


				"int res = atomic_inc( classified );\n" //commented to see its impact
				//"test_classified_value[ftid] = 1;\n"
				"objects[res] = rect;\n"
				"}\n"
				"}\n" //end here

				"#define WG_SIZE 64\n"
				"#define HALF_WG_SIZE (WG_SIZE/2)\n"
				"__kernel\n"
				"void __attribute__((reqd_work_group_size(64, 1, 1)))scan(__global unsigned char* input, \n"
				"          __global int* output, \n"
				"                     int  rows,\n"
				"                     int  cols,\n"
				"					  int outputStep,\n"
				"			__global int* lData,\n"
				"			__global int* lData2,\n"
				"					  int totalWG) {\n"

				    // Each work group is responsible for scanning a row\n"

				    // If we add additional elements to local memory (half the\n"
				    // work group size) and fill them with zeros, we don't need\n"
				    // conditionals in the code to check for out-of-bounds\n"
				    // accesses\n"
				//"    __local int lData[WG_SIZE+HALF_WG_SIZE];\n"
				//"    __local int lData2[WG_SIZE+HALF_WG_SIZE];\n"

				"    int myGlobalY = get_global_id(1);\n"
				"    int myLocalX = get_local_id(0);\n"
				"	 int groupNoY=get_group_id(1);\n"
				"	 int groupNoX=get_group_id(0);\n"
				"	 int indexWG=(groupNoY*totalWG+groupNoX)*96;\n"
				//"if(groupNoX!=0)\n"
				//	"printf(\"group id: %d\\n\",groupNoY);\n"
				    // Initialize out-of-bounds elements with zeros\n"
				"    if(myLocalX < HALF_WG_SIZE) {\n"
				"        lData[indexWG+myLocalX] = 0.0f;\n"
				"        lData2[indexWG+myLocalX] = 0.0f;\n"
				"    }\n"

				//if(getglobal==1 && myGlobalY==0)\n"
				//{printf("kernel1 : %d\n", input[4]);\n"
				//printf("kernel0 : %d\n", input[0]);}\n"

				    // Start past out-of-bounds elements\n"
				"    myLocalX += HALF_WG_SIZE;\n"
				"    int newmyLocalX = indexWG+myLocalX;\n"

				    // This value needs to be added to the local data.  It's the highest\n"
				    // value from the prefix scan of the previous elements in the row\n"
				"    float prevMaxVal = 0;\n"

				    // March down a row WG_SIZE elements at a time\n"
				"    int iterations = cols/WG_SIZE;\n"
				"    if(cols % WG_SIZE != 0) \n"
				"    {\n"
				        // If cols are not an exact multiple of WG_SIZE, then we need to do\n"
				        // one more iteration \n"
				"        iterations++;\n"
				"    }\n"

				"   for(int i = 0; i < iterations; i++) {\n"

				"        int columnOffset = i*WG_SIZE + get_local_id(0);\n"

				        // Don't do anything if a thread's index is past the end of the row.  We still need\n"
				        // the thread available for memory barriers though.\n"
				"        if(columnOffset < cols) {\n"
				            // Cache the input data in local memory\n"
				"            lData[newmyLocalX] = input[myGlobalY*cols + columnOffset];\n"
				"        }\n"
				"        else {\n"
				            // Otherwise just store zeros \n"
				"            lData[newmyLocalX] = 0.0f;\n"
				"        }\n"

				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 1\n"
				"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 2\n"
				"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 4\n"
				"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 8\n"
				"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 16\n"
				"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-16];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // 32\n"
				"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-32];\n"
				"        barrier(CLK_LOCAL_MEM_FENCE);\n"

				        // Write data out to global memory\n"
				"        if(columnOffset < outputStep) {\n"
				            //output[myGlobalY*cols + columnOffset] = lData[myLocalX] + prevMaxVal;\n"
				"			output[myGlobalY*outputStep + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
				"        }\n"

				        // Copy the value from the highest thread in the group to my local index\n"
				"        prevMaxVal += lData[indexWG+(WG_SIZE+HALF_WG_SIZE-1)];\n"
				"    }\n"
				"}\n"
		"__kernel void __attribute__((reqd_work_group_size(64, 1, 1)))scan_vec(__global uchar4* input, \n"
										"          __global int4* output, \n"
										"                     int  rows,\n"
										"                     int  cols,\n"
										"					  int outputStep,\n"
										"			__global int4* lData,\n"
										"			__global int4* lData2,\n"
										"					  int totalWG) {\n"


										"    int myGlobalY = get_global_id(1);\n"
										"    int myLocalX = get_local_id(0);\n"
										"	 int groupNoY=get_group_id(1);\n"
										"	 int groupNoX=get_group_id(0);\n"
										"	 int indexWG=(groupNoY*1+groupNoX)*(96);\n" //datatytp[e changed to int4. so each index contains 4 elements. so starting fo next wg index should be 96/4 index away.


										"    if(myLocalX < HALF_WG_SIZE) {\n"  //it should be changed to HALF_WG_SIZE/4 as I am using vector data type
										"        lData[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
										"        lData2[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
										"    }\n"


										"    myLocalX += HALF_WG_SIZE;\n"
										"    int newmyLocalX = indexWG+myLocalX;\n"


										"    int prevMaxVal = 0;\n"

										"    int columns = cols/4;\n" //as data type has been changed there would be 1/4th column
										    // March down a row WG_SIZE elements at a time\n"
										"    int iterations = columns/WG_SIZE;\n"
										"    if(columns % WG_SIZE != 0) \n"
										"    {\n"

										"        iterations++;\n"
										"    }\n"

										"   for(int i = 0; i < iterations; i++) {\n"

										"        int columnOffset = i*WG_SIZE + get_local_id(0);\n"

										"        if(columnOffset < columns) {\n"
										            // Cache the input data in local memory\n"
										"            lData[newmyLocalX] = convert_int4(input[myGlobalY*columns + columnOffset]);\n" //need to confirm if there is any other way
										"        }\n"
										"        else {\n"
										            // Otherwise just store zeros \n"
										"            lData[newmyLocalX] = (int4)(0, 0, 0, 0);\n"
										"        }\n"
				  	  	  	  	  	  	"lData[newmyLocalX].y += lData[newmyLocalX].x;\n"
				        				"lData[newmyLocalX].z += lData[newmyLocalX].y;\n"
				        				"lData[newmyLocalX].w += lData[newmyLocalX].z;\n"

										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 1\n"
										"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 2\n"
										"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 4\n"
										"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 8\n"
										"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 16\n"
										"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-16].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // 32\n"
										"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-32].w;\n"
										"        barrier(CLK_LOCAL_MEM_FENCE);\n"

										        // Write data out to global memory\n"
										"        if(columnOffset < (outputStep/4)) {\n"

										"			output[myGlobalY*(outputStep/4) + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
										"        }\n"

										        // Copy the value from the highest thread in the group to my local index\n"
										"        prevMaxVal += lData[indexWG+(WG_SIZE+HALF_WG_SIZE-1)].w;\n"

										"    }\n"

										"}\n"


		"__kernel void __attribute__((reqd_work_group_size(64, 1, 1)))scan_vec2(__global int4* input, \n"
											"          __global int4* output, \n"
											"                     int  rows,\n"
											"                     int  cols,\n"
											"					  int outputStep,\n"
											"			__global int4* lData,\n"
											"			__global int4* lData2,\n"
											"					  int totalWG) {\n"


											"    int myGlobalY = get_global_id(1);\n"
											"    int myLocalX = get_local_id(0);\n"
											"	 int groupNoY=get_group_id(1);\n"
											"	 int groupNoX=get_group_id(0);\n"
											"	 int indexWG=(groupNoY*1+groupNoX)*(96);\n" //datatytp[e changed to int4. so each index contains 4 elements. so starting fo next wg index should be 96/4 index away.


											"    if(myLocalX < HALF_WG_SIZE) {\n"  //it should be changed to HALF_WG_SIZE/4 as I am using vector data type
											"        lData[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
											"        lData2[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
											"    }\n"


											"    myLocalX += HALF_WG_SIZE;\n"
											"    int newmyLocalX = indexWG+myLocalX;\n"


											"    int prevMaxVal = 0;\n"

											"    int columns = cols/4;\n" //as data type has been changed there would be 1/4th column
											    // March down a row WG_SIZE elements at a time\n"
											"    int iterations = columns/WG_SIZE;\n"
											"    if(columns % WG_SIZE != 0) \n"
											"    {\n"

											"        iterations++;\n"
											"    }\n"

											"   for(int i = 0; i < iterations; i++) {\n"

											"        int columnOffset = i*WG_SIZE + get_local_id(0);\n"

											"        if(columnOffset < columns) {\n"
											            // Cache the input data in local memory\n"
											"            lData[newmyLocalX] = convert_int4(input[myGlobalY*columns + columnOffset]);\n" //need to confirm if there is any other way
											"        }\n"
											"        else {\n"
											            // Otherwise just store zeros \n"
											"            lData[newmyLocalX] = (int4)(0, 0, 0, 0);\n"
											"        }\n"
					  	  	  	  	  	  	"lData[newmyLocalX].y += lData[newmyLocalX].x;\n"
					        				"lData[newmyLocalX].z += lData[newmyLocalX].y;\n"
					        				"lData[newmyLocalX].w += lData[newmyLocalX].z;\n"

											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 1\n"
											"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 2\n"
											"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 4\n"
											"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 8\n"
											"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 16\n"
											"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-16].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // 32\n"
											"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-32].w;\n"
											"        barrier(CLK_LOCAL_MEM_FENCE);\n"

											        // Write data out to global memory\n"
											"        if(columnOffset < (outputStep/4)) {\n"

											"			output[myGlobalY*(outputStep/4) + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
											"        }\n"

											        // Copy the value from the highest thread in the group to my local index\n"
											"        prevMaxVal += lData[indexWG+(WG_SIZE+HALF_WG_SIZE-1)].w;\n"

											"    }\n"

											"}\n"

		"#define WG_SIZE_vscan 16\n"
				"#define HALF_WG_SIZE_vscan (WG_SIZE_vscan/2)\n"

				"__kernel void __attribute__((reqd_work_group_size(16, 1, 1)))scan_vec_small(__global uchar4* input, \n"
												"          __global int4* output, \n"
												"                     int  rows,\n"
												"                     int  cols,\n"
												"					  int outputStep,\n"
												"			__global int4* lData,\n"
												"			__global int4* lData2,\n"
												"					  int totalWG) {\n"



												"    int myGlobalY = get_global_id(1);\n"
												"    int myLocalX = get_local_id(0);\n"
												"	 int groupNoY=get_group_id(1);\n"
												"	 int groupNoX=get_group_id(0);\n"
												"	 int indexWG=(groupNoY*1+groupNoX)*(24);\n" //datatytp[e changed to int4. so each index contains 4 elements. so starting fo next wg index should be 96/4 index away.

				//
												"    if(myLocalX < HALF_WG_SIZE_vscan) {\n"  //it should be changed to HALF_WG_SIZE_vscan/4 as I am using vector data type
												"        lData[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
												"        lData2[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
												"    }\n"



												    // Start past out-of-bounds elements\n"
												"    myLocalX += HALF_WG_SIZE_vscan;\n"
												"    int newmyLocalX = indexWG+myLocalX;\n"

												    // This value needs to be added to the local data.  It's the highest\n"
												    // value from the prefix scan of the previous elements in the row\n"
												"    int prevMaxVal = 0;\n"

												"    int columns = cols/4;\n" //as data type has been changed there would be 1/4th column
												    // March down a row WG_SIZE_vscan elements at a time\n"
												"    int iterations = columns/WG_SIZE_vscan;\n"
												"    if(columns % WG_SIZE_vscan != 0) \n"
												"    {\n"

												"        iterations++;\n"
												"    }\n"

												"   for(int i = 0; i < iterations; i++) {\n"

												"        int columnOffset = i*WG_SIZE_vscan + get_local_id(0);\n"


												"        if(columnOffset < columns) {\n"
												            // Cache the input data in local memory\n"
												"            lData[newmyLocalX] = convert_int4(input[myGlobalY*columns + columnOffset]);\n" //need to confirm if there is any other way
												"        }\n"
												"        else {\n"
												            // Otherwise just store zeros \n"
												"            lData[newmyLocalX] = (int4)(0, 0, 0, 0);\n"
												"        }\n"
								  	  	  	  	  	  	"lData[newmyLocalX].y += lData[newmyLocalX].x;\n"
														"lData[newmyLocalX].z += lData[newmyLocalX].y;\n"
														"lData[newmyLocalX].w += lData[newmyLocalX].z;\n"

												"        barrier(CLK_LOCAL_MEM_FENCE);\n"

												        // 1\n"
												"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1].w;\n"
												"        barrier(CLK_LOCAL_MEM_FENCE);\n"

												        // 2\n"
												"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2].w;\n"
												"        barrier(CLK_LOCAL_MEM_FENCE);\n"

												        // 4\n"
												"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4].w;\n"
												"        barrier(CLK_LOCAL_MEM_FENCE);\n"

												        // 8\n"
												"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8].w;\n"
												"        barrier(CLK_LOCAL_MEM_FENCE);\n"



												        // Write data out to global memory\n"
												"        if(columnOffset < (outputStep/4)) {\n"
				//								            //output[myGlobalY*cols + columnOffset] = lData[myLocalX] + prevMaxVal;\n"
				//											"if(get_global_id(0)==0 && myGlobalY==1 && i==0){\n"
				//											"printf(\"vector 0 xx id: %d\\n\",lData[newmyLocalX].x + prevMaxVal);\n"
				//											"printf(\"vector 0 yy %d\\n\",lData[newmyLocalX].y + prevMaxVal);\n"
				//											"printf(\"vector 0 zz %d\\n\",lData[newmyLocalX].z + prevMaxVal);\n"
				//											"printf(\"group id ww %d\\n\",lData[newmyLocalX].w + prevMaxVal);\n"
				//										"    }\n"
												"			output[myGlobalY*(outputStep/4) + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
												"        }\n"

												        // Copy the value from the highest thread in the group to my local index\n"
												"        prevMaxVal += lData[indexWG+(WG_SIZE_vscan+HALF_WG_SIZE_vscan-1)].w;\n"

												"    }\n"

												"}\n"

		"__kernel void __attribute__((reqd_work_group_size(16, 1, 1)))scan_vec_small_2(__global int4* input, \n"
														"          __global int4* output, \n"
														"                     int  rows,\n"
														"                     int  cols,\n"
														"					  int outputStep,\n"
														"			__global int4* lData,\n"
														"			__global int4* lData2,\n"
														"					  int totalWG) {\n"



														"    int myGlobalY = get_global_id(1);\n"
														"    int myLocalX = get_local_id(0);\n"
														"	 int groupNoY=get_group_id(1);\n"
														"	 int groupNoX=get_group_id(0);\n"
														"	 int indexWG=(groupNoY*1+groupNoX)*(24);\n" //datatytp[e changed to int4. so each index contains 4 elements. so starting fo next wg index should be 96/4 index away.

						//
														"    if(myLocalX < HALF_WG_SIZE_vscan) {\n"  //it should be changed to HALF_WG_SIZE_vscan/4 as I am using vector data type
														"        lData[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
														"        lData2[indexWG+myLocalX] = (int4)(0, 0, 0, 0);\n"
														"    }\n"



														    // Start past out-of-bounds elements\n"
														"    myLocalX += HALF_WG_SIZE_vscan;\n"
														"    int newmyLocalX = indexWG+myLocalX;\n"

														    // This value needs to be added to the local data.  It's the highest\n"
														    // value from the prefix scan of the previous elements in the row\n"
														"    int prevMaxVal = 0;\n"

														"    int columns = cols/4;\n" //as data type has been changed there would be 1/4th column
														    // March down a row WG_SIZE_vscan elements at a time\n"
														"    int iterations = columns/WG_SIZE_vscan;\n"
														"    if(columns % WG_SIZE_vscan != 0) \n"
														"    {\n"

														"        iterations++;\n"
														"    }\n"

														"   for(int i = 0; i < iterations; i++) {\n"

														"        int columnOffset = i*WG_SIZE_vscan + get_local_id(0);\n"


														"        if(columnOffset < columns) {\n"
														            // Cache the input data in local memory\n"
														"            lData[newmyLocalX] = convert_int4(input[myGlobalY*columns + columnOffset]);\n" //need to confirm if there is any other way
														"        }\n"
														"        else {\n"
														            // Otherwise just store zeros \n"
														"            lData[newmyLocalX] = (int4)(0, 0, 0, 0);\n"
														"        }\n"
										  	  	  	  	  	  	"lData[newmyLocalX].y += lData[newmyLocalX].x;\n"
																"lData[newmyLocalX].z += lData[newmyLocalX].y;\n"
																"lData[newmyLocalX].w += lData[newmyLocalX].z;\n"

														"        barrier(CLK_LOCAL_MEM_FENCE);\n"

														        // 1\n"
														"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1].w;\n"
														"        barrier(CLK_LOCAL_MEM_FENCE);\n"

														        // 2\n"
														"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2].w;\n"
														"        barrier(CLK_LOCAL_MEM_FENCE);\n"

														        // 4\n"
														"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4].w;\n"
														"        barrier(CLK_LOCAL_MEM_FENCE);\n"

														        // 8\n"
														"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8].w;\n"
														"        barrier(CLK_LOCAL_MEM_FENCE);\n"



														        // Write data out to global memory\n"
														"        if(columnOffset < (outputStep/4)) {\n"
						//								            //output[myGlobalY*cols + columnOffset] = lData[myLocalX] + prevMaxVal;\n"
						//											"if(get_global_id(0)==0 && myGlobalY==1 && i==0){\n"
						//											"printf(\"vector 0 xx id: %d\\n\",lData[newmyLocalX].x + prevMaxVal);\n"
						//											"printf(\"vector 0 yy %d\\n\",lData[newmyLocalX].y + prevMaxVal);\n"
						//											"printf(\"vector 0 zz %d\\n\",lData[newmyLocalX].z + prevMaxVal);\n"
						//											"printf(\"group id ww %d\\n\",lData[newmyLocalX].w + prevMaxVal);\n"
						//										"    }\n"
														"			output[myGlobalY*(outputStep/4) + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
														"        }\n"

														        // Copy the value from the highest thread in the group to my local index\n"
														"        prevMaxVal += lData[indexWG+(WG_SIZE_vscan+HALF_WG_SIZE_vscan-1)].w;\n"

														"    }\n"

														"}\n"

				"__kernel\n"
						"void __attribute__((reqd_work_group_size(64, 1, 1))) scan2(__global int* input, \n"
						"          __global int* output, \n"
						"                     int  rows,\n"
						"                     int  cols,\n"
						"					  int outputStep,\n"
						"			__global int* lData,\n"
						"			__global int* lData2,\n"
						"					  int totalWG) {\n"

						    // Each work group is responsible for scanning a row\n"

						    // If we add additional elements to local memory (half the\n"
						    // work group size) and fill them with zeros, we don't need\n"
						    // conditionals in the code to check for out-of-bounds\n"
						    // accesses\n"
						//"    __local int lData[WG_SIZE+HALF_WG_SIZE];\n"
						//"    __local int lData2[WG_SIZE+HALF_WG_SIZE];\n"

						"    int myGlobalY = get_global_id(1);\n"
						"    int myLocalX = get_local_id(0);\n"
						"	 int groupNoY=get_group_id(1);\n"
						"	 int groupNoX=get_group_id(0);\n"
						"	 int indexWG=(groupNoY*totalWG+groupNoX)*96;\n"
						//"if(ftid>=0 && ftid<=7)\n"
							//"printf(\"group id: %d\\n\",groupNo);\n"
						    // Initialize out-of-bounds elements with zeros\n"
						"    if(myLocalX < HALF_WG_SIZE) {\n"
						"        lData[indexWG+myLocalX] = 0.0f;\n"
						"        lData2[indexWG+myLocalX] = 0.0f;\n"
						"    }\n"

						//if(getglobal==1 && myGlobalY==0)\n"
						//{printf("kernel1 : %d\n", input[4]);\n"
						//printf("kernel0 : %d\n", input[0]);}\n"

						    // Start past out-of-bounds elements\n"
						"    myLocalX += HALF_WG_SIZE;\n"
						"    int newmyLocalX = indexWG+myLocalX;\n"

						    // This value needs to be added to the local data.  It's the highest\n"
						    // value from the prefix scan of the previous elements in the row\n"
						"    float prevMaxVal = 0;\n"

						    // March down a row WG_SIZE elements at a time\n"
						"    int iterations = cols/WG_SIZE;\n"
						"    if(cols % WG_SIZE != 0) \n"
						"    {\n"
						        // If cols are not an exact multiple of WG_SIZE, then we need to do\n"
						        // one more iteration \n"
						"        iterations++;\n"
						"    }\n"

						"   for(int i = 0; i < iterations; i++) {\n"

						"        int columnOffset = i*WG_SIZE + get_local_id(0);\n"

						        // Don't do anything if a thread's index is past the end of the row.  We still need\n"
						        // the thread available for memory barriers though.\n"
						"        if(columnOffset < cols) {\n"
						            // Cache the input data in local memory\n"
						"            lData[newmyLocalX] = input[myGlobalY*cols + columnOffset];\n"
						"        }\n"
						"        else {\n"
						            // Otherwise just store zeros \n"
						"            lData[newmyLocalX] = 0.0f;\n"
						"        }\n"

						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 1\n"
						"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-1];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 2\n"
						"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-2];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 4\n"
						"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-4];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 8\n"
						"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-8];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 16\n"
						"        lData2[newmyLocalX] = lData[newmyLocalX] + lData[newmyLocalX-16];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // 32\n"
						"        lData[newmyLocalX] = lData2[newmyLocalX] + lData2[newmyLocalX-32];\n"
						"        barrier(CLK_LOCAL_MEM_FENCE);\n"

						        // Write data out to global memory\n"
						"        if(columnOffset < outputStep) {\n"
						            //output[myGlobalY*cols + columnOffset] = lData[myLocalX] + prevMaxVal;\n"
						"			output[myGlobalY*outputStep + columnOffset] = lData[newmyLocalX] + prevMaxVal;\n"
						"        }\n"

						        // Copy the value from the highest thread in the group to my local index\n"
						"        prevMaxVal += lData[indexWG+(WG_SIZE+HALF_WG_SIZE-1)];\n"
						"    }\n"
						"}\n"
				"__kernel\n"
				"void __attribute__((reqd_work_group_size(16, 16, 1))) transpose(__global int* iImage,\n"
				"               __global int* oImage,\n"
				"                        int    inRows,\n"
				"                        int    inCols,\n"
				"                        int    inputMatStep,\n"
				"						 int outMatStep,\n"
				"						 int totalWG,"
				"				__global int* tmpBuffer) {\n"

				//"    __local float tmpBuffer[256];\n"

				    // Work groups will perform the transpose (i.e., an entire work group will be moved from\n"
				    // one part of the image to another) \n"
				"    int myWgInX = get_group_id(0);\n"
				"    int myWgInY = get_group_id(1);\n"

				    // The local index of a thread should not change (threads with adjacent IDs \n"
				    // within a work group should always perform adjacent memory accesses).  We will \n"
				    // account for the clever indexing of local memory later.\n"
				"    int myLocalX = get_local_id(0);\n"
				"    int myLocalY = get_local_id(1);\n"

				"    int myGlobalInX = myWgInX*16 + myLocalX;\n"
				"    int myGlobalInY = myWgInY*16 + myLocalY;\n"
				"	 int indexWG=(myWgInY*totalWG+myWgInX)*256;\n"

				    // Don't read out of bounds\n"
				"    if(myGlobalInX < inCols && myGlobalInY < inRows) {\n"

				        // Cache data to local memory (coalesced reads)\n"
				"        tmpBuffer[indexWG+(myLocalY*16 + myLocalX)] = iImage[myGlobalInY*inputMatStep + myGlobalInX];\n"  //can try with input cols

				"    }\n"

			//	"    barrier(CLK_LOCAL_MEM_FENCE);\n"
				"    barrier(CLK_GLOBAL_MEM_FENCE);\n"

				    // This avoids some confusion with dimensions, but otherwise aren't needed\n"
				"    int outRows = inCols;\n"

				  "  int outCols = inRows;\n"
				 //"int outCols = outMatStep;\n"

				    // Swap work group IDs for their location after the transpose\n"
				"    int myWgOutX = myWgInY;\n"
				"    int myWgOutY = myWgInX;\n"

				"    int myGlobalOutX = myWgOutX*16 + myLocalX;\n"
				"    int myGlobalOutY = myWgOutY*16 + myLocalY;\n"

				    // When writing back to global memory, we need to swap image dimensions (for the\n"
				    // transposed size), and also need to swap thread X/Y indices in local memory\n"
				    // (to achieve coalesced memory writes)\n"

				    // Don't write out of bounds\n"
				"    if(myGlobalOutX >= 0 && myGlobalOutX < outCols && \n"
				"       myGlobalOutY >= 0 && myGlobalOutY < outRows) {\n"
					//"if(myWgInX==0 && myWgInY==0){\n"
				       // The read from tmpBuffer is going to conflict, but the write should be coalesced\n"

				"       oImage[myGlobalOutY*outMatStep + myGlobalOutX] = tmpBuffer[indexWG+(myLocalX*16 + myLocalY)]; \n"


				//"       oImage[myGlobalOutY*outCols + myGlobalOutX] = 10; \n"
				//"if(myGlobalOutY*outCols + myGlobalOutX==0)\n"
			//"       oImage[myGlobalOutY*outCols + myGlobalOutX] = indexWG+(myLocalX*16 + myLocalY); \n"
				//"    }\n"
				"    }\n"

				"    return;\n"
				"} \n"

		"__kernel void __attribute__((reqd_work_group_size(16, 16, 1))) transposev2(__global int* iImage,\n"
						"               __global int* oImage,\n"
						"                        int    inRows,\n"
						"                        int    inCols,\n"
						"                        int    inputMatStep,\n"
						"						 int outMatStep,\n"
						"						 int totalWG,"
						"				__global int* tmpBuffer,\n"
						"						 int sum_offset) {\n"

						//"    __local float tmpBuffer[256];\n"

						    // Work groups will perform the transpose (i.e., an entire work group will be moved from\n"
						    // one part of the image to another) \n"
						"    int myWgInX = get_group_id(0);\n"
						"    int myWgInY = get_group_id(1);\n"

						    // The local index of a thread should not change (threads with adjacent IDs \n"
						    // within a work group should always perform adjacent memory accesses).  We will \n"
						    // account for the clever indexing of local memory later.\n"
						"    int myLocalX = get_local_id(0);\n"
						"    int myLocalY = get_local_id(1);\n"

						"    int myGlobalInX = myWgInX*16 + myLocalX;\n"
						"    int myGlobalInY = myWgInY*16 + myLocalY;\n"
						"	 int indexWG=(myWgInY*totalWG+myWgInX)*256;\n"

						    // Don't read out of bounds\n"
						"    if(myGlobalInX < inCols && myGlobalInY < inRows) {\n"

						        // Cache data to local memory (coalesced reads)\n"
						"        tmpBuffer[indexWG+(myLocalY*16 + myLocalX)] = iImage[myGlobalInY*inputMatStep + myGlobalInX];\n"

						"    }\n"

						"    barrier(CLK_LOCAL_MEM_FENCE);\n"

						    // This avoids some confusion with dimensions, but otherwise aren't needed\n"
						"    int outRows = inCols;\n"

						   " int outCols = inRows;\n"
						// "int outCols = outMatStep;\n"

						    // Swap work group IDs for their location after the transpose\n"
						"    int myWgOutX = myWgInY;\n"
						"    int myWgOutY = myWgInX;\n"

						"    int myGlobalOutX = myWgOutX*16 + myLocalX;\n"
						"    int myGlobalOutY = myWgOutY*16 + myLocalY;\n"

						    // When writing back to global memory, we need to swap image dimensions (for the\n"
						    // transposed size), and also need to swap thread X/Y indices in local memory\n"
						    // (to achieve coalesced memory writes)\n"
						"    int output_index=outCols+myGlobalOutY*outCols + myGlobalOutX+1;\n"
						    // Don't write out of bounds\n"
						"    if(myGlobalOutX >= 0 && myGlobalOutX < outCols && \n"
						"       myGlobalOutY >= 0 && myGlobalOutY < outRows) {\n"

						       // The read from tmpBuffer is going to conflict, but the write should be coalesced\n"
						"       oImage[outMatStep+myGlobalOutY*outMatStep + myGlobalOutX+1+sum_offset] = tmpBuffer[indexWG+(myLocalX*16 + myLocalY)]; \n"
						"    }\n"

						"    return;\n"
						"} \n";


size_t divUp(int x, int x1) {return (int)(ceilf((float)x / x1)) * x1; }

cv::Size operator -(const cv::Size& a, const cv::Size& b)
{
	return cv::Size(a.width - b.width, a.height - b.height);
}

cv::Size operator +(const cv::Size& a, const int& i)
{
	return cv::Size(a.width + i, a.height + i);
}

cv::Size operator *(const cv::Size& a, const float& f)
{
	return cv::Size(cvRound(a.width * f), cvRound(a.height * f));
}

cv::Size operator /(const cv::Size& a, const float& f)
{
	return cv::Size(cvRound(a.width / f), cvRound(a.height / f));
}

bool operator <=(const cv::Size& a, const cv::Size& b)
						{
	return a.width <= b.width && a.height <= b.width;
						}

/*-------to calculate time-------------*/

double get_wall_timelbp(){
	struct timeval time;
	if (gettimeofday(&time,NULL)){
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

/*--------------------*/
unsigned int roundUp(unsigned int value, unsigned int multiple);

struct PyrLavel
{
	PyrLavel(int _order, float _scale, cv::Size frame, cv::Size window, cv::Size minObjectSize)
	{
		do
		{
			order = _order;
			scale = pow(_scale, order);
			sFrame = frame / scale;
			workArea = sFrame - window + 1;
			sWindow = window * scale;
			_order++;
		} while (sWindow <= minObjectSize);
	}

	bool isFeasible(cv::Size maxObj)
	{
		return workArea.width > 0 && workArea.height > 0 && sWindow <= maxObj;
	}

	PyrLavel next(float factor, cv::Size frame, cv::Size window, cv::Size minObjectSize)
	{
		return PyrLavel(order + 1, factor, frame, window, minObjectSize);
	}

	int order;
	float scale;
	cv::Size sFrame;
	cv::Size workArea;
	cv::Size sWindow;
};

struct Stage
{
	int    first;
	int    ntrees;
	float  threshold;
};

struct ClNode
{
	int   left;
	int   right;
	int   featureIdx;
};

// to convert other data types to string
template <typename T>
std::string to_stringlbp(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

/*-------declaring array to hold time*/
double kernelExTime[10]={0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
string AvgkernelExTime[6][2];
string wakeupkernelExTime[2][2];
double gettime_tocalc2[10];
double readbuffertime=0.0;
int loopcnt[2]={0,0};

int testme=0;
int outerloopcount=0;
struct LbpCascade 
{
public:

	LbpCascade(){}
	~LbpCascade(){}

	unsigned int process(ocl::oclMat& image, cl_mem candidatebuffer, int outputsz, float scaleFactor, bool /*findLargestObject*/,
			bool /*visualizeInPlace*/, cv::Size minObjectSize, cv::Size maxObjectSize,
			cv::ocl::Context* clCxt,bool calTime, int it)
	{
		CV_Assert(scaleFactor > 1 && image.depth() == CV_8U);

		if (maxObjectSize == cv::Size())
			maxObjectSize = image.size();
		//cout<<"lbp block1"<<endl;
		allocateBuffers(image.size());
		cl_command_queue qu = reinterpret_cast<cl_command_queue>(clCxt->oclCommandQueue());

		unsigned int classified = 0;

		cl_mem dclassified = ocl::openCLCreateBuffer(clCxt, CL_MEM_READ_WRITE, sizeof(int));

/*		int size_of_features=features.size();
		uchar *newFeatures=(uchar*)malloc(sizeof(uchar)*size_of_features);
		for(int i=0;i<size_of_features;i++)
		{
			newFeatures[i]=features[i];
		}

		cl_mem features_host = ocl::openCLCreateBuffer(clCxt, CL_MEM_READ_ONLY, size_of_features*sizeof(uchar));*/

		clEnqueueWriteBuffer(qu, dclassified, 1, 0,sizeof(cl_int),&classified, 0, NULL, NULL);

		/*clEnqueueWriteBuffer(qu, features_host, 1, 0,
				size_of_features*sizeof(uchar),
				newFeatures, 0, NULL, NULL);*/



		PyrLavel level(0, scaleFactor, image.size(), NxM, minObjectSize);

		double kernelTime_col=0;

		int loopCount=0;
		int outloopCount=0;
		double lbp_time=0;

		//cout<<"lbp block2"<<endl;
		while (level.isFeasible(maxObjectSize))  //
		{
			int acc = level.sFrame.width + 1;
			float iniScale = level.scale;
			//integral = 0;

			cv::Size area = level.workArea;
			int step = 1 + (level.scale <= 2.f);

			int total = 0, prev  = 0;

			while (acc <= integralFactor * (image.cols + 1) && level.isFeasible(maxObjectSize))//
			{
				// create sutable matrix headers
					//	cout<<"lbp block3"<<endl;
				ocl::oclMat src  = resuzeBuffer(cv::Rect(0, 0, level.sFrame.width, level.sFrame.height));
				ocl::oclMat sint = integral(cv::Rect(prev, 0, level.sFrame.width + 1, level.sFrame.height + 1));
				//ocl::oclMat buff = integralBuffer; //no use in small local memory yuli1009

				ocl::resize(image, src, level.sFrame, 0, 0, CV_INTER_LINEAR);

//				Mat	srcHost;
//				src.download(srcHost);
//
//
//				std::stringstream strFname4;
//				printf("loop count............ %d\n",loopCount);
//				strFname4 << "Integral_host"<<loopCount<<".txt";
//				const char* filenames1=strFname4.str().c_str();
//				FILE *fp1 = fopen(filenames1, "w");
//
//				fprintf(fp1,"Columnn %2d Rows: %d\n: ", srcHost.cols,srcHost.rows);
//				for (int i = 0; i < srcHost.rows; i++)
//				{
//					fprintf(fp1,"\nrow %2d: ", i);
//					for (int j = 0; j < srcHost.cols; j++)
//					{
//						fprintf(fp1," %5d ", srcHost.at<uchar>(i,j));
//					}
//				}
//				fclose(fp1);  //comment out, save mem
				/*if(loopCount==3)
				{
				  imwrite("scaled_3.jpg", srcHost);
				}*/
				//------------small local memory begins here--------------------//
				const char * build_options = "-D STUMP_BASED=1";
				int vlen = 4; //int 4bytes??yuli1008
				int offset = src.offset / vlen;
				int pre_invalid = src.offset % vlen;
				ocl::oclMat t_sum;
				//t_sum.create(src.cols, src.rows, CV_32FC1);
				t_sum.create( src.rows,src.cols, CV_32FC1);
				int sum_offset = sint.offset / vlen;
				int dst_step = t_sum.step / vlen;
				vector<pair<size_t , const void *> > args;

					//	cout<<"lbp block4"<<endl;
#ifdef clSurfKernel
				int output_mat_step=(t_sum.step/4);
				ocl::oclMat ldata1;
				int ldata_cols=src.cols<96?96:src.cols;
				//int ldata_cols=src.cols<96?96*4:src.cols*4;


				ldata1.create(src.rows,ldata_cols, CV_32FC1);
				//cout<<"ldata1 outoput mat: "<<ldata_cols<<endl;
				ocl::oclMat ldata2;
				ldata2.create(src.rows,ldata_cols, CV_32FC1);
				//cout<<"ldata2 outoput mat: "<<ldata_cols<<endl;
				//cout<<"step outoput mat: "<<output_mat_step<<endl;

				int totalWG=1;
				//vector<pair<size_t , const void *> > args;
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&src.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&t_sum.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&output_mat_step ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&ldata1.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&ldata2.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&totalWG ));
				//size_t gt[3] = {64,src.rows,1}, lt[3] = {64, 1, 1};

				size_t gt[3] = {16,src.rows,1}, lt[3] = {16, 1, 1};

				if(calTime)
				{
					//double dtKERNL1=get_wall_timelbp();
					gettime_tocalc2[0]=get_wall_timelbp();
				}
				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan", gt, lt, args, -1, -1,NULL);
				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan_vec", gt, lt, args, -1, -1,NULL);
				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan_vec_small", gt, lt, args, -1, -1,NULL);
//				ldata1.release();
//				ldata2.release();
					//	cout<<"lbp block5"<<endl;
#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//			double dtKERNL2=get_wall_timelbp();
					gettime_tocalc2[1]=get_wall_timelbp();
					/*wakeupkernelExTime[0][0]="wakeup time for integral cols";
				wakeupkernelExTime[0][1]=to_stringlbp(dtKERNL2-dtKERNL1);*/
				}
				kernelExTime[0]=kernelExTime[0]+(gettime_tocalc2[1]-gettime_tocalc2[0]);
				ocl::oclMat XposeMat;
				XposeMat.create(src.cols,src.rows, CV_32FC1);
				//XposeMat.create(image.cols,image.rows, CV_32FC1);

				int totalWGY=roundUp(t_sum.rows, 16)/16;
				int totalWGX=roundUp(t_sum.cols, 16)/16;


				ocl::oclMat tempBuffer;
				tempBuffer.create(totalWGX*16,totalWGY*16, CV_32FC1);
				//tempBuffer.create(1,totalWGX*totalWGY*256, CV_32FC1);

				int input_mat_step=(t_sum.step/4);
				output_mat_step=(XposeMat.step/4);

				//ckKernel=BuildKernel("scan_oclmat.cl","transpose");
//				cout<<"x axis threads size : "<<roundUp(t_sum.cols, 16)<<endl;
//				cout<<"y axis threads size : "<<roundUp(t_sum.rows, 16)<<endl;
//				cout<<"x axis total WG : "<<roundUp(t_sum.cols, 16)/16<<endl;
//				cout<<"y axis total WG : "<<roundUp(t_sum.rows, 16)/16<<endl;
//
				//cout<<"output step: "<<output_mat_step<<endl;
				//cout<<"input step: "<<input_mat_step<<endl;
				args.clear();
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&t_sum.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&XposeMat.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&t_sum.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&t_sum.cols  ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&input_mat_step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&output_mat_step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&totalWGX ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&tempBuffer.data ));

				size_t gt1[3] = {roundUp(t_sum.cols, 16), roundUp(t_sum.rows, 16),1}, lt1[3] = {16, 16, 1};

				if(calTime)
				{
					//double dtKERNL1=get_wall_timelbp();
					gettime_tocalc2[2]=get_wall_timelbp();
				}

				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "transpose", gt1, lt1, args, -1, -1,NULL);

				//tempBuffer.release();
				//t_sum.release();
				//		cout<<"lbp block6"<<endl;
#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//			double dtKERNL2=get_wall_timelbp();
					gettime_tocalc2[3]=get_wall_timelbp();
					/*wakeupkernelExTime[0][0]="wakeup time for integral cols";
			wakeupkernelExTime[0][1]=to_stringlbp(dtKERNL2-dtKERNL1);*/
				}


				kernelExTime[1]=kernelExTime[1]+(gettime_tocalc2[3]-gettime_tocalc2[2]);
#endif
#ifdef DEBUG_INTEGRAL_COLS

				Mat t_sumhost;
				t_sum.download(t_sumhost);
				std::stringstream strFname3;
				//printf("loop count %d",loopCount);
//				if(chooseKernel==1)
//				strFname3 << "Integral_Cols_original"<<loopCount<<".txt";
//				else
//					strFname3 << "Integral_Cols_global"<<loopCount<<".txt";
				strFname3 << "Integral_Cols_clSurf_40scan_small_"<<loopCount<<".txt";
				const char* filenames=strFname3.str().c_str();
				FILE *fp = fopen(filenames, "w");

				fprintf(fp,"Columnn %2d Rows: %d\n: ", t_sumhost.cols,t_sumhost.rows);
				for (int i = 0; i < t_sumhost.rows; i++)
				{
					fprintf(fp,"\nrow %2d: ", i);
					for (int j = 0; j < t_sumhost.cols; j++)
					{
						fprintf(fp," %5d ", t_sumhost.at<int>(i,j));
					}
				}
				fclose(fp);  //comment out, save mem

#endif



#ifdef clSurfKernel
				output_mat_step=(XposeMat.step/4);

				int ldata_scan2_cols=XposeMat.cols<96?96:src.cols;

				//int ldata_scan2_cols=XposeMat.cols<96?96*4:src.cols*4;
				ocl::oclMat ldata12;
				ldata12.create(XposeMat.rows,ldata_scan2_cols, CV_32FC1);
				ocl::oclMat ldata22;
				ldata22.create(XposeMat.rows,ldata_scan2_cols, CV_32FC1);

				totalWG=1;

				ocl::oclMat scan2Mat;
				scan2Mat.create(XposeMat.rows, XposeMat.cols,CV_32FC1);

				args.clear();
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&XposeMat.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&scan2Mat.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&XposeMat.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&output_mat_step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&output_mat_step ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&ldata12.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&ldata22.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&totalWG ));
				//size_t gt11[3] = {64,XposeMat.rows,1}, lt11[3] = {64, 1, 1};

				size_t gt11[3] = {16,XposeMat.rows,1}, lt11[3] = {16, 1, 1};

				if( calTime)
				{
					//double dtKERNLr2=get_wall_timelbp();
					gettime_tocalc2[4]=get_wall_timelbp();
				}

				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan2", gt11, lt11, args, -1, -1,NULL);
				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan_vec2", gt11, lt11, args, -1, -1,NULL);
				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "scan_vec_small_2", gt11, lt11, args, -1, -1,NULL);


				//ldata12.release();
				//ldata22.release();
				//XposeMat.release();

#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//double dtKERNLr2=get_wall_timelbp();
					gettime_tocalc2[5]=get_wall_timelbp();
				}
				kernelExTime[2]=kernelExTime[2]+(gettime_tocalc2[5]-gettime_tocalc2[4]);

				input_mat_step=(scan2Mat.step/4);
				output_mat_step=(sint.step/4);
				totalWG=roundUp(scan2Mat.cols, 16)/16;


				totalWGY=roundUp(scan2Mat.rows, 16)/16;
				totalWGX=roundUp(scan2Mat.cols, 16)/16;
				ocl::oclMat tempBuffer_2;
				tempBuffer_2.create(totalWGY*16,totalWGX*16, CV_32FC1);

//				cout<<"x axis threads size : "<<roundUp(scan2Mat.cols, 16)<<endl;
//				cout<<"y axis threads size : "<<roundUp(scan2Mat.rows, 16)<<endl;
//				cout<<"x axis total WG : "<<roundUp(scan2Mat.cols, 16)/16<<endl;
//				cout<<"y axis total WG : "<<roundUp(scan2Mat.rows, 16)/16<<endl;
			//
				//tempBuffer_2.create(image.cols, image.rows,CV_32FC1);

				//cout<<"offset values: "<<sum_offset<<endl;
				args.clear();
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&scan2Mat.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&sint.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&scan2Mat.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&scan2Mat.cols ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&input_mat_step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&output_mat_step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&totalWG ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&tempBuffer_2.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&sum_offset ));



				size_t gt12[3] = {roundUp(scan2Mat.cols, 16), roundUp(scan2Mat.rows, 16),1}, lt12[3] = {16, 16, 1};

				if( calTime)
				{
					//double dtKERNLr2=get_wall_timelbp();
					gettime_tocalc2[6]=get_wall_timelbp();
				}

				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "transposev2", gt12, lt12, args, -1, -1,NULL);
				//tempBuffer_2.release();
				//scan2Mat.release();
					//	cout<<"lbp block7"<<endl;
#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//double dtKERNLr2=get_wall_timelbp();
					gettime_tocalc2[7]=get_wall_timelbp();
				}
				kernelExTime[3]=kernelExTime[3]+(gettime_tocalc2[7]-gettime_tocalc2[6]);

#ifdef DEBUG_INTEGRAL_ROWS
				cout<<endl;
				Mat sinthost;
				scan2Mat.download(sinthost);
				std::stringstream strFname2;
				strFname2 << "Integral_Rows_clSurf_40small_"<<loopCount<<".txt";
				const char* filename=strFname2.str().c_str();
				FILE *fp2 = fopen(filename, "w");

				fprintf(fp2,"Columnn %2d Rows: %d\n: ", sinthost.cols,sinthost.rows);
				for (int i = 0; i < sinthost.rows; i++)
				{
					fprintf(fp2,"\nrow %2d: ", i);
					for (int j = 0; j < sinthost.cols; j++)
					{
						fprintf(fp2," %5d ", sinthost.at<int>(i,j));
					}
				}
				fclose(fp2); ///comment out, save mem
#endif

#endif

				// calculate job
				int totalWidth = level.workArea.width / step;
				total += totalWidth * (level.workArea.height / step);

				// go to next pyramide level
				level = level.next(scaleFactor, image.size(), NxM, minObjectSize);
				area = level.workArea;

				step = (1 + (level.scale <= 2.f));
				prev = acc;
				acc += level.sFrame.width + 1;
				if( calTime)
				{
					loopCount++;


					testme=testme+1;
					//cout<<"inner loop counting :"<<loopcnt[0]<<" "<<testme<<endl;

				}

				clFinish(qu);
				ldata1.release();
				ldata2.release();
				tempBuffer.release();
				ldata12.release();
				ldata22.release();
				tempBuffer_2.release();


			}
			//device::lbp::classifyPyramid(image.cols, image.rows, NxM.width - 1, NxM.height - 1, iniScale, scaleFactor,
			//total, stage_mat, stage_mat.cols / sizeof(Stage), nodes_mat,
			//leaves_mat, subsets_mat, features_mat, subsetSize, candidates, dclassified.ptr<unsigned int>(), integral);
#ifdef lbpWSIZE_32

			const int block = 32; //yuli0826
			size_t localThreads[3] = { 32, 1, 1 };
			size_t globalThreads[3] = { divUp(total, block), 1, 1};
#endif

#ifdef lbpWSIZE_64

			const int block = 64; //yuli0826
			size_t localThreads[3] = { 64, 1, 1 };
			size_t globalThreads[3] = { divUp(total, block), 1, 1};
#endif

#ifdef lbpWSIZE_128

			const int block = 128; //yuli0826
			size_t localThreads[3] = { 128, 1, 1 };
			size_t globalThreads[3] = { divUp(total, block), 1, 1};
#endif

#ifdef lbpWSIZE_256

			const int block = 256; //yuli0826
			size_t localThreads[3] = { 256, 1, 1 };
			size_t globalThreads[3] = { divUp(total, block), 1, 1};
#endif



			//size_t globalThreads[3] = { 128, 1, 1};
			size_t gthread =globalThreads[0];
			int globalthreads_value=(int)(ceilf((float)total / (float)block)) * block;

			int is = (int)integral.step / sizeof(int) ;
			int NxMmiw = NxM.width - 1;
			int NxMmih = NxM.height - 1;
			int nstages = stage_mat.cols / sizeof(Stage);

			//cout<<"subsetSize: "<<subsetSize<<endl;

			vector<pair<size_t, const void *> > args;
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&stage_mat.data ));
			args.push_back ( make_pair(sizeof(cl_int) , (void *)&nstages ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&nodes_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&leaves_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&subsets_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&features_mat.data ));
			//args.push_back ( make_pair(sizeof(cl_mem) , (void *)&features_host ));
			args.push_back ( make_pair(sizeof(cl_int) , (void *)&subsetSize ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&image.cols ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&image.rows ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&NxMmiw  )); //NxM.width-1, yuli0823
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&NxMmih  )); //NxM.height-1, yuli0823
			args.push_back ( make_pair(sizeof(cl_float), (void *)&iniScale ));
			args.push_back ( make_pair(sizeof(cl_float), (void *)&scaleFactor ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&total ));
			args.push_back ( make_pair(sizeof(cl_mem) ,  (void *)&integral.data ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&is  ));
			args.push_back ( make_pair(sizeof(cl_mem)  , (void *)&candidatebuffer ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&outputsz  ));
			args.push_back ( make_pair(sizeof(cl_mem) ,  (void *)&dclassified));

			const char * build_options = "-D STUMP_BASED=1";

			if( calTime)
			{
				//double dtlbp1=get_wall_timelbp();
				gettime_tocalc2[4]=get_wall_timelbp();
			}
			//cout<<"calling lbp_cascade"<<endl;
			ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "lbp_cascade", globalThreads, localThreads, args, -1, -1, build_options);

			//			cout<<"lbp block8"<<endl;
#ifdef DEBUG_HOST_CODE


//			cout<<"integral cols number:"<<integral.cols<<endl;
//			cout<<"size of gthread"<<gthread<<endl;
//			cout<<"size of globalthreads_value"<<globalthreads_value<<endl;


#endif
#ifdef KERNEL_TIME
			clFinish(qu);
#endif
			if( calTime)
			{
				//			double dtlbp2=get_wall_timelbp();
				gettime_tocalc2[5]=get_wall_timelbp();
			}

			kernelExTime[2]=kernelExTime[2]+(gettime_tocalc2[5]-gettime_tocalc2[4]);
			lbp_time+=gettime_tocalc2[5]-gettime_tocalc2[4];


			if( calTime)
			{
				outloopCount++;
				//loopcnt[1]=loopcnt[1]+1;
				outerloopcount=outerloopcount+1;
				//cout<<"outer loop counting: "<<loopcnt[1]<<endl;
			}

		}

		//cout<<"lbp_process time: "<<lbp_time<<endl;

		if( calTime)
    	{

			gettime_tocalc2[6]=get_wall_timelbp();

		}

		clEnqueueReadBuffer(qu, dclassified, 1, 0,
				sizeof(cl_uint),
				&classified, 0, NULL, NULL);
		#ifdef KERNEL_TIME
            clFinish(qu);
        #endif
	 	if( calTime)
    	{

			gettime_tocalc2[7]=get_wall_timelbp();
			//workEnd1();
		}

 	//readbuffertime=readbuffertime+(gettime_tocalc2[7]-gettime_tocalc2[6]);
	kernelExTime[3]=kernelExTime[3]+(gettime_tocalc2[7]-gettime_tocalc2[6]);
		//cout<<"hello:";
		return classified;

	}


	bool read(const string& classifierAsXml)
	{
		FileStorage fs(classifierAsXml, FileStorage::READ);
		return fs.isOpened() ? read(fs.getFirstTopLevelNode()) : false;
	}

private:

	void allocateBuffers(cv::Size frame)
	{
		if (frame == cv::Size())
			return;

		if (resuzeBuffer.empty() || frame.width > resuzeBuffer.cols || frame.height > resuzeBuffer.rows)
		{
			resuzeBuffer.create(frame, CV_8UC1);

			integral.create(frame.height + 1, integralFactor * (frame.width + 1), CV_32SC1);

		}
	}

	bool read(const FileNode &root)
	{
		const char *GPU_CC_STAGE_TYPE       = "stageType";
		const char *GPU_CC_FEATURE_TYPE     = "featureType";
		const char *GPU_CC_BOOST            = "BOOST";
		const char *GPU_CC_LBP              = "LBP";
		const char *GPU_CC_MAX_CAT_COUNT    = "maxCatCount";
		const char *GPU_CC_HEIGHT           = "height";
		const char *GPU_CC_WIDTH            = "width";
		const char *GPU_CC_STAGE_PARAMS     = "stageParams";
		const char *GPU_CC_MAX_DEPTH        = "maxDepth";
		const char *GPU_CC_FEATURE_PARAMS   = "featureParams";
		const char *GPU_CC_STAGES           = "stages";
		const char *GPU_CC_STAGE_THRESHOLD  = "stageThreshold";
		const float GPU_THRESHOLD_EPS       = 1e-5f;
		const char *GPU_CC_WEAK_CLASSIFIERS = "weakClassifiers";
		const char *GPU_CC_INTERNAL_NODES   = "internalNodes";
		const char *GPU_CC_LEAF_VALUES      = "leafValues";
		const char *GPU_CC_FEATURES         = "features";
		const char *GPU_CC_RECT             = "rect";

		std::string stageTypeStr = (string)root[GPU_CC_STAGE_TYPE];
		CV_Assert(stageTypeStr == GPU_CC_BOOST);

		string featureTypeStr = (string)root[GPU_CC_FEATURE_TYPE];
		CV_Assert(featureTypeStr == GPU_CC_LBP);

		NxM.width =  (int)root[GPU_CC_WIDTH];
		NxM.height = (int)root[GPU_CC_HEIGHT];
		CV_Assert( NxM.height > 0 && NxM.width > 0 );

		isStumps = ((int)(root[GPU_CC_STAGE_PARAMS][GPU_CC_MAX_DEPTH]) == 1) ? true : false;
		CV_Assert(isStumps);

		FileNode fn = root[GPU_CC_FEATURE_PARAMS];
		if (fn.empty())
			return false;

		ncategories = fn[GPU_CC_MAX_CAT_COUNT];  //GPU_CC_MAX_CAT_COUNT=256;

		subsetSize = (ncategories + 31) / 32;  //
		//"int idx =  (subsets[ current_node * subsetSize + c] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"
		nodeStep = 3 + ( ncategories > 0 ? subsetSize : 1 );//


//cout<<"helllllllllllllllllo";
		fn = root[GPU_CC_STAGES];
		if (fn.empty())
			return false;

		std::vector<Stage> stages;
		stages.reserve(fn.size());

		std::vector<int> cl_trees;
		std::vector<int> cl_nodes;
		std::vector<float> cl_leaves;
		std::vector<int> subsets;

		FileNodeIterator it = fn.begin(), it_end = fn.end();
		for (size_t si = 0; it != it_end; si++, ++it )
		{
			FileNode fns = *it;
			Stage st;
			st.threshold = (float)fns[GPU_CC_STAGE_THRESHOLD] - GPU_THRESHOLD_EPS;  //why???

			fns = fns[GPU_CC_WEAK_CLASSIFIERS];
			if (fns.empty())
				return false;

			st.ntrees = (int)fns.size();
			st.first = (int)cl_trees.size();

			stages.push_back(st);// (int, int, float)

			cl_trees.reserve(stages[si].first + stages[si].ntrees);

			// weak trees
			FileNodeIterator it1 = fns.begin(), it1_end = fns.end();
			for ( ; it1 != it1_end; ++it1 )
			{
				FileNode fnw = *it1;

				FileNode internalNodes = fnw[GPU_CC_INTERNAL_NODES];
				FileNode leafValues = fnw[GPU_CC_LEAF_VALUES];
				if ( internalNodes.empty() || leafValues.empty() )
					return false;

				int nodeCount = (int)internalNodes.size()/nodeStep;
				cl_trees.push_back(nodeCount);

				cl_nodes.reserve((cl_nodes.size() + nodeCount) * 3);
				cl_leaves.reserve(cl_leaves.size() + leafValues.size());

				if( subsetSize > 0 )
					subsets.reserve(subsets.size() + nodeCount * subsetSize);

				// nodes
				FileNodeIterator iIt = internalNodes.begin(), iEnd = internalNodes.end();

				for( ; iIt != iEnd; )
				{
					cl_nodes.push_back((int)*(iIt++));
					cl_nodes.push_back((int)*(iIt++));
					cl_nodes.push_back((int)*(iIt++));

					if( subsetSize > 0 )
						for( int j = 0; j < subsetSize; j++, ++iIt )
							subsets.push_back((int)*iIt);
				}

				// leaves
				iIt = leafValues.begin(), iEnd = leafValues.end();
				for( ; iIt != iEnd; ++iIt )
					cl_leaves.push_back((float)*iIt);
			}
		}

		fn = root[GPU_CC_FEATURES];
		if( fn.empty() )
			return false;
		//std::vector<uchar> features;
		features.reserve(fn.size() * 4);
		FileNodeIterator f_it = fn.begin(), f_end = fn.end();
		for (; f_it != f_end; ++f_it)
		{
			FileNode rect = (*f_it)[GPU_CC_RECT];
			FileNodeIterator r_it = rect.begin();
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
		}

		// copy data structures on gpu
		//cl_command_queue cmd_qu = reinterpret_cast<cl_command_queue>(stage_mat.clCxt->oclCommandQueue());
		double test_upload_time1=get_wall_timelbp();
		stage_mat.upload(cv::Mat(1, (int) (stages.size() * sizeof(Stage)), CV_8UC1, (uchar*)&(stages[0]) ));
		//clFinish(cmd_qu);
		trees_mat.upload(cv::Mat(cl_trees).reshape(1,1));
		nodes_mat.upload(cv::Mat(cl_nodes).reshape(1,1));
		leaves_mat.upload(cv::Mat(cl_leaves).reshape(1,1));
		subsets_mat.upload(cv::Mat(subsets).reshape(1,1));
		features_mat.upload(cv::Mat(features).reshape(1,1));
		double test_upload_time2=get_wall_timelbp();
		//clFinish(cmd_qu);
//		//clFinish(cmd_qu);
	//	cout<<"time needed to upload stage matrix: "<<(test_upload_time2-test_upload_time1)<<endl;
return true;
	}

	enum stage { BOOST = 0 };
	enum feature { LBP = 1, HAAR = 2 };
	static const stage stageType = BOOST;
	static const feature featureType = LBP;

	cv::Size NxM;
	bool isStumps;
	int ncategories;
	int subsetSize;
	int nodeStep;

	// OpenCL representation of classifier
	ocl::oclMat stage_mat;
	ocl::oclMat trees_mat;
	ocl::oclMat nodes_mat;
	ocl::oclMat leaves_mat;
	ocl::oclMat subsets_mat;
	ocl::oclMat features_mat;

	ocl::oclMat integral;
	ocl::oclMat integralBuffer;
	ocl::oclMat resuzeBuffer;
	std::vector<uchar> features;
	static const int integralFactor = 4;
};

unsigned int roundUp(unsigned int value, unsigned int multiple) {

   unsigned int remainder = value % multiple;

   // Make the value a multiple of multiple
   if(remainder != 0) {
      value += (multiple-remainder);
   }

   return value;
}
