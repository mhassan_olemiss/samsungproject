#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/ocl/ocl.hpp"
#include <iostream>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string>
#include <fstream>
//#include <chrono>
#include "../../modules/ocl/src/lbp.cpp"

using namespace std;
using namespace cv;
//#define KERNEL_TIME
#define LOOP_NUM 50
//#define VIDEO_FRAME 100		//video to images frames

//#define WEBCAM
#define VIDEO
//#define IMAGE
//#define disabletext

//#define HD
#define qHD
//#define VGA

#define OUTPUT_VIDEO  "/lbp/Video/video_Image"

int64 work_begin = 0;
int64 work_end = 0;
double dtoclimg=0;
double dtgray=0;
double dtcvtcolor=0;
double dtresize=0;
double pr=0;
double ds=0;
double readCand=0;
double detectionTime=0;
double drawtm=0;
double gettime_tocalc[12];
static void workBegin()
{
	work_begin = getTickCount();
}
static void workEnd()
{
	work_end += (getTickCount() - work_begin);
}
static double getTime()
{
	return work_end /((double)cvGetTickFrequency() * 1000.);
}

string inputName = string("HDG1.jpg");
string videoInputName = string("inputqHD3_30minman_small.avi");
string outputName = string("HDG1_july8.jpg");
string cascadeName = string("lbpcascade_frontalface.xml");

string requiredTime[16][2];
string vid_requiredTime[16][2];


void detectLBP( Mat& img, vector<Rect>& faces,
		LbpCascade& cascade,
		double scale,
		cv::ocl::Context* clCxt,bool calTime,int it
);

void Draw(Mat& img, vector<Rect>& faces, double scale);

double get_wall_time()
{
	struct timeval time;
	if (gettimeofday(&time,NULL)){
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time()
{
	return (double)clock() / CLOCKS_PER_SEC;
}
// to convert other data types to string
template <typename T>
std::string to_string(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}



class myTickMeter
{
public:
	myTickMeter() { reset(); }
	void start() { startTime = cvGetTickCount(); }
	void stop()
	{
		int64 time = cvGetTickCount();
		if ( startTime == 0 )
			return;

		++counter;

		sumTime += ( time - startTime );
		startTime = 0;
	}

	int64 getTimeTicks() const { return sumTime; }
	double getTimeMicro() const { return (double)getTimeTicks()/cvGetTickFrequency(); }
	double getTimeMilli() const { return getTimeMicro()*1e-3; }
	double getTimeSec()   const { return getTimeMilli()*1e-3; }
	int64 getCounter() const { return counter; }

	void reset() {startTime = 0; sumTime = 0; counter = 0; }
private:
	int64 counter;
	int64 sumTime;
	int64 startTime;
};



int main()
{
	double t=get_wall_time();
	vector<ocl::Info> oclinfo;
	int devnums = ocl::getDevice(oclinfo);
	if( devnums < 1 )
	{
		std::cout << "no device found\n";
		return -1;
	}
	ocl::setBinpath("./");
	cv::ocl::Context* clCxt = ocl::Context::getContext();

	double scale = 1.0;
	bool calTime=1;
	LbpCascade impl;
	FileStorage fs(cascadeName, FileStorage::READ);
	const char *GPU_CC_LBP = "LBP";
	string featureTypeStr = (string)fs.getFirstTopLevelNode()["featureType"];
	double dtldcas1=get_wall_time();
	if (featureTypeStr == GPU_CC_LBP)
	{
		impl.read(cascadeName);
	}
	else
	{
		cerr << "ERROR: not LBP xml." << endl;
		return -1;
	}


	double dtldcas2=get_wall_time();
	requiredTime[14][0]="time to load cascade";
	requiredTime[14][1]=to_string(dtldcas2-dtldcas1);
	vid_requiredTime[14][0]="time to load cascade";
	vid_requiredTime[14][1]=to_string(dtldcas2-dtldcas1);

#ifdef WEBCAM
	bool tryflip = false;


	Mat frame, frameCopy;

	cvNamedWindow( "result", 1 );
	CvCapture* capture = 0;

	capture = cvCaptureFromCAM(0);

	double dettime=0.0;
	int frcount=0;
	if(!capture) cout << "Capture from CAM didn't work" << endl;
	if( capture )
	{
		cout << "In capture ..." << endl;
		int it=0;
		for(;;)
		{
			frcount++;
			IplImage* iplImg = cvQueryFrame( capture );
			frame = iplImg;
			if( frame.empty() )
				break;
			if( iplImg->origin == IPL_ORIGIN_TL )
				frame.copyTo( frameCopy );
			else
				flip( frame, frameCopy, 0 );

			vector<Rect> faces;
			double dtl1=get_wall_time();
			t = (double)cvGetTickCount();
			detectLBP(frameCopy, faces, impl, scale, clCxt, calTime,it);
			double dtl2=get_wall_time();
			dettime=dettime+(1/(dtl2-dtl1));
			Draw(frameCopy, faces, scale);

			if( waitKey( 10 ) >= 0 )
				goto _cleanup_;
		}
		waitKey(0);
		_cleanup_:
		cvReleaseCapture( &capture );
	}
#endif



#ifdef VIDEO
	bool tryflip = false;


	Mat frame, frameCopy;
	//Size s = Size( (int)480,
	//	  (int)800);


#ifdef HD
	Size s = Size( (int)1920,(int)1080);
#endif
#ifdef qHD
	Size s = Size( (int)1280,(int)720);
#endif
#ifdef VGA
	Size s = Size( (int)640,(int)480);
#endif

	VideoWriter out;
	out.open("./outputqHD3_July14_v1.avi",CV_FOURCC('M','J','P','G'),30,s,true);
	//out.open("./out_face.mp4",-1,30,s,true);



	CvCapture* capture = 0;

	capture = cvCaptureFromAVI( videoInputName.c_str() );
	//capture = cvCaptureFromFile( videoInputName.c_str() );
	double dettime=0.0;
	int frcount=0;
	if(!capture) cout << "Capture from AVI didn't work" << endl;
	if( capture )
	{
		cout << "In capture ..." << endl;
		int it=0;
		for(;;)
			// for(int i=1;i<2;i++)
		{
			frcount++;
			IplImage* iplImg = cvQueryFrame( capture );
			frame = iplImg;
			if( frame.empty() )
				break;
			if( iplImg->origin == IPL_ORIGIN_TL )
				frame.copyTo( frameCopy );
			else
				flip( frame, frameCopy, 0 );

			// detectAndDraw( frameCopy, cascade, nestedCascade, scale, tryflip );

			vector<Rect> faces;
			double dtl1=get_wall_time();
			t = (double)cvGetTickCount();
			detectLBP(frameCopy, faces, impl, scale, clCxt, calTime,it);
			double dtl2=get_wall_time();
			t = (double)cvGetTickCount() - t;
			//cout<<"detection time: "<<t/((double)cvGetTickFrequency()*1000.) <<endl;
			//cout<<"total kernel lbp_cascade execution"<<kernelExTime[2]<<endl;
			//cout<<"detection rate  "<<1/(dtl2-dtl1)<<"fps"<<endl;
			cout<<"frame: "<<(frcount-1)<<endl;
			dettime=dettime+(1/(dtl2-dtl1));
			Draw(frameCopy, faces, scale);
			out<<frameCopy;



			if( waitKey( 10 ) >= 0 )
				goto _cleanup_;
		}
		cout<<"avg fps "<<dettime/frcount<<endl;
		_cleanup_:
		cvReleaseCapture( &capture );
	}
#endif



#ifdef IMAGE
	double dtimre1=get_wall_time();
	Mat image = imread( inputName, 1 );
	double dtimre2=get_wall_time();
	requiredTime[6][0]="time to read image in seconds";
	requiredTime[6][1]=to_string(dtimre2-dtimre1);

	if(image.empty())
	{
		cout << "Couldn't read .jpg" << endl;
		return -1;
	}

	cout << "In image read" << endl;

	vector<Rect> faces;
	double timecalc=0;
	Mat outputImg;

	for(int i=0;i<LOOP_NUM;i++)
	{


		int it=0;
		calTime = i == 0 ? false : true;
		//calTime=1;
		double dtl1=0;
		double dtl2=0;
		if(calTime)
		{
			workBegin();
			dtl1=get_wall_time();
		}
		// cout<<"entering  detectlbp"<<endl;
		try
		{
			detectLBP(image, faces, impl, scale, clCxt,calTime,it);
		}
		catch(int r)
		{
			cout<<"problem with detectlbp<<endl";
		}
		if(calTime)
		{
			workEnd();
			dtl2=get_wall_time();
		}
		if(calTime)
		{
			timecalc=timecalc+(dtl2-dtl1);
		}
		//cout<<"2....";
	}
	cout << "average GPU time : "<<endl;
	cout << getTime() / (LOOP_NUM-1) << " ms" << endl;
	cout<<"total time for facedetect : "<<timecalc<<endl;
	cout<<"average time for facedetect : ";
	cout<<timecalc/(LOOP_NUM-1)<<"sec"<<endl;
	double dr1=get_wall_time();

	Draw(image, faces, scale);

	double dr2=get_wall_time();
	requiredTime[2][0]="time to execute draw in seconds";
	requiredTime[2][1]=to_string(dr2-dr1);

#ifdef disabletext
	cout<<"total kernel integral rows execution"<<kernelExTime[1]<<endl;
	cout<<"total kernel integral cols execution"<<kernelExTime[0]<<endl;
	cout<<"total kernel lbp_cascade execution"<<kernelExTime[2]<<endl;
	requiredTime[1][0]="time to execute detectLBP in seconds";
	requiredTime[1][1]=to_string(timecalc/(LOOP_NUM-1));
	requiredTime[10][0]="time to create ocl image matrix in seconds";
	requiredTime[10][1]=to_string(dtoclimg/(LOOP_NUM-1));
	requiredTime[9][0]="time to create smallimage image in seconds";
	requiredTime[9][1]=to_string(dtgray/(LOOP_NUM-1));
	requiredTime[7][0]="time to convert color image in seconds";
	requiredTime[7][1]=to_string(dtcvtcolor/(LOOP_NUM-1));
	requiredTime[8][0]="time to resize image in seconds..do not need it";
	requiredTime[8][1]="---------";
	requiredTime[3][0]="time to execute lbp_process in seconds";
	requiredTime[3][1]=to_string(pr/(LOOP_NUM-1));
	requiredTime[4][0]="time required to read buffer for candidates";
	requiredTime[4][1]=to_string(readCand/(LOOP_NUM-1));
	requiredTime[5][0]="time to read buffer for detection numbers";
	requiredTime[5][1]=to_string(kernelExTime[3]/(LOOP_NUM-1));
	/*requiredTime[15][0]="time to write buffer for detection numbers";
  requiredTime[15][1]=to_string(writebuffertime/(LOOP_NUM-1));*/

	requiredTime[11][0]="average kernel execution time-integral cols";
	//requiredTime[11][1]=to_string(kernelExTime[0]/testme);
	requiredTime[12][0]="average kernel execution time-integral rows";
	// requiredTime[12][1]=to_string(kernelExTime[1]/(testme));
	requiredTime[13][0]="average kernel execution time-lbp_cascade";
	// requiredTime[13][1]=to_string(kernelExTime[2]/(outerloopcount));

	double t1=get_wall_time();

	std::stringstream dtoString;
	dtoString<<t1-t;


	requiredTime[0][0]="time to execute full program in seconds";
	requiredTime[0][1]=dtoString.str();

	ofstream myfile;

	string fileName="July51.3Time_Result"+inputName.substr(0,4)+".txt";
	//string resd=fileName+inputName.substr(1,4);
	myfile.open (fileName.c_str());

	for ( int i = 0; i <16; ++i )
	{
		for (int j = 0; j < 2; j++)
		{

			myfile << requiredTime[i][j] << ";";
		}
		myfile<<endl;
	}

#ifdef KERNEL_TIME
	for ( int i = 0; i <6; ++i )
	{
		for (int j = 0; j < 2; j++)
		{

			myfile << AvgkernelExTime[i][j] << ";";
		}
		myfile<<endl;
	}
	for ( int i = 0; i <2; ++i )
	{
		for (int j = 0; j < 2; j++)
		{

			myfile << wakeupkernelExTime[i][j] << ";";
		}
		myfile<<endl;
	}
#endif
	myfile.close();
#endif
#endif
	// waitKey(0);

	return 0;
}

void detectLBP( Mat& img, vector<Rect>& faces,
		LbpCascade& cascade,
		double scale,
		cv::ocl::Context* clCxt,bool calTime, int it)
{

	cl_command_queue cmd_qu = reinterpret_cast<cl_command_queue>(clCxt->oclCommandQueue());
	//cout<<"face block1"<<endl;
	if(calTime)
	{

		gettime_tocalc[0]=get_wall_time();

	}

	ocl::oclMat image(img);

	if(calTime)
	{

		gettime_tocalc[1]=get_wall_time();

	}
	dtoclimg=dtoclimg+(gettime_tocalc[1]-gettime_tocalc[0]);


	if(calTime)
	{

		gettime_tocalc[2]=get_wall_time();

	}

	ocl::oclMat smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 ); //we dont need two mat, only one mat is 	sufficient.


	if(calTime)
	{

		gettime_tocalc[3]=get_wall_time();

	}
	dtgray=dtgray+(gettime_tocalc[3]-gettime_tocalc[2]);


	if(calTime)
	{


		gettime_tocalc[4]=get_wall_time();


	}

	ocl::cvtColor( image, smallImg, CV_BGR2GRAY );  //using of only smallIMg
	if(calTime)
	{

		gettime_tocalc[5]=get_wall_time();


	}
	// cout<<"dest  step  :"<<smallImg.step<<endl;
	// cout<<"after cvt color"<<endl;
	dtcvtcolor=dtcvtcolor+(gettime_tocalc[5]-gettime_tocalc[4]);

	//cout<<"face block2"<<endl;
	cl_mem candidatebuffer;
	int outputsz = smallImg.size().width;
	/*==========adding zero copy related codes=========*/

	candidatebuffer = openCLCreateBuffer(clCxt, CL_MEM_ALLOC_HOST_PTR, 4 * sizeof(int) * outputsz);

	// I do not need to map as we are not
	//transferring any data from host to memory

	/*=============================================*/

	if(calTime)
	{

		gettime_tocalc[8]=get_wall_time();

	}



	int detections_num = cascade.process( smallImg, candidatebuffer, outputsz,2.0, 2,0,Size(24,24), Size(0, 0), clCxt,calTime,it);

	if(calTime)
	{
		//double pr2=get_wall_time();
		gettime_tocalc[9]=get_wall_time();
		//workEnd();
	}


	pr=pr+(gettime_tocalc[9]-gettime_tocalc[8]);

	//int * candidate = (int *)malloc(4 * sizeof(int) * outputsz);//commented to implement zero copy feature


	if(calTime)
	{
		//double pr2=get_wall_time();
		gettime_tocalc[10]=get_wall_time();
		//workEnd();
	}

	//openCLReadBuffer( clCxt, candidatebuffer, candidate, 4 * sizeof(int)*outputsz );//commented to implement zero 	copy 		feature

	int *candidate = (cl_int*)clEnqueueMapBuffer(cmd_qu, candidatebuffer,CL_TRUE,CL_MAP_READ, 0, 4 * sizeof(int)*outputsz, 0, 		NULL, NULL,NULL);  //mapping the object
	//classified=*C;

	//cout<<"face block3"<<endl;
#ifdef KERNEL_TIME
	clFinish(cmd_qu);
#endif

	if(calTime)
	{
		//double pr2=get_wall_time();
		gettime_tocalc[11]=get_wall_time();
		//workEnd();
	}

	readCand=readCand+(gettime_tocalc[11]-gettime_tocalc[10]);



	for (int i = 0; i < detections_num; i++)
	{
		faces.push_back(Rect(candidate[i * 4],candidate[i * 4 + 1],candidate[i * 4 + 2],candidate[i * 4 + 3]) );

	}

	groupRectangles( faces, 4, 0.2f );//use CPU call to replace GPU call disjoin
	//4 for groupThreshold, 0.2f for grouping_eps

	clEnqueueUnmapMemObject(cmd_qu, candidatebuffer, candidate, 0, NULL, NULL); //unmapping teh object after work has been done.
	//free(candidate);

	//cout<<"face block4"<<endl;
}

void Draw(Mat& img, vector<Rect>& faces, double scale)
{
	int i = 0;
	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Point center;
		int radius;
		center.x = cvRound((r->x + r->width*0.5)*scale);
		center.y = cvRound((r->y + r->height*0.5)*scale);
		radius = cvRound((r->width + r->height)*0.25*scale);
		circle( img, center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
	}
#ifdef IMAGE
imwrite(outputName, img);
#endif
#ifdef WEBCAM
imshow("result", img);
#endif

}
