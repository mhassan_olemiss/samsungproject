#include "opencv2/objdetect/objdetect.hpp" 
#include "opencv2/highgui/highgui.hpp" 
#include "opencv2/imgproc/imgproc.hpp" 
//#include "opencv2/core/utility.hpp"
#include <sys/time.h>
#include "opencv2/highgui/highgui_c.h"
#include <string> 
#include <fstream>
#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>

using namespace std;
using namespace cv;


string requiredTime[9][2];

template <typename T>
std::string to_string(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces );
void Draw(Mat& img, vector<Rect>& faces, double scale);

double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
    return (double)clock() / CLOCKS_PER_SEC;
}

string cascadeName = "lbpcascade_frontalface.xml";
string nestedCascadeName = "haarcascade_eye_tree_eyeglasses.xml";

string inputImage="sam22.jpg";
string outputImage="sam22_out_cpu_1_ite.jpg";

int main( int argc, const char** argv )
{

	double t1=get_wall_time();
   // CvCapture* capture = 0;
    Mat frame, frameCopy, image;
   // const string scaleOpt = "--scale=";
   // size_t scaleOptLen = scaleOpt.length();
    //const string cascadeOpt = "--cascade=";
    //size_t cascadeOptLen = cascadeOpt.length();
    //const string nestedCascadeOpt = "--nested-cascade";
   // size_t nestedCascadeOptLen = nestedCascadeOpt.length();
   // const string tryFlipOpt = "--try-flip";
   // size_t tryFlipOptLen = tryFlipOpt.length();
    string inputName;
    bool tryflip = false;
    vector<Rect> faces;
    

    CascadeClassifier cascade, nestedCascade;
    double scale = 1;

    double dtldcas1=get_wall_time();
    if( !cascade.load( cascadeName ) )
    {
        cerr << "ERROR: Could not load classifier cascade" << endl;
       // help();
        return -1;
    }

  	double dtldcas2=get_wall_time();
	requiredTime[8][0]="time to load cascade"; 
	requiredTime[8][1]=to_string(dtldcas2-dtldcas1); 
  
   	double dtimread1=get_wall_time();
        image = imread( inputImage, 1 );
	double dtimread2=get_wall_time();
	requiredTime[7][0]="time to read image"; 
	requiredTime[7][1]=to_string(dtimread2-dtimread1); 

        if(image.empty()) cout << "Couldn't read image" << endl;
    


   
        //cout << "In image read" << endl;
        if( !image.empty() )
        {
		double dtdetect1=get_wall_time();

            	detect( image, cascade, nestedCascade, scale, tryflip,faces );

	    	double dtdetect2=get_wall_time();
    		requiredTime[1][0]="time to execute detect in seconds";
    		requiredTime[1][1]=to_string(dtdetect2-dtdetect1); 
 	   
		double dtdraw1=get_wall_time();

	    	Draw (image,faces,scale);

		double dtdraw2=get_wall_time();
    		requiredTime[2][0]="time to execute draw in seconds";
    		requiredTime[2][1]=to_string(dtdraw2-dtdraw1); 
	
           
        }
	else
        {
                cerr << "couldn't read image " << endl;
        }
       
	double t2=get_wall_time();
   	requiredTime[0][0]="time to execute full program in seconds";
    	requiredTime[0][1]=to_string(t2-t1);
	
	ofstream myfile;

	myfile.open ("Time_Result_CPU_1_ite.txt");
	for ( int i = 0; i <9; ++i )
	{
		for (int j = 0; j < 2; j++)
		{
		
			  myfile << requiredTime[i][j] << ";";
		}
		myfile<<endl;
	}
	myfile.close();
   
    return 0;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces )
{
    int i = 0;
    double t = 0;
   	
	double dtgray1=get_wall_time();
    
    	Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );

	double dtgray2=get_wall_time();
    	requiredTime[3][0]="time to smallimage,cvRound image in seconds";
    	requiredTime[3][1]=to_string(dtgray2-dtgray1);      
	
	double dtcvtcolor1=get_wall_time();

    	cvtColor( img, gray, COLOR_BGR2GRAY );

	double dtcvtcolor2=get_wall_time();
    	requiredTime[4][0]="time to convert image in seconds";
    	requiredTime[4][1]=to_string(dtcvtcolor2-dtcvtcolor1);      

	double dtresize1=get_wall_time();

    	resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
	
	double dtresize2=get_wall_time();
    	requiredTime[5][0]="time to resize image in seconds";
    	requiredTime[5][1]=to_string(dtresize2-dtresize1); 
    	
	//equalizeHist( smallImg, smallImg );

    
	double dtimre1=get_wall_time();

    	cascade.detectMultiScale( smallImg, faces,
        1.06, 2, 0,
        Size(24, 24) );

	double dtimre2=get_wall_time();
	requiredTime[6][0]="time to execute detectMultiscale in seconds";
    	requiredTime[6][1]=to_string(dtimre2-dtimre1); 
    	
}
void Draw(Mat& img, vector<Rect>& faces, double scale)	
{
for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++ )
    {
        Point center;
        int radius;
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( img, center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
    }
	
   
	//printf("\nnum of faces: %d\n", faces.size());
	imwrite(outputImage, img);
    //cv::imshow( "result", img );
}
