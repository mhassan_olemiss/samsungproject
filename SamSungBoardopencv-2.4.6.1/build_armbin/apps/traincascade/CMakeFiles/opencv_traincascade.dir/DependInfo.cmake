# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/apps/traincascade/HOGfeatures.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/HOGfeatures.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/boost.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/boost.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/cascadeclassifier.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/cascadeclassifier.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/features.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/features.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/haarfeatures.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/haarfeatures.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/imagestorage.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/imagestorage.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/lbpfeatures.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/lbpfeatures.cpp.o"
  "/opencv-2.4.6.1/apps/traincascade/traincascade.cpp" "/opencv-2.4.6.1/build_armbin/apps/traincascade/CMakeFiles/opencv_traincascade.dir/traincascade.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/apps/haartraining/CMakeFiles/opencv_haartraining_engine.dir/DependInfo.cmake"
  )
