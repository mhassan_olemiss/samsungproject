# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/highgui/test/test_drawing.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_drawing.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_ffmpeg.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_ffmpeg.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_fourcc.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_fourcc.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_framecount.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_framecount.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_grfmt.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_grfmt.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_gui.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_gui.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_main.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_positioning.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_positioning.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_video_io.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_video_io.cpp.o"
  "/opencv-2.4.6.1/modules/highgui/test/test_video_pos.cpp" "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_video_pos.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "HAVE_JPEG"
  "HAVE_PNG"
  "HAVE_TIFF"
  "HAVE_JASPER"
  "HAVE_OPENEXR"
  "CVAPI_EXPORTS"
  "HIGHGUI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  )
