# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/legacy/test/test_bruteforcematcher.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_bruteforcematcher.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_em.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_em.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_main.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_nearestneighbors.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_nearestneighbors.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_optflow.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_optflow.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_pyrsegmentation.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_pyrsegmentation.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_stereomatching.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_stereomatching.cpp.o"
  "/opencv-2.4.6.1/modules/legacy/test/test_subdivisions.cpp" "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_test_legacy.dir/test/test_subdivisions.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
