# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/features2d/perf/perf_batchDistance.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_perf_features2d.dir/perf/perf_batchDistance.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/perf/perf_fast.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_perf_features2d.dir/perf/perf_fast.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/perf/perf_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_perf_features2d.dir/perf/perf_main.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/perf/perf_orb.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_perf_features2d.dir/perf/perf_orb.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/perf/perf_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_perf_features2d.dir/perf/perf_precomp.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
