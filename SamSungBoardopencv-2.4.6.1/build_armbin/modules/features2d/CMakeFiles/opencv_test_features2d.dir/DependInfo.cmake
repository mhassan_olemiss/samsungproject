# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/features2d/test/test_brisk.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_brisk.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_descriptors_regression.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_descriptors_regression.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_detectors_regression.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_detectors_regression.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_fast.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_fast.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_keypoints.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_keypoints.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_main.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_matchers_algorithmic.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_matchers_algorithmic.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_mser.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_mser.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_nearestneighbors.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_nearestneighbors.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_orb.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_orb.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/features2d/test/test_rotation_and_scale_invariance.cpp" "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_rotation_and_scale_invariance.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
