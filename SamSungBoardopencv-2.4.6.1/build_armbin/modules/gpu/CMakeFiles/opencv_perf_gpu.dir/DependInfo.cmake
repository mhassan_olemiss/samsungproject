# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/gpu/perf/perf_calib3d.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_calib3d.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_core.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_core.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_denoising.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_denoising.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_features2d.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_features2d.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_filters.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_filters.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_imgproc.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_imgproc.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_labeling.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_labeling.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_main.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_matop.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_matop.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_objdetect.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_objdetect.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/perf/perf_video.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_perf_gpu.dir/perf/perf_video.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/photo/CMakeFiles/opencv_photo.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
