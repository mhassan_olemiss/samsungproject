# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/gpu/src/arithm.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/arithm.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/bgfg_gmg.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/bgfg_gmg.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/bgfg_mog.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/bgfg_mog.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/bilateral_filter.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/bilateral_filter.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/blend.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/blend.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/brute_force_matcher.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/brute_force_matcher.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/calib3d.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/calib3d.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/cascadeclassifier.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/cascadeclassifier.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/color.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/color.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/cu_safe_call.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/cu_safe_call.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/cudastream.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/cudastream.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/cuvid_video_source.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/cuvid_video_source.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/denoising.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/denoising.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/element_operations.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/element_operations.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/error.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/error.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/fast.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/fast.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/ffmpeg_video_source.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/ffmpeg_video_source.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/fgd_bgfg.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/fgd_bgfg.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/filtering.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/filtering.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/frame_queue.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/frame_queue.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/gftt.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/gftt.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/global_motion.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/global_motion.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/graphcuts.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/graphcuts.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/hog.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/hog.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/hough.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/hough.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/imgproc.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/imgproc.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/match_template.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/match_template.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/matrix_operations.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/matrix_operations.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/matrix_reductions.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/matrix_reductions.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/mssegmentation.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/mssegmentation.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/optflowbm.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/optflowbm.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/optical_flow.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/optical_flow.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/optical_flow_farneback.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/optical_flow_farneback.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/orb.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/orb.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/pyramids.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/pyramids.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/pyrlk.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/pyrlk.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/remap.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/remap.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/resize.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/resize.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/speckle_filtering.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/speckle_filtering.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/split_merge.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/split_merge.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/stereobm.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/stereobm.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/stereobp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/stereobp.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/stereocsbp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/stereocsbp.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/thread_wrappers.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/thread_wrappers.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/tvl1flow.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/tvl1flow.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/video_decoder.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/video_decoder.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/video_parser.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/video_parser.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/video_reader.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/video_reader.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/video_writer.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/video_writer.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/src/warp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/src/warp.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_gpu.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_gpu.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_gpu.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_gpu.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/photo/CMakeFiles/opencv_photo.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  )
