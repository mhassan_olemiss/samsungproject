# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/gpu/test/main.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/main.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_bgfg.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_bgfg.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_calib3d.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_calib3d.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_color.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_color.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_copy_make_border.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_copy_make_border.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_core.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_core.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_denoising.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_denoising.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_features2d.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_features2d.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_filters.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_filters.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_global_motion.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_global_motion.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_gpumat.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_gpumat.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_hough.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_hough.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_imgproc.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_imgproc.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_labeling.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_labeling.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_nvidia.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_nvidia.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_objdetect.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_objdetect.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_opengl.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_opengl.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_optflow.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_optflow.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_pyramids.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_pyramids.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_remap.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_remap.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_resize.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_resize.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_stream.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_stream.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_threshold.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_threshold.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_video.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_video.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_warp_affine.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_warp_affine.cpp.o"
  "/opencv-2.4.6.1/modules/gpu/test/test_warp_perspective.cpp" "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_test_gpu.dir/test/test_warp_perspective.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/photo/CMakeFiles/opencv_photo.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
