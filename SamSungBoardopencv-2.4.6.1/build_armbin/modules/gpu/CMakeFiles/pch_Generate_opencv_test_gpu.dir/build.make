# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opencv-2.4.6.1

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opencv-2.4.6.1/build_armbin

# Utility rule file for pch_Generate_opencv_test_gpu.

# Include the progress variables for this target.
include modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/progress.make

modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu: modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch

modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch: ../modules/gpu/test/test_precomp.hpp
modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch: modules/gpu/test_precomp.hpp
modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch: lib/libopencv_test_gpu_pch_dephelp.a
	$(CMAKE_COMMAND) -E cmake_progress_report /opencv-2.4.6.1/build_armbin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating test_precomp.hpp.gch/opencv_test_gpu_Release.gch"
	cd /opencv-2.4.6.1/build_armbin/modules/gpu && /usr/bin/cmake -E make_directory /opencv-2.4.6.1/build_armbin/modules/gpu/test_precomp.hpp.gch
	cd /opencv-2.4.6.1/build_armbin/modules/gpu && /usr/bin/c++ -O3 -DNDEBUG -DNDEBUG -I"/opencv-2.4.6.1/modules/gpu/test" -I"/opencv-2.4.6.1/modules/features2d/include" -I"/opencv-2.4.6.1/modules/highgui/include" -I"/opencv-2.4.6.1/modules/flann/include" -I"/opencv-2.4.6.1/modules/imgproc/include" -I"/opencv-2.4.6.1/modules/core/include" -I"/opencv-2.4.6.1/modules/ts/include" -I"/opencv-2.4.6.1/modules/gpu/include" -I"/opencv-2.4.6.1/modules/legacy/include" -I"/opencv-2.4.6.1/modules/ml/include" -I"/opencv-2.4.6.1/modules/photo/include" -I"/opencv-2.4.6.1/modules/video/include" -I"/opencv-2.4.6.1/modules/objdetect/include" -I"/opencv-2.4.6.1/modules/calib3d/include" -isystem"/opencv-2.4.6.1/build_armbin/modules/gpu" -I"/opencv-2.4.6.1/modules/gpu/src" -I"/opencv-2.4.6.1/modules/gpu/src/cuda" -isystem"/opencv-2.4.6.1/build_armbin" -DHAVE_CVCONFIG_H -DCVAPI_EXPORTS -DHAVE_CVCONFIG_H -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -x c++-header -o /opencv-2.4.6.1/build_armbin/modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch /opencv-2.4.6.1/build_armbin/modules/gpu/test_precomp.hpp

modules/gpu/test_precomp.hpp: ../modules/gpu/test/test_precomp.hpp
	$(CMAKE_COMMAND) -E cmake_progress_report /opencv-2.4.6.1/build_armbin/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating test_precomp.hpp"
	cd /opencv-2.4.6.1/build_armbin/modules/gpu && /usr/bin/cmake -E copy /opencv-2.4.6.1/modules/gpu/test/test_precomp.hpp /opencv-2.4.6.1/build_armbin/modules/gpu/test_precomp.hpp

pch_Generate_opencv_test_gpu: modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu
pch_Generate_opencv_test_gpu: modules/gpu/test_precomp.hpp.gch/opencv_test_gpu_Release.gch
pch_Generate_opencv_test_gpu: modules/gpu/test_precomp.hpp
pch_Generate_opencv_test_gpu: modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/build.make
.PHONY : pch_Generate_opencv_test_gpu

# Rule to build all files generated by this target.
modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/build: pch_Generate_opencv_test_gpu
.PHONY : modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/build

modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/clean:
	cd /opencv-2.4.6.1/build_armbin/modules/gpu && $(CMAKE_COMMAND) -P CMakeFiles/pch_Generate_opencv_test_gpu.dir/cmake_clean.cmake
.PHONY : modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/clean

modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/depend:
	cd /opencv-2.4.6.1/build_armbin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opencv-2.4.6.1 /opencv-2.4.6.1/modules/gpu /opencv-2.4.6.1/build_armbin /opencv-2.4.6.1/build_armbin/modules/gpu /opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : modules/gpu/CMakeFiles/pch_Generate_opencv_test_gpu.dir/depend

