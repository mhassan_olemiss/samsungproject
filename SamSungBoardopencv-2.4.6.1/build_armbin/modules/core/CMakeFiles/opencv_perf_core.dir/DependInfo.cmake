# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/core/perf/perf_abs.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_abs.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_addWeighted.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_addWeighted.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_arithm.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_arithm.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_bitwise.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_bitwise.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_compare.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_compare.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_convertTo.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_convertTo.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_dft.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_dft.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_dot.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_dot.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_inRange.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_inRange.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_main.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_mat.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_mat.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_math.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_math.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_merge.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_merge.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_minmaxloc.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_minmaxloc.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_norm.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_norm.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_reduce.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_reduce.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_split.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_split.cpp.o"
  "/opencv-2.4.6.1/modules/core/perf/perf_stat.cpp" "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_stat.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  )
