# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/build_armbin/modules/ocl/kernels.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/kernels.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/arithm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/arithm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/blend.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/blend.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/brute_force_matcher.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/brute_force_matcher.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/build_warps.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/build_warps.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/canny.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/canny.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/color.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/color.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/columnsum.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/columnsum.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/error.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/error.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/fft.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/fft.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/filtering.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/filtering.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/gemm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/gemm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/gfft.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/gfft.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/haar.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/haar.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/hog.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/hog.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/imgproc.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/imgproc.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/initialization.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/initialization.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/interpolate_frames.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/interpolate_frames.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/lbp.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/lbp.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/match_template.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/match_template.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/matrix_operations.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/matrix_operations.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/mcwutil.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/mcwutil.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/moments.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/moments.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/mssegmentation.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/mssegmentation.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/optical_flow_farneback.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/optical_flow_farneback.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/pyrdown.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/pyrdown.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/pyrlk.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/pyrlk.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/pyrup.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/pyrup.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/split_merge.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/split_merge.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/stereo_csbp.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/stereo_csbp.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/stereobm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/stereobm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/stereobp.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/stereobp.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/src/tvl1flow.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/src/tvl1flow.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_ocl.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_ocl.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_ocl.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_ocl.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  )
