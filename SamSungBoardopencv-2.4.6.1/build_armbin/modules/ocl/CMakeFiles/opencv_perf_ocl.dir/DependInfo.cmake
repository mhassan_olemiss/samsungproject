# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/ocl/perf/main.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/main.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_arithm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_arithm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_blend.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_blend.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_brute_force_matcher.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_brute_force_matcher.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_calib3d.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_calib3d.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_canny.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_canny.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_color.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_color.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_fft.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_fft.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_filters.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_filters.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_gemm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_gemm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_haar.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_haar.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_hog.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_hog.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_imgproc.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_imgproc.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_match_template.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_match_template.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_matrix_operation.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_matrix_operation.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_moments.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_moments.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_norm.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_norm.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_opticalflow.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_opticalflow.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_pyramid.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_pyramid.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/perf_split_merge.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/perf_split_merge.cpp.o"
  "/opencv-2.4.6.1/modules/ocl/perf/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_perf_ocl.dir/perf/precomp.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
