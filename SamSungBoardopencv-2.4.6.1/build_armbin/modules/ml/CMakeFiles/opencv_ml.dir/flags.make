# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# compile CXX with /usr/bin/c++
CXX_FLAGS =    -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -O3 -DNDEBUG  -DNDEBUG -fPIC -I/opencv-2.4.6.1/modules/ml/test -I/opencv-2.4.6.1/modules/features2d/include -I/opencv-2.4.6.1/modules/highgui/include -I/opencv-2.4.6.1/modules/flann/include -I/opencv-2.4.6.1/modules/imgproc/include -I/opencv-2.4.6.1/modules/core/include -I/opencv-2.4.6.1/modules/ts/include -I/opencv-2.4.6.1/modules/ml/include -I/opencv-2.4.6.1/build_armbin/modules/ml -I/opencv-2.4.6.1/modules/ml/src -I/opencv-2.4.6.1/build_armbin   

CXX_DEFINES = -Dopencv_ml_EXPORTS -DHAVE_CVCONFIG_H -DCVAPI_EXPORTS

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/inner_functions.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/boost.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/svm.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/rtrees.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/cnn.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/testset.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/data.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/knearest.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/nbayes.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/ann_mlp.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/precomp.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/estimate.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/em.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/tree.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/ertrees.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/ml_init.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/ml/CMakeFiles/opencv_ml.dir/src/gbt.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/ml/precomp.hpp" -Winvalid-pch 

