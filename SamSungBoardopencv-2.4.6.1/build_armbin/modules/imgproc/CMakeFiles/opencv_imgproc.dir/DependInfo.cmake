# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/imgproc/src/accum.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/accum.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/approx.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/approx.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/canny.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/canny.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/clahe.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/clahe.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/color.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/color.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/contours.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/contours.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/convhull.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/convhull.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/corner.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/corner.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/cornersubpix.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/cornersubpix.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/deriv.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/deriv.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/distransform.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/distransform.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/emd.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/emd.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/featureselect.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/featureselect.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/filter.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/filter.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/floodfill.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/floodfill.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/gabor.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/gabor.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/generalized_hough.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/generalized_hough.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/geometry.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/geometry.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/grabcut.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/grabcut.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/histogram.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/histogram.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/hough.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/hough.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/imgwarp.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/imgwarp.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/linefit.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/linefit.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/matchcontours.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/matchcontours.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/moments.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/moments.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/morph.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/morph.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/phasecorr.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/phasecorr.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/pyramids.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/pyramids.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/rotcalipers.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/rotcalipers.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/samplers.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/samplers.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/segmentation.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/segmentation.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/shapedescr.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/shapedescr.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/smooth.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/smooth.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/subdivision2d.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/subdivision2d.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/sumpixels.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/sumpixels.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/tables.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/tables.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/templmatch.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/templmatch.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/thresh.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/thresh.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/undistort.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/undistort.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/src/utils.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/src/utils.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_imgproc.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_imgproc.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_imgproc.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_imgproc.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  )
