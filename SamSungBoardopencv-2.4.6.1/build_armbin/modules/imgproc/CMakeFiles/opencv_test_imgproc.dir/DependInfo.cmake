# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/imgproc/test/test_approxpoly.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_approxpoly.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_bilateral_filter.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_bilateral_filter.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_boundingrect.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_boundingrect.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_canny.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_canny.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_color.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_color.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_contours.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_contours.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_convhull.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_convhull.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_cvtyuv.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_cvtyuv.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_distancetransform.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_distancetransform.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_emd.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_emd.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_filter.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_filter.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_floodfill.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_floodfill.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_grabcut.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_grabcut.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_histograms.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_histograms.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_houghLines.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_houghLines.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_imgwarp.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_imgwarp.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_imgwarp_strict.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_imgwarp_strict.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_main.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_moments.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_moments.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_pc.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_pc.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_templmatch.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_templmatch.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_thresh.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_thresh.cpp.o"
  "/opencv-2.4.6.1/modules/imgproc/test/test_watershed.cpp" "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_test_imgproc.dir/test/test_watershed.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  )
