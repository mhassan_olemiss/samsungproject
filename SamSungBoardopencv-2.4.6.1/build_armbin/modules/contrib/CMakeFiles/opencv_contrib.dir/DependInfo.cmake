# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/contrib/src/adaptiveskindetector.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/adaptiveskindetector.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/ba.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/ba.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/basicretinafilter.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/basicretinafilter.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/bowmsctrainer.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/bowmsctrainer.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/chamfermatching.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/chamfermatching.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/chowliutree.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/chowliutree.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/colormap.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/colormap.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/colortracker.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/colortracker.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/contrib_init.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/contrib_init.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/detection_based_tracker.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/detection_based_tracker.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/facerec.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/facerec.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/featuretracker.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/featuretracker.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/fuzzymeanshifttracker.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/fuzzymeanshifttracker.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/gencolors.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/gencolors.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/hybridtracker.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/hybridtracker.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/imagelogpolprojection.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/imagelogpolprojection.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/inputoutput.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/inputoutput.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/lda.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/lda.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/logpolar_bsm.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/logpolar_bsm.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/magnoretinafilter.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/magnoretinafilter.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/octree.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/octree.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/openfabmap.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/openfabmap.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/parvoretinafilter.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/parvoretinafilter.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/polyfit.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/polyfit.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/retina.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/retina.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/retinacolor.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/retinacolor.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/retinafilter.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/retinafilter.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/rgbdodometry.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/rgbdodometry.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/selfsimilarity.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/selfsimilarity.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/spinimages.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/spinimages.cpp.o"
  "/opencv-2.4.6.1/modules/contrib/src/stereovar.cpp" "/opencv-2.4.6.1/build_armbin/modules/contrib/CMakeFiles/opencv_contrib.dir/src/stereovar.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_contrib.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_contrib.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_contrib.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_contrib.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  )
