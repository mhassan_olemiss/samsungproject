# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/stitching/src/autocalib.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/autocalib.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/blenders.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/blenders.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/camera.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/camera.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/exposure_compensate.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/exposure_compensate.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/matchers.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/matchers.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/motion_estimators.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/motion_estimators.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/seam_finders.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/seam_finders.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/stitcher.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/stitcher.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/util.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/util.cpp.o"
  "/opencv-2.4.6.1/modules/stitching/src/warpers.cpp" "/opencv-2.4.6.1/build_armbin/modules/stitching/CMakeFiles/opencv_stitching.dir/src/warpers.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_stitching.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_stitching.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_stitching.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_stitching.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/photo/CMakeFiles/opencv_photo.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/legacy/CMakeFiles/opencv_legacy.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/gpu/CMakeFiles/opencv_gpu.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ocl/CMakeFiles/opencv_ocl.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/nonfree/CMakeFiles/opencv_nonfree.dir/DependInfo.cmake"
  )
