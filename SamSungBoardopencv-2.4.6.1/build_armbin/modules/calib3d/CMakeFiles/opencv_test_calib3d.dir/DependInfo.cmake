# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/calib3d/test/test_affine3d_estimator.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_affine3d_estimator.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_cameracalibration.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_cameracalibration.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_cameracalibration_artificial.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_cameracalibration_artificial.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_cameracalibration_badarg.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_cameracalibration_badarg.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_chessboardgenerator.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_chessboardgenerator.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_chesscorners.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_chesscorners.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_chesscorners_badarg.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_chesscorners_badarg.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_chesscorners_timing.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_chesscorners_timing.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_compose_rt.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_compose_rt.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_cornerssubpix.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_cornerssubpix.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_fundam.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_fundam.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_homography.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_homography.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_main.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_main.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_modelest.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_modelest.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_posit.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_posit.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_precomp.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_reproject_image_to_3d.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_reproject_image_to_3d.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_solvepnp_ransac.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_solvepnp_ransac.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_stereomatching.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_stereomatching.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_undistort.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_undistort.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_undistort_badarg.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_undistort_badarg.cpp.o"
  "/opencv-2.4.6.1/modules/calib3d/test/test_undistort_points.cpp" "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_test_calib3d.dir/test/test_undistort_points.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  )
