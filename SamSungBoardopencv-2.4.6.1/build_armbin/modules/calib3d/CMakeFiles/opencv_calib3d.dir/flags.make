# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# compile CXX with /usr/bin/c++
CXX_FLAGS =    -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -O3 -DNDEBUG  -DNDEBUG -fPIC -I/opencv-2.4.6.1/modules/calib3d/perf -I/opencv-2.4.6.1/modules/features2d/include -I/opencv-2.4.6.1/modules/highgui/include -I/opencv-2.4.6.1/modules/flann/include -I/opencv-2.4.6.1/modules/imgproc/include -I/opencv-2.4.6.1/modules/core/include -I/opencv-2.4.6.1/modules/ts/include -I/opencv-2.4.6.1/modules/calib3d/include -I/opencv-2.4.6.1/build_armbin/modules/calib3d -I/opencv-2.4.6.1/modules/calib3d/src -I/opencv-2.4.6.1/modules/calib3d/test -I/opencv-2.4.6.1/build_armbin   

CXX_DEFINES = -Dopencv_calib3d_EXPORTS -DHAVE_CVCONFIG_H -DCVAPI_EXPORTS

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/p3p.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/calibinit.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/modelest.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/polynom_solver.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/triangulate.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/fundam.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/stereosgbm.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/stereobm.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/posit.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/calibration.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/quadsubpix.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/epnp.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/precomp.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/checkchessboard.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/circlesgrid.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/calib3d/CMakeFiles/opencv_calib3d.dir/src/solvepnp.cpp.o_FLAGS =  -include "/opencv-2.4.6.1/build_armbin/modules/calib3d/precomp.hpp" -Winvalid-pch 

