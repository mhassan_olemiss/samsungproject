# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opencv-2.4.6.1/modules/objdetect/src/cascadedetect.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/cascadedetect.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/datamatrix.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/datamatrix.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/distancetransform.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/distancetransform.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/featurepyramid.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/featurepyramid.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/fft.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/fft.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/haar.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/haar.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/hog.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/hog.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/latentsvm.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/latentsvm.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/latentsvmdetector.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/latentsvmdetector.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/linemod.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/linemod.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/lsvmparser.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/lsvmparser.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/lsvmtbbversion.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/lsvmtbbversion.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/matching.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/matching.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/objdetect_init.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/objdetect_init.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/precomp.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/precomp.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/resizeimg.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/resizeimg.cpp.o"
  "/opencv-2.4.6.1/modules/objdetect/src/routine.cpp" "/opencv-2.4.6.1/build_armbin/modules/objdetect/CMakeFiles/opencv_objdetect.dir/src/routine.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CVCONFIG_H"
  "CVAPI_EXPORTS"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_objdetect.so" "/opencv-2.4.6.1/build_armbin/lib/libopencv_objdetect.so.2.4.6"
  "/opencv-2.4.6.1/build_armbin/lib/libopencv_objdetect.so.2.4" "/opencv-2.4.6.1/build_armbin/lib/libopencv_objdetect.so.2.4.6"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opencv-2.4.6.1/build_armbin/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/opencv-2.4.6.1/build_armbin/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  )
