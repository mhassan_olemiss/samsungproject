#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/ocl/ocl.hpp"
#include <iostream>
#include <stdio.h>
#include <sys/time.h>
//#include "C:\opencv\modules\ocl\src\lbp.cpp"
#include <sstream>

using namespace std;
using namespace cv;

//#define LOOP_NUM 100		//repeat times when deal with image
//#define VIDEO_FRAME 20		//video to images frames
//#define VIDEO				//video or image, important!!!

//#define HAAR				//HAAR OpenCL, LBP OpenCL or LBP CPU, important!!!
//#define LBP
#define USECPU

#define DEVICE 0			//set device to use, depends on how many graphics cards you have

string outputName = string("sam25_outli.jpg");
string inputName =  string("sam25.jpg");;
#ifdef HAAR
	string cascadeName = string("c:\\opencv\\data\\haarcascades\\haarcascade_frontalface_alt.xml");
	ocl::OclCascadeClassifierBuf face_cascade;
#else
	string cascadeName = string("lbpcascade_frontalface.xml");
#endif
//#define OUTPUT_VIDEO  "c:\\opencv\\data\\video_Image"		//pre_processed video

//below for image speed calculation
int64 work_begin = 0;
int64 work_end = 0;
double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
    return (double)clock() / CLOCKS_PER_SEC;
}
/*
static void workBegin()
{
    work_begin = getTickCount();
}
static void workEnd()
{
    work_end += (getTickCount() - work_begin);
}
static double getTime()
{
    return work_end /((double)cvGetTickFrequency() * 1000.);
}
    
//below for video FPS calculation
//comes from C:\opencv\modules\contrib\include\opencv2\contrib\contrib.hpp
//and C:\opencv\modules\contrib\src\spinimages.cpp
class myTickMeter	
{
public:
    myTickMeter() { reset(); }
    void start() { startTime = cvGetTickCount(); }
    void stop()
	{
		int64 time = cvGetTickCount();
		if ( startTime == 0 )
			return;

		++counter;

		sumTime += ( time - startTime );
		startTime = 0;
	}

    int64 getTimeTicks() const { return sumTime; }
    double getTimeMicro() const { return (double)getTimeTicks()/cvGetTickFrequency(); }
    double getTimeMilli() const { return getTimeMicro()*1e-3; }
    double getTimeSec()   const { return getTimeMilli()*1e-3; }
    int64 getCounter() const { return counter; }

    void reset() {startTime = 0; sumTime = 0; counter = 0; }
private:
    int64 counter;
    int64 sumTime;
    int64 startTime;
};*/



/*void detect( Mat& img, vector<Rect>& faces,
             ocl::OclCascadeClassifierBuf& cascade,
             double scale);
void detectLBP( Mat& img, vector<Rect>& faces,
             LbpCascade& cascade,
             double scale, ocl::Context* clCxt, bool calTime);*/

void Draw(Mat& img, vector<Rect>& faces, double scale);

int main()
{
    /*vector<ocl::Info> oclinfo;
    int devnums = ocl::getDevice(oclinfo);
    if( devnums < 1 )
    {
        std::cout << "no device found\n";
        return -1;
    }
	cout << "Devices nums:" << devnums << endl;
	for (int i = 0; i < devnums; i++)
	{	cout << i << ": " << oclinfo[i].DeviceName[0] << endl; 	}
    //if you want to use undefault device, set it here
    setDevice(oclinfo[DEVICE]);
	ocl::Context* clCxt = ocl::Context::getContext();

	cout << "< " << oclinfo[DEVICE].DeviceName[0] << 
		" > CL_DEVICE_MAX_COMPUTE_UNITS = " << clCxt->computeUnits() << endl;
	//cout << "CL_DEVICE_MAX_WORK_GROUP_SIZE" << clCxt-> << endl;
	//cout << "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS" << clCxt->computeUnits() << endl;
	//cout << "CL_DEVICE_MAX_WORK_ITEM_SIZES" << clCxt->computeUnits() << endl;
 
    ocl::setBinpath("./");*/

	double scale = 1.0;
	//bool calTime;			//the first gpu call is not counted 

#ifdef HAAR
	Mat frame, frameCopy;
    if( !face_cascade.load( cascadeName ))
    {
        cerr << "ERROR: Could not load classifier cascade" << endl;
        return -1;
    }
#endif
#ifdef LBP
	LbpCascade impl;
	FileStorage fs(cascadeName, FileStorage::READ);
    const char *GPU_CC_LBP = "LBP";
    string featureTypeStr = (string)fs.getFirstTopLevelNode()["featureType"];
    if (featureTypeStr == GPU_CC_LBP)
	{
		impl.read(cascadeName);
	}
	else
	{
		cerr << "ERROR: not LBP xml." << endl;
		return -1;
	}
#endif
#ifdef USECPU
    Mat cframe, cframe_gray;
	CascadeClassifier cface_cascade;
    std::vector<Rect> cfaces;
    //-- 1. Load the cascade
    if( !cface_cascade.load( cascadeName ) ){ printf("--(!)Error loading face cascade\n"); return -1; };
#endif

#ifdef VIDEO
	char str[5];
	string str2;
	int i;
	for (i = 0; i < VIDEO_FRAME; i++)
	{
		myTickMeter tm;			//for FPS measure, yuli1023
		tm.start ();

		//calTime = i == 0 ? false : true;
		sprintf(str,"%04d",i);
		str2 = str;
		Mat image = imread(OUTPUT_VIDEO + str2 + ".jpg");
		if( image.empty() )
		{
			cout << "Couldn't read .jpg" << endl;
			return -1;
		}
		if (i == 0) cout << "In video read" << endl;
		//if(calTime) workBegin();
		vector<Rect> faces;
#ifdef HAAR
		//detect(image, faces, face_cascade, scale);
#endif
#ifdef LBP
        //detectLBP(image, faces, impl, scale, clCxt, calTime);
#endif
#ifdef USECPU
		Mat cimage_gray;
	    cvtColor( image, cimage_gray, COLOR_BGR2GRAY );
		equalizeHist( cimage_gray, cimage_gray );
		double dtimre1=get_wall_time();
	    cface_cascade.detectMultiScale( cimage_gray, faces, 1.2, 0, 0, Size(0, 0) );
double dtimre2=get_wall_time();
#endif

		//if(calTime) workEnd();
		tm.stop();
		double detectionTime = tm.getTimeMilli();
		double fps = 1000 / detectionTime;
		cout << "\n" <<  fps << " FPS. \n" ;

		Draw(image, faces, scale);

 		int c = waitKey(10);
		if( (char)c == 'c' ) { break; }
	}
	//cout << "average time (noCamera) : ";
	//cout << getTime() / i << " ms" << endl;

#else

	Mat image = imread( inputName, 1 );
    if(image.empty())
	{
        cout << "Couldn't read .jpg" << endl;
		return -1;
	}
    cout << "In image read" << endl;
    vector<Rect> faces;
   // for(int i = 0; i <= LOOP_NUM; i ++)
  //  {
		//calTime = i == 0 ? false : true;
		//if(calTime) workBegin();
#ifdef HAAR
		detect(image, faces, face_cascade, scale);
#endif
#ifdef LBP
        detectLBP(image, faces, impl, scale, clCxt, calTime);
#endif
#ifdef USECPU
		Mat cimage_gray;
	    cvtColor( image, cimage_gray, COLOR_BGR2GRAY );
		equalizeHist( cimage_gray, cimage_gray );
	double dtimre1=get_wall_time();
	    cface_cascade.detectMultiScale( cimage_gray, faces, 1.2, 2, 0, Size(0, 0) );
double dtimre2=get_wall_time();
double requiredTime=dtimre2-dtimre1;
   
    printf( "detection time = %f sec\n", requiredTime );
#endif

		//if(calTime) workEnd();
   // }
   /* cout << "average GPU time (noCamera) : ";
    cout << getTime() / LOOP_NUM << " ms" << endl;
    cout << "getTime2: " << getTime2() / LOOP_NUM << " ms" << endl;*/
    Draw(image, faces, scale);
    waitKey(0);

#endif

	//cvDestroyWindow("result");
    return 0;
}

/*void detect( Mat& img, vector<Rect>& faces,
             ocl::OclCascadeClassifierBuf& cascade,
             double scale)
{
    ocl::oclMat image(img);
    ocl::oclMat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    ocl::cvtColor( image, gray, CV_BGR2GRAY );
    ocl::resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    ocl::equalizeHist( smallImg, smallImg );

    cascade.detectMultiScale( smallImg, faces, 1.1,
                              3, 0
                              |CV_HAAR_SCALE_IMAGE
                              , Size(30,30), Size(0, 0) );
   //cascade.detectMultiScale( smallImg, faces, 1.06, 2, 0, Size(24, 24) );   


}*/

/*void detectLBP( Mat& img, vector<Rect>& faces,
             LbpCascade& cascade,
             double scale,  ocl::Context* clCxt, bool calTime )
{
    ocl::oclMat image(img);
    ocl::oclMat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    ocl::cvtColor( image, gray, CV_BGR2GRAY );

	ocl::oclMat facesbuff;

	int detections_num = cascade.process( gray, facesbuff, 1.2, 4, 0,0,Size(0,0), Size(0, 0), clCxt, calTime );
    Mat faces_downloaded;
	facesbuff.download(faces_downloaded);
	faces = Mat_<Rect>(faces_downloaded.colRange(0,detections_num));
}*/

void Draw(Mat& img, vector<Rect>& faces, double scale)
{
    int i = 0;
    for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
    {
        Point center;
        int radius;
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( img, center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
    }
   // imshow( "result", img );
	imwrite(outputName, img);

	//text output, yuli0820
	printf("num of faces: %d\n", faces.size());
	for( int i = 0; i < (int)(faces.size()); i++ )
	{
		printf("x,y:(%d, %d) width(height)=%d\n", faces[i].x, faces[i].y, faces[i].width, faces[i].height);
	} 
}

