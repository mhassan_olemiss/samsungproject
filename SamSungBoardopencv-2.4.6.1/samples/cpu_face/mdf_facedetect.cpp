#include "opencv2/objdetect/objdetect.hpp" 
#include "opencv2/highgui/highgui.hpp" 
#include "opencv2/imgproc/imgproc.hpp" 
//#include "opencv2/core/utility.hpp"
#include <sys/time.h>
#include "opencv2/highgui/highgui_c.h"
#include <string> 
#include <fstream>
#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>

using namespace std;
using namespace cv;
#define LOOP_NUM 100
//#define VIDEO_FRAME 100		//video to images frames
//#define VIDEO
//#define VIDEO_old
#define IMAGE
#define OUTPUT_VIDEO  "/cpu_face/Video/video_Image"	

string requiredTime[9][2];
string vid_requiredTime[8][2];
int64 work_begin = 0;
int64 work_end = 0;

double dtoclimg=0;
double dtgray=0;
double dtcvtcolor=0;
double dtresize=0;
double pr=0;
double ds=0;
double detectionTime=0;
double drawtm=0;
double gettime_tocalc[8];

template <typename T>
std::string to_string(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces,bool calTime );
void Draw(Mat& img, vector<Rect>& faces, double scale);

double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
    return (double)clock() / CLOCKS_PER_SEC;
}

string cascadeName = "lbpcascade_frontalface.xml";
string nestedCascadeName = "haarcascade_eye_tree_eyeglasses.xml";

string inputImage="Input/sam22.jpg";
string outputImage="Output/sam22-out.jpg";
string videoInputName = string("m.avi");


class myTickMeter	
{
public:
    myTickMeter() { reset(); }
    void start() { startTime = cvGetTickCount(); }
    void stop()
	{
		int64 time = cvGetTickCount();
		if ( startTime == 0 )
			return;

		++counter;

		sumTime += ( time - startTime );
		startTime = 0;
	}

    int64 getTimeTicks() const { return sumTime; }
    double getTimeMicro() const { return (double)getTimeTicks()/cvGetTickFrequency(); }
    double getTimeMilli() const { return getTimeMicro()*1e-3; }
    double getTimeSec()   const { return getTimeMilli()*1e-3; }
    int64 getCounter() const { return counter; }

    void reset() {startTime = 0; sumTime = 0; counter = 0; }
private:
    int64 counter;
    int64 sumTime;
    int64 startTime;
};




int main( int argc, const char** argv )
{

	double t1=get_wall_time();
	double tf1=get_wall_time();
	bool calTime;
	double timecalc=0;
   	// CvCapture* capture = 0;
    	Mat frame, frameCopy, image;
   	// const string scaleOpt = "--scale=";
   	// size_t scaleOptLen = scaleOpt.length();
   	//const string cascadeOpt = "--cascade=";
    	//size_t cascadeOptLen = cascadeOpt.length();
    	//const string nestedCascadeOpt = "--nested-cascade";
   	// size_t nestedCascadeOptLen = nestedCascadeOpt.length();
   	// const string tryFlipOpt = "--try-flip";
	// size_t tryFlipOptLen = tryFlipOpt.length();
    	string inputName;
    	bool tryflip = false;
    	vector<Rect> faces;
    
	
	CascadeClassifier cascade, nestedCascade;
	double scale = 1;
	
    	double dtldcas1=get_wall_time();
    	if( !cascade.load( cascadeName ) )
    	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
	       // help();
		return -1;
    	}

  	double dtldcas2=get_wall_time();
	requiredTime[8][0]="time to load cascade"; 
	requiredTime[8][1]=to_string(dtldcas2-dtldcas1); 
	
	vid_requiredTime[2][0]="time to load cascade"; 
	vid_requiredTime[2][1]=to_string(dtldcas2-dtldcas1); 

#ifdef VIDEO



 // Mat frame, frameCopy;
  Size s = Size( (int)240,
		  (int)180);

  VideoWriter out;
  out.open("./video_out.avi",CV_FOURCC('M','J','P','G'),15,s,true);
  //out.open("./out_face.mp4",-1,30,s,true);



  CvCapture* capture = 0;

  capture = cvCaptureFromAVI( videoInputName.c_str() );
  //capture = cvCaptureFromFile( videoInputName.c_str() );
  if(!capture) cout << "Capture from AVI didn't work" << endl;
  if( capture )
  {
	  cout << "In capture ..." << endl;

	 for(;;)
	 // for(int i=1;i<2;i++)
	  {
		  IplImage* iplImg = cvQueryFrame( capture );
		  frame = iplImg;
		  if( frame.empty() )
			  break;
		  if( iplImg->origin == IPL_ORIGIN_TL )
			  frame.copyTo( frameCopy );
		  else
			  flip( frame, frameCopy, 0 );

		 // detectAndDraw( frameCopy, cascade, nestedCascade, scale, tryflip );

		  vector<Rect> faces;
		  double dtl1=get_wall_time();
		 // t = (double)cvGetTickCount();
		// detect(frameCopy, faces, impl, scale, clCxt, calTime);
		 detect( frameCopy, cascade, nestedCascade, scale, tryflip,faces,calTime );
		  double dtl2=get_wall_time();
		  //t = (double)cvGetTickCount() - t;
		//  cout<<"detection time: "<<t/((double)cvGetTickFrequency()*1000.) <<endl;

		 cout<<"detection time: "<<(dtl2-dtl1) <<endl;
		 cout<<"detection rate: "<<1/(dtl2-dtl1) <<"fps"<<endl;
 Draw(frameCopy, faces, scale);
		  out<<frameCopy;



		  if( waitKey( 10 ) >= 0 )
			  goto _cleanup_;
	  }
	  _cleanup_:
	          cvReleaseCapture( &capture );
  }
#endif

#ifdef VIDEO_old
	char str[5];
	string str2;
	int i;
	for (i = 0; i < VIDEO_FRAME; i++)
	{
		myTickMeter tm;			//for FPS measure, yuli1023
		
		double dtl1=0;
		double dtl2=0;

		double dtdraw1=0;
		double dtdraw2=0;


		calTime = i == 0 ? false : true;
		sprintf(str,"%04d",i);
		str2 = str;
		Mat image = imread(OUTPUT_VIDEO + str2 + ".jpg");
		//cout<<OUTPUT_VIDEO + str2 + ".jpg";
		if( image.empty() )
		{
			cout << "Couldn't read .jpg" << endl;
			return -1;
		}
		if (i == 0) cout << "In video read" << endl;

		if(calTime) 
		{
			tm.start ();
			dtl1=get_wall_time();
		}

		vector<Rect> faces;
			detect( image, cascade, nestedCascade, scale, tryflip,faces,calTime );

		
		if(calTime) 
		{
			tm.stop();
			dtl2=get_wall_time();
		}
		double lidetectionTime=0.0;
		if(calTime) 
		{
			detectionTime = detectionTime+(dtl2-dtl1);//tm.getTimeMilli();
			lidetectionTime = tm.getTimeMilli();
		}
		double fps = 1000 / lidetectionTime;
		cout << "\n" <<  fps << " FPS. \n";

		double mnfps = 1/ (dtl2-dtl1);
		cout << "\nmnfps" <<  fps << " FPS. \n";
		
		if(calTime) 
		{
			dtdraw1=get_wall_time();
		}
		Draw(image, faces, scale);

		if(calTime) 
		{
			dtdraw2=get_wall_time();
		}


		
		if(calTime) 
		{
			drawtm=drawtm+(dtdraw2-dtdraw1);
		}
 		//int c = waitKey(10);
		//if( (char)c == 'c' ) { break; }
	}


		vid_requiredTime[1][0]="time to execute detectLBP in FPS";
    		vid_requiredTime[1][1]=to_string((VIDEO_FRAME-1)/detectionTime);
		vid_requiredTime[0][0]="time to  draw circle and image in FPS";
		vid_requiredTime[0][1]=to_string((VIDEO_FRAME-1)/drawtm);
		
		
 	    
		vid_requiredTime[3][0]="time to smallimage,cvRound image in seconds";
		vid_requiredTime[3][1]=to_string(dtgray/(VIDEO_FRAME-1));   
	 	vid_requiredTime[4][0]="time to convert image in seconds";
		vid_requiredTime[4][1]=to_string(dtcvtcolor/(VIDEO_FRAME-1)); 
		vid_requiredTime[5][0]="time to resize image in seconds";
		vid_requiredTime[5][1]=to_string(dtresize/(VIDEO_FRAME-1)); 
		vid_requiredTime[6][0]="time to execute detectMultiScale in FPS";
	    	vid_requiredTime[6][1]=to_string((VIDEO_FRAME-1)/pr);


		ofstream myfile;
 
		string fileName="Time_Result_VIDEO_LG_CPU.txt";
		string resd=fileName+inputName.substr(1,4);
    		myfile.open (fileName.c_str());
		//"Time_Result"+inputName.substr(1,4)+".txt");
    		for ( int i = 0; i <8; ++i )
    		{
			for (int j = 0; j < 2; j++)
			{
			 
			      myfile << vid_requiredTime[i][j] << ";";
			}
			myfile<<endl;
   		}
	myfile.close();
	double tf2=get_wall_time();
   	vid_requiredTime[7][0]="time to execute full program in seconds";
    	vid_requiredTime[7][1]=to_string(tf2-tf1);
#endif
#ifdef IMAGE
  
   	double dtimread1=get_wall_time();
  
   
        image = imread( inputImage, 1 );

	double dtimread2=get_wall_time();
	requiredTime[7][0]="time to read image"; 
	requiredTime[7][1]=to_string(dtimread2-dtimread1); 

        if(image.empty()) cout << "Couldn't read image" << endl;
    	


   
        //cout << "In image read" << endl;
        if( !image.empty() )
        {
		double dtdetect1=get_wall_time();
		
		for(int i=0;i<LOOP_NUM;i++)
		{	
		
			calTime = i == 0 ? false : true;
			//bool calTime=true;
			double dtl1=0;
			double dtl2=0;

			if(calTime) 
			{
				
				dtl1=get_wall_time();
			}
double t = (double)cvGetTickCount();
	
		    	detect( image, cascade, nestedCascade, scale, tryflip,faces,calTime );

 t = (double)cvGetTickCount() - t;
//cout<<"detection time: "<<t/((double)cvGetTickFrequency()*1000.) <<"ms"<<endl;
			if(calTime)
			{
				
				dtl2=get_wall_time();
			}
			if(calTime)
			{
				timecalc=timecalc+(dtl2-dtl1);
			}

		}

	 	cout<<"total time for facedetect : "<<timecalc<<endl;
		//cout<<"average time for facedetect : ";
		//cout<<timecalc/(LOOP_NUM-1)<<"sec"<<endl;

    	requiredTime[1][0]="time to execute detect in seconds";
    	requiredTime[1][1]=to_string(timecalc/(LOOP_NUM-1)); 
 	    
		requiredTime[3][0]="time to smallimage,cvRound image in seconds";
		requiredTime[3][1]=to_string(dtgray/(LOOP_NUM-1));   
	 	requiredTime[4][0]="time to convert image in seconds";
		requiredTime[4][1]=to_string(dtcvtcolor/(LOOP_NUM-1)); 
		requiredTime[5][0]="time to resize image in seconds";
		requiredTime[5][1]=to_string(dtresize/(LOOP_NUM-1)); 
		requiredTime[6][0]="time to execute detectMultiScale in seconds";
	    requiredTime[6][1]=to_string(pr/(LOOP_NUM-1));

		double dtdraw1=get_wall_time();

	    Draw (image,faces,scale);

		double dtdraw2=get_wall_time();
    	requiredTime[2][0]="time to execute draw in seconds";
    	requiredTime[2][1]=to_string(dtdraw2-dtdraw1); 
	
           
        }
	else
        {
                cerr << "couldn't read image " << endl;
        }
       
	double t2=get_wall_time();
   	requiredTime[0][0]="time to execute full program in seconds";
    requiredTime[0][1]=to_string(t2-t1);
	
	ofstream myfile;
	string fileName="Time_Result_CPU"+inputImage.substr(6,5)+".txt";
	myfile.open (fileName.c_str());
	for ( int i = 0; i <9; ++i )
	{
		for (int j = 0; j < 2; j++)
		{
		
			  myfile << requiredTime[i][j] << ";";
		}
		myfile<<endl;
	}
	myfile.close();
 #endif
   
    return 0;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces,bool calTime )
{
    int i = 0;
    double t = 0;
   	
	//double dtgray1=get_wall_time();
	if(calTime)
	{
		   // double dtoclimg1=get_wall_time();
		 gettime_tocalc[0]=get_wall_time();
		//workBegin();
	}
    
    	Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
	if(calTime)
	{
		    //double dtoclimg2=get_wall_time();
		gettime_tocalc[1]=get_wall_time();
		//workEnd();
	}
		
	dtgray=dtgray+(gettime_tocalc[1]-gettime_tocalc[0]);

	//double dtgray2=get_wall_time();
    	//requiredTime[3][0]="time to smallimage,cvRound image in seconds";
    	//requiredTime[3][1]=to_string(dtgray2-dtgray1);      
	
	//double dtcvtcolor1=get_wall_time();

	if(calTime)
	{
//		    double dtgray1=get_wall_time();
	 	gettime_tocalc[2]=get_wall_time();
		//workBegin();
	}

    	cvtColor( img, gray, COLOR_BGR2GRAY );

	if(calTime)
	{
		    //double dtgray2=get_wall_time();
		gettime_tocalc[3]=get_wall_time();
		//workEnd();
	}

	/*double dtcvtcolor2=get_wall_time();
    	requiredTime[4][0]="time to convert image in seconds";
    	requiredTime[4][1]=to_string(dtcvtcolor2-dtcvtcolor1);  */

	dtcvtcolor=dtcvtcolor+(gettime_tocalc[3]-gettime_tocalc[2]);    

	//double dtresize1=get_wall_time();
	if(calTime)
	{
		    
		    //double dtresize1=get_wall_time();
			gettime_tocalc[4]=get_wall_time();
			//workBegin();
	}
    	resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
	
	if(calTime)
	{
		    
		    //double dtresize1=get_wall_time();
			gettime_tocalc[5]=get_wall_time();
			//workBegin();
	}
	/*double dtresize2=get_wall_time();
    	requiredTime[5][0]="time to resize image in seconds";
    	requiredTime[5][1]=to_string(dtresize2-dtresize1); */

	dtresize=dtresize+(gettime_tocalc[5]-gettime_tocalc[4]);
    	
	//equalizeHist( smallImg, smallImg );

    
	//double dtimre1=get_wall_time();
	if(calTime)
	{
		//double pr1=get_wall_time();
		gettime_tocalc[6]=get_wall_time();
		//workBegin();
	}
		
    	cascade.detectMultiScale( smallImg, faces,1.06, 3, 0,Size(24, 24) );
	
	if(calTime)
	{
		//double pr2=get_wall_time();
		gettime_tocalc[7]=get_wall_time();
		//workEnd();	
	}

	pr=pr+(gettime_tocalc[7]-gettime_tocalc[6]);
	/*double dtimre2=get_wall_time();
	requiredTime[6][0]="time to execute detectMultiscale in seconds";
    	requiredTime[6][1]=to_string(dtimre2-dtimre1);*/ 
    	
}
void Draw(Mat& img, vector<Rect>& faces, double scale)	
{
for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++ )
    {
        Point center;
        int radius;
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( img, center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
    }
#ifdef IMAGE
  imwrite(outputImage, img);
#endif

   
	//printf("\nnum of faces: %d\n", faces.size());
	//imwrite(outputImage, img);
    //cv::imshow( "result", img );
}
