#include "opencv2/objdetect/objdetect.hpp" 
#include "opencv2/highgui/highgui.hpp" 
#include "opencv2/imgproc/imgproc.hpp" 
//#include "opencv2/core/utility.hpp"
#include <sys/time.h>
#include "opencv2/highgui/highgui_c.h"
#include <string> 
#include <fstream>
#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>

using namespace std;
using namespace cv;
#define LOOP_NUM 50

string requiredTime[7][2];
int64 work_begin = 0;
int64 work_end = 0;

double dtoclimg=0;
double dtgray=0;
double dtcvtcolor=0;
double dtresize=0;
double pr=0;
double ds=0;
double gettime_tocalc[8];

template <typename T>
std::string to_string(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces,bool calTime );
void Draw(Mat& img, vector<Rect>& faces, double scale);

double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
    return (double)clock() / CLOCKS_PER_SEC;
}

string cascadeName = "lbpcascade_frontalface.xml";
string nestedCascadeName = "haarcascade_eye_tree_eyeglasses.xml";

string inputImage="sam22.jpg";
string outputImage="sam22_out_cpu.jpg";

int main( int argc, const char** argv )
{

	double t1=get_wall_time();
	bool calTime;
	double timecalc=0;
   	// CvCapture* capture = 0;
    	Mat frame, frameCopy, image;
   	// const string scaleOpt = "--scale=";
   	// size_t scaleOptLen = scaleOpt.length();
   	//const string cascadeOpt = "--cascade=";
    	//size_t cascadeOptLen = cascadeOpt.length();
    	//const string nestedCascadeOpt = "--nested-cascade";
   	// size_t nestedCascadeOptLen = nestedCascadeOpt.length();
   	// const string tryFlipOpt = "--try-flip";
	// size_t tryFlipOptLen = tryFlipOpt.length();
    	string inputName;
    	bool tryflip = false;
    	vector<Rect> faces;
    

	CascadeClassifier cascade, nestedCascade;
	double scale = 1;

    
    	if( !cascade.load( cascadeName ) )
    	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
	       // help();
		return -1;
    	}

  
  
   
        image = imread( inputImage, 1 );
        if(image.empty()) cout << "Couldn't read image" << endl;
    	


   
        //cout << "In image read" << endl;
        if( !image.empty() )
        {
		double dtdetect1=get_wall_time();
		
		for(int i=0;i<LOOP_NUM;i++)
		{	
		
			calTime = i == 0 ? false : true;
			double dtl1=0;
			double dtl2=0;

			if(calTime) 
			{
				
				dtl1=get_wall_time();
			}
	
		    	detect( image, cascade, nestedCascade, scale, tryflip,faces,calTime );

			if(calTime)
			{
				
				dtl2=get_wall_time();
			}
			if(calTime)
			{
				timecalc=timecalc+(dtl2-dtl1);
			}

		}

	 	cout<<"total time for facedetect : "<<timecalc<<endl;
		cout<<"average time for facedetect : ";
		cout<<timecalc/(LOOP_NUM-1)<<"sec"<<endl;

    		requiredTime[1][0]="time to execute detect in seconds";
    		requiredTime[1][1]=to_string(timecalc/(LOOP_NUM-1)); 
 	    
		requiredTime[3][0]="time to smallimage,cvRound image in seconds";
		requiredTime[3][1]=to_string(dtgray/(LOOP_NUM-1));   
	 	requiredTime[4][0]="time to convert image in seconds";
		requiredTime[4][1]=to_string(dtcvtcolor/(LOOP_NUM-1)); 
		requiredTime[5][0]="time to resize image in seconds";
		requiredTime[5][1]=to_string(dtresize/(LOOP_NUM-1)); 
		requiredTime[6][0]="time to execute detectMultiScale in seconds";
	    	requiredTime[6][1]=to_string(pr/(LOOP_NUM-1));

		double dtdraw1=get_wall_time();

	    	Draw (image,faces,scale);

		double dtdraw2=get_wall_time();
    		requiredTime[2][0]="time to execute draw in seconds";
    		requiredTime[2][1]=to_string(dtdraw2-dtdraw1); 
	
           
        }
	else
        {
                cerr << "couldn't read image " << endl;
        }
       
	double t2=get_wall_time();
   	requiredTime[0][0]="time to execute full program in seconds";
    	requiredTime[0][1]=to_string(t2-t1);
	
	ofstream myfile;

	myfile.open ("Time_Result_CPU.txt");
	for ( int i = 0; i <7; ++i )
	{
		for (int j = 0; j < 2; j++)
		{
		
			  myfile << requiredTime[i][j] << ";";
		}
		myfile<<endl;
	}
	myfile.close();
   
    return 0;
}

void detect( Mat& img, CascadeClassifier& cascade,
                    CascadeClassifier& nestedCascade,
                    double scale, bool tryflip,vector<Rect>& faces,bool calTime )
{
    int i = 0;
    double t = 0;
   	
	//double dtgray1=get_wall_time();
	if(calTime)
	{
		   // double dtoclimg1=get_wall_time();
		 gettime_tocalc[0]=get_wall_time();
		//workBegin();
	}
    
    	Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
	if(calTime)
	{
		    //double dtoclimg2=get_wall_time();
		gettime_tocalc[1]=get_wall_time();
		//workEnd();
	}
		
	dtgray=dtgray+(gettime_tocalc[1]-gettime_tocalc[0]);

	//double dtgray2=get_wall_time();
    	//requiredTime[3][0]="time to smallimage,cvRound image in seconds";
    	//requiredTime[3][1]=to_string(dtgray2-dtgray1);      
	
	//double dtcvtcolor1=get_wall_time();

	if(calTime)
	{
//		    double dtgray1=get_wall_time();
	 	gettime_tocalc[2]=get_wall_time();
		//workBegin();
	}

    	cvtColor( img, gray, COLOR_BGR2GRAY );

	if(calTime)
	{
		    //double dtgray2=get_wall_time();
		gettime_tocalc[3]=get_wall_time();
		//workEnd();
	}

	/*double dtcvtcolor2=get_wall_time();
    	requiredTime[4][0]="time to convert image in seconds";
    	requiredTime[4][1]=to_string(dtcvtcolor2-dtcvtcolor1);  */

	dtcvtcolor=dtcvtcolor+(gettime_tocalc[3]-gettime_tocalc[2]);    

	//double dtresize1=get_wall_time();
	if(calTime)
	{
		    
		    //double dtresize1=get_wall_time();
			gettime_tocalc[4]=get_wall_time();
			//workBegin();
	}
    	resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
	
	if(calTime)
	{
		    
		    //double dtresize1=get_wall_time();
			gettime_tocalc[5]=get_wall_time();
			//workBegin();
	}
	/*double dtresize2=get_wall_time();
    	requiredTime[5][0]="time to resize image in seconds";
    	requiredTime[5][1]=to_string(dtresize2-dtresize1); */

	dtresize=dtresize+(gettime_tocalc[5]-gettime_tocalc[4]);
    	
	//equalizeHist( smallImg, smallImg );

    
	//double dtimre1=get_wall_time();
	if(calTime)
	{
		//double pr1=get_wall_time();
		gettime_tocalc[6]=get_wall_time();
		//workBegin();
	}
		
    	cascade.detectMultiScale( smallImg, faces,1.06, 2, 0,Size(24, 24) );
	
	if(calTime)
	{
		//double pr2=get_wall_time();
		gettime_tocalc[7]=get_wall_time();
		//workEnd();	
	}

	pr=pr+(gettime_tocalc[7]-gettime_tocalc[6]);
	/*double dtimre2=get_wall_time();
	requiredTime[6][0]="time to execute detectMultiscale in seconds";
    	requiredTime[6][1]=to_string(dtimre2-dtimre1);*/ 
    	
}
void Draw(Mat& img, vector<Rect>& faces, double scale)	
{
for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++ )
    {
        Point center;
        int radius;
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( img, center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
    }
	
   
	//printf("\nnum of faces: %d\n", faces.size());
	imwrite(outputImage, img);
    //cv::imshow( "result", img );
}
