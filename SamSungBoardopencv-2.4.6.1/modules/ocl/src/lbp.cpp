#include "precomp.hpp" 
#include <stdio.h> 
#include <string> 
 
//#include "opencv2/imgproc.hpp" 
//#include "/opencv2/objdetect/objdetect_c.h" 
#include "/opencv-2.4.6.1/modules/ocl/include/opencv2/ocl/ocl.hpp" 
//#include "opencv2/core/utility.hpp" 
#include "opencv2/imgproc/imgproc_c.h" 
#include "opencv2/core/core_c.h" 
#include "/opencv-2.4.6.1/modules/ocl/include/opencv2/ocl/private/util.hpp" 
#include <sys/time.h> 
#include <sys/types.h> 
#include <string> 
#include <iostream> 
#include <stdio.h> 
//#define KERNEL_TIME



using namespace cv;
using namespace std;
int64 work_begin1 = 0;
int64 work_end1 = 0;
//#define DEBUG_HOST_CODE
//#define DEBUG_INTEGRAL_COLS
//#define DEBUG_SCALED_SRC
//#define DEBUG_INTEGRAL_ROWS
////////////////////////OpenCL kernel strings////////////
//extern const char *lbpobjectdetect;
const char* lbpobjectdetect="#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable\n"
		//"#define _localvariableValue_ 1000\n"
		"__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST; \n"
		"__kernel void helloworld(__global char* in, __global char* out)\n"
		"{\n"
		"int num = get_global_id(0);\n"
		"out[num] = in[num] + 1;\n"
		"}\n"
		//------------small local memory kernel begins here--------------------//

		//"#define NUM_BANKS_256 32\n"
		//"#define GET_CONFLICT_OFFSET256(lid) ((lid) >> LOG_NUM_BANKS_256)\n"

		"#define LSIZE 32\n"
				"#define LSIZE_LOOP_VALUE 15\n" //formula=(LSIZE>>1)-1
				"#define LSIZE_SHIFT_VALUE 4\n" //formula= 2th power value to represent LSIZE/2. e.g for LSIZE=256, LSIZE_SHIFT_VALUE=7
				"#define LSIZE_1 31\n"
				"#define LSIZE_2 30\n"
				"#define HF_LSIZE 8\n"
				"#define LOG_LSIZE 8\n"
				"#define LOG_NUM_BANKS 2\n"
				"#define NUM_BANKS 4\n"
				"#define GET_CONFLICT_OFFSET(lid) ((lid) >> LOG_NUM_BANKS)\n"

		"__kernel void v2_integral_cols_mas(__global int* idata, __global int* odata, __global int* temp, const int SIZE)\n"
		"{\n"
		"int gid=get_global_id(0);\n"
			"int offset =1;\n"

			"temp[gid]=idata[gid];\n"

		"	for(int d=SIZE>>1;d>0;d>>=1) {\n"
		//"if(gid==2)\n"
		//"		printf(\"kernel d=%d \",idata[gid]);\n"

		//"		barrier(CLK_GLOBAL_MEM_FENCE);\n"
			"	if(gid<d) {\n"
			"		int ai=offset*(2*gid+1)-1;\n"
		"		int bi=offset*(2*gid+2)-1;\n"

		"			temp[bi]+=temp[ai];\n"
		"		}\n"
		"		offset*=2;\n"
		"	}\n"

		"	int last_element=temp[SIZE-1];\n"

		"	if(gid==0) {\n"
		"		temp[SIZE-1]=0;\n"
		"	}\n"

		"	for(int d=1;d<SIZE;d*=2) {\n"
		"		offset>>=1;\n"
		//"		barrier(CLK_GLOBAL_MEM_FENCE);\n"

			"	if(gid<d) {\n"
			"		int ai=offset*(2*gid+1)-1;\n"
			"		int bi=offset*(2*gid+2)-1;\n"

		"			float t= temp[ai];\n"
		"			temp[ai]=temp[bi];\n"
		"			temp[bi]+=t;\n"
		"		}\n"
		"	}\n"

		"	odata[gid]=temp[gid+1];\n"
		"	odata[SIZE-1]=last_element;\n"

		"}\n"

		"__kernel void "
				"  v2_integral_cols_sum(__global uchar *src,\n"
						"int rows,int cols,__global int *lm_sum, int pixels,int steps,int o_steps)\n"
						"{\n"

					"int gid=get_global_id(0);\n"
					"if(gid>=pixels)"
					"return;\n"
					"else \n"
					"{"
					"int x = gid % steps;\n"
				    "int y = gid / steps;\n"
					"int sum=0;\n"

			//		"int ox = (y * (o_steps/4) + x) % (o_steps/4);\n"
			//		"int oy = (y * (o_steps/4) + x) / (o_steps/4);\n"
					"for (int i=0;i<=x;i++)"
					"{\n"
						"sum=sum+src[y * steps + i];\n"
					"}\n"
					"lm_sum[x * (o_steps/4) + y]=sum;\n"

				/*"if(gid==32)\n"
				"{"
					"printf(\"lm_sum [%d %d %d] %d  \",x,y,gid,lm_sum[x * (o_steps/4) + y]);\n"
					//"if(ox==18)\n"
					"printf(\"\\n\");\n"
					"printf(\"src values is %d  \\n\",src[y * steps + x]);\n"

				"}\n"*/
			"}"

		"}\n"


		"__kernel void v2_integral_rows_sum(__global int *src,\n"
		"int rows,int cols,__global int *lm_sum, int pixels,int steps,int o_steps,int sum_offset,int iteration)\n"
		"{\n"

			"int gid=get_global_id(0);\n"
			"if(gid>=pixels)"
			"return;\n"
			"else \n"
			"{"
				"int x = gid % (steps/4);\n"
				"int y = gid / (steps/4);\n"
				"int sum=0;\n"

				//		"int ox = (y * (o_steps/4) + x) % (o_steps/4);\n"
				//		"int oy = (y * (o_steps/4) + x) / (o_steps/4);\n"
				"for (int i=0;i<=x;i++)"
				"{\n"
					"sum=sum+src[y * (steps/4) + i];\n"
				"}\n"
				//"lm_sum[(x) * (o_steps/4) + (y)]=(x==0||y==0?0:1);\n"
				"lm_sum[(x+1) * (o_steps/4)+ (sum_offset+1+y)]=sum;\n"

				/*"if(gid==0 && iteration==0)\n"
				"{"
					"printf(\"rows src[%d %d %d]  %d \\n\",x,y,gid,src[y * (steps/4) + x]);\n"
//					"if(x==19)\n"
//					"printf(\"\\n\");\n"
					//"printf(\"expression values is %d  \\n\",y * (steps/4) + x);\n"
					"printf(\"\\n x y values are %d  %d \\n\",x,y);\n"
					"printf(\"\\n output expression values is %d  \\n\",(x+1) * (o_steps/4)+ (sum_offset+1+y));\n"
					"printf(\"sum expression values is %d  \\n\",sum);\n"
					"printf(\"\\n sum_offset values is %d  \\n\",sum_offset);\n"


				"}\n"*/
			"}"

		"}\n"


		"__kernel void v2_integral_cols(__global uchar *src,__global int *sum,\n"
				"int src_offset,int pre_invalid,int rows,int cols,int src_step,int dst_step)\n"
				"{\n"
				"unsigned int lid = get_local_id(0);\n"
				"unsigned int gid = get_group_id(0);\n"
				"unsigned int global_id = get_global_id(0);\n"
				//"if(lid<=1)\n"
				"{\n"
				//"printf(\" global id................................. %d \\n\",global_id);\n"
		"}\n"
				"int src_t[2], sum_t[2];\n"
				"__local int lm_sum[2][LSIZE + LOG_LSIZE];\n"
				"__local int* sum_p;\n"
				"gid = gid << 1;\n"
				"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
				"{\n"
				"src_t[0] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid, (uint)cols - 1)] : 0);\n"
				"src_t[1] = (i + lid < rows ? src[src_offset + (lid+i) * src_step + min(gid + 1, (uint)cols - 1)] : 0);\n"
				"sum_t[0] = (i == 0 ? 0 : lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"    /////
				"sum_t[1] =  (i == 0 ? 0 : lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n" /////
				/*"if(lid<=1)\n"
				"{\n"
				"printf(\" group id................................. %d \\n\",gid);\n"

				"printf(\" lid................................. %d \\n\",lid);\n"
				"printf(\" value of i................................. %d \\n\",i);\n"
				"printf(\" srct_o value %d \\n\",src_t[0]);\n"
				"printf(\" srct_1 value %d \\n\",src_t[1]);\n"
				"printf(\" sum_0 value %d \\n\",sum_t[0]);\n"
				"printf(\" sum_1 value %d \\n\",sum_t[1]);\n"
				"printf(\" express_0 value %d \\n\",src_offset + (lid+i) * src_step + min(gid, (uint)cols - 1));\n"
				"printf(\" lm_sum_0 value %d \\n\",lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"
				"printf(\" lm_sum_1 value %d \\n\",lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n"

				"printf(\" (lid+i) * src_step value %d \\n\",(lid+i) * src_step);\n"
				"printf(\" min(gid, (uint)cols - 1) value %d \\n\",min(gid, (uint)cols - 1));\n"


			"}\n"*/

				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"
				//"int bf_loc = lid ;\n"
				"lm_sum[0][bf_loc] = src_t[0];\n"
				"lm_sum[1][bf_loc] = src_t[1];\n"
				"int offset = 1;\n"



				"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
				"{\n"
					"barrier(CLK_LOCAL_MEM_FENCE);\n"
					"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
					//"printf(\" ai value before offset %d \\n\",ai);\n"
					//"printf(\" bi value before offset %d \\n\",bi);\n"

					"ai += GET_CONFLICT_OFFSET(ai);\n"
					"bi += GET_CONFLICT_OFFSET(bi);\n"

					//"if(lid<=1)\n"
					/*"{\n"
					"printf(\" group id................................. %d \\n\",gid);\n"

					"printf(\" lid................................. %d \\n\",lid);\n"
					"printf(\" value of i................................. %d \\n\",i);\n"
					"printf(\" value of d................................. %d \\n\",d);\n"
					"printf(\" srct_o value %d \\n\",src_t[0]);\n"
					"printf(\" srct_1 value %d \\n\",src_t[1]);\n"
					"printf(\" sum_0 value %d \\n\",sum_t[0]);\n"
					"printf(\" sum_1 value %d \\n\",sum_t[1]);\n"
					"printf(\" ai value after offset %d \\n\",ai);\n"
					"printf(\" bi value after offset %d \\n\",bi);\n"
					"printf(\" offset expression value %d \\n\",((lid & LSIZE_LOOP_VALUE)<<1) +1);\n"
					"printf(\" offset value %d \\n\",offset);\n"
					"printf(\" lm_sum values  ai%d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][ai]);\n"
					"printf(\" lm_sum values bi %d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]);\n"
					"printf(\" lm_sum expression value  %d \\n\",lid >> LSIZE_SHIFT_VALUE);\n"



					"}\n"*/


					"if((lid & LSIZE_LOOP_VALUE) < d)\n" //this only works upto 16 elements
					"{\n"

						"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
						//"printf(\" inside if condition %d \\n\",lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]);\n"

					"}\n"
					"offset <<= 1;\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"if(lid < 2)\n"
				"{\n"
				"lm_sum[lid][LSIZE_2 + LOG_LSIZE] = 0;\n"
				"}\n"
				"for(int d = 1;  d < LSIZE; d <<= 1)\n"
				"{\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"offset >>= 1;\n"
				"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
				"ai += GET_CONFLICT_OFFSET(ai);\n"
				"bi += GET_CONFLICT_OFFSET(bi);\n"
				//=======================================================
				"if((lid & LSIZE_LOOP_VALUE) < d)\n"
				"{\n"
				"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"

				"if((lid & LSIZE_LOOP_VALUE) < d)\n"
				"{\n"
				"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				//=======================================================
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"int loc_s0 = gid * dst_step + i + lid - 1 - pre_invalid * dst_step , loc_s1 = loc_s0 + dst_step;\n"
				"if(lid > 0 && (i+lid) <= rows)\n"
				"{\n"
				"lm_sum[0][bf_loc] += sum_t[0];\n"  //////
				"lm_sum[1][bf_loc] += sum_t[1];\n"  //////
				"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
				"if ((gid < cols + pre_invalid && gid >= pre_invalid))\n"
				"{\n"
				"sum[loc_s0] = sum_p[0];\n"
				"}\n"
				"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
				"if(gid + 1 < cols + pre_invalid)\n"
				"{\n"
				"sum[loc_s1] = sum_p[0];\n"
				"}\n"
				"}\n"
				"barrier(CLK_LOCAL_MEM_FENCE);\n"
				"}\n"
				"}\n"



		"__kernel void v2_integral_rows(__global int *srcsum,__global int *sum ,\n"
		"int rows,int cols,int src_step,int sum_step,int sum_offset)\n"
		"{\n"
		"unsigned int lid = get_local_id(0);\n"
		"unsigned int gid = get_group_id(0);\n"
		"int src_t[2], sum_t[2];\n"
		"__local int lm_sum[2][LSIZE + LOG_LSIZE];\n"
		"__local int *sum_p;\n"
		"src_step = src_step >> 2;\n"    //yuli1009
		"gid = gid << 1;\n"                //yuli1009
		"for(int i = 0; i < rows; i =i + LSIZE_1)\n"
		"{\n"
		"src_t[0] = i + lid < rows ? srcsum[(lid+i) * src_step + gid] : 0;\n"
		"src_t[1] = i + lid < rows ? srcsum[(lid+i) * src_step + gid + 1] : 0;\n"
		"sum_t[0] =  (i == 0 ? 0 : lm_sum[0][LSIZE_2 + LOG_LSIZE]);\n"
		"sum_t[1] =  (i == 0 ? 0 : lm_sum[1][LSIZE_2 + LOG_LSIZE]);\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int bf_loc = lid + GET_CONFLICT_OFFSET(lid);\n"
		"lm_sum[0][bf_loc] = src_t[0];\n"
		"lm_sum[1][bf_loc] = src_t[1];\n"
		"int offset = 1;\n"
		"for(int d = LSIZE >> 1 ;  d > 0; d>>=1)\n"
		"{\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
		"ai += GET_CONFLICT_OFFSET(ai);\n"
		"bi += GET_CONFLICT_OFFSET(bi);\n"

		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi]  +=  lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"offset <<= 1;\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"if(lid < 2)\n"
		"{\n"
		"lm_sum[lid][LSIZE_2 + LOG_LSIZE] = 0;\n"
		"}\n"
		"for(int d = 1;  d < LSIZE; d <<= 1)\n"
		"{\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"offset >>= 1;\n"
		"int ai = offset * (((lid & LSIZE_LOOP_VALUE)<<1) +1) - 1,bi = ai + offset;\n"
		"ai += GET_CONFLICT_OFFSET(ai);\n"
		"bi += GET_CONFLICT_OFFSET(bi);\n"
		/*"if((lid & LSIZE_LOOP_VALUE) < d)\n"
"{\n"
"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
"}\n"*/
		//======================================================
		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] += lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"

		"if((lid & LSIZE_LOOP_VALUE) < d)\n"
		"{\n"
		"lm_sum[lid >> LSIZE_SHIFT_VALUE][ai] = lm_sum[lid >> LSIZE_SHIFT_VALUE][bi] - lm_sum[lid >> LSIZE_SHIFT_VALUE][ai];\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		//======================================================
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"int loc_s0 = sum_offset + gid * sum_step + sum_step + i + lid, loc_s1 = loc_s0 + sum_step ;\n"
		"if(lid > 0 && (i+lid) <= rows)\n"
		"{\n"
		"lm_sum[0][bf_loc] += sum_t[0];\n"
		"lm_sum[1][bf_loc] += sum_t[1];\n"
		"sum_p = (__local int*)(&(lm_sum[0][bf_loc]));\n"
		"if(gid < cols)\n"
		"{\n"
		"sum[loc_s0] = sum_p[0];\n"
		"}\n"
		"sum_p = (__local int*)(&(lm_sum[1][bf_loc]));\n"
		"if(gid + 1 < cols)\n"
		"{\n"
		"sum[loc_s1] = sum_p[0];\n"
		"}\n"
		"}\n"
		"barrier(CLK_LOCAL_MEM_FENCE);\n"
		"}\n"
		"}\n"
		//------------small local memory kernel ends here--------------------//
		"typedef struct Stage\n"
		"{\n"
		"int    first;\n"
		"int    ntrees;\n"
		"float  threshold;\n"
		"}Stage;\n"
		"typedef struct ClNode\n"
		"{\n"
		"int   left;\n"
		"int   right;\n"
		"int   featureIdx;\n"
		"}ClNode;\n"

		"__kernel void __attribute__((reqd_work_group_size(32, 1, 1))) lbp_cascade_buf(\n"
				"global Stage* stages,\n"
				"int nstages,\n"
				"global ClNode* nodes,\n"
				"global float* leaves,\n"
				"global int*subsets,\n"
				"__constant uchar* features,\n"
				"int subsetSize, \n"
				"int frameW, \n"
				"int frameH, \n"
				"int windowW, \n"
				"int windowH, \n"
				"float scale, \n"
				"const float factor,\n"
				"const int total, \n"
				"global int* integral, \n"
				"const int pitch, \n"
				"global int4* objects, \n"
				"int object_col,\n"
				"global unsigned int* classified)\n" //start from here
				//"global  int* test_classified_value)\n"
				"{\n"
				"int ftid = get_group_id(0) * get_local_size(0) + get_local_id(0);\n"

				//mad24 (get_group_id(0), get_local_size(0),get_local_id(0));\n"//get_group_id(0) * get_local_size(0) + get_local_id(0);\n" //block1

				"bool ftid_check=(ftid >= total);\n"
				"if (ftid_check) return;\n"

				"int step = (scale <= 2.f);\n"
				"int windowsForLine = ((int)( frameW / scale + 0.5f) - windowW ) >> step;\n"
				"int stotal = windowsForLine * (( (int)(frameH / scale + 0.5f) - windowH) >> step);\n"
				"int wshift = 0;\n"
				"int scaleTid = ftid;\n"

				//block1
				/*"if(ftid>=0 && ftid<=7)\n"
				"{\n"*/


				/*"}\n"*/
				/*"if(ftid==125)\n"
				"{\n"
				"printf(\"ftid..... .............................>>>>>>>> %d\\n\",ftid);\n"
				"printf(\"from before while kernel ftid_check : %d\\n\",ftid_check);\n"
				"printf(\"from before while kernel windowsForLine : %d\\n\",windowsForLine);\n"
				"printf(\"from before while kernel stotal : %d\\n\",stotal);\n"
				"printf(\"from before while kernel wshift : %d\\n\",wshift);\n"
				"printf(\"from before while kernel scaleTid : %d\\n\",scaleTid);\n"

				"}\n"*/
				"while (scaleTid >= stotal)\n"                               //block2
				"{\n"
				"scaleTid -= stotal;\n"
				"wshift += (int)(frameW / scale + 0.5f) + 1;\n"
				"scale *= factor;\n"
				"step = (scale <= 2.f);\n"
				"windowsForLine = ( ((int)(frameW / scale + 0.5f) - windowW) >> step);\n"
				"stotal = windowsForLine * ( (int)(frameH / scale + 0.5f) - windowH) >> step;\n"
				"}\n"                                                          //block2
				"int y = scaleTid / windowsForLine;\n"   //added 1018         //block3
				"int x = scaleTid % windowsForLine;\n"
				"x <<= step;\n"
				"y <<= step;\n"
				"bool bcalc = true;\n"  //true
				"int xc = x + wshift;\n"
				"int current_node = 0;\n"
				"int current_leave = 0;\n"
				"int s=0;\n"
				//this is to debug the code and see what the first portion is doing
				/*"if(ftid==125)\n"
				"{\n"

				"printf(\"from kernel after loop ftid_check : %d\\n\",ftid_check);\n"
				"printf(\"from kernel  after loopwindowsForLine : %d\\n\",windowsForLine);\n"
				"printf(\"from kernel after loop stotal : %d\\n\",stotal);\n"
				"printf(\"from kernel  after loopw shift : %d\\n\",wshift);\n"
				"printf(\"from kernel after loop  scaleTid : %d\\n\",scaleTid);\n"
				"printf(\"from kernel scale : %f\\n\",scale);\n"

				"printf(\"from kernel step : %d\\n\",step);\n"
				"printf(\"from kernel y : %d\\n\",y);\n"
				"printf(\"from kernel x : %d\\n\",x);\n"
				"printf(\"from kernel xc : %d\\n\",xc);\n"

				"}\n"*/

				"while (s < nstages && bcalc)\n"                        //block3
				//"for (int s = 0; s < nstages && bcalc; ++s)\n"   //b4
				"{\n"

				"float sum = 0;\n"
				"Stage stage = stages[s];\n"

				/*"if(ftid==125 && s==0)\n"
				"{\n"
					"printf(\"from kernel stages.................................................. :: %d\\n\",s);\n"
				//"printf(\"from kernel s........... : %d\\n\",s);\n"
				//"printf(\"from kernel  stage ntrees........ : %d\\n\",stage.ntrees);\n"

				"}\n"*/
				//"#pragma unroll stage.ntrees \n"
				"for (int t = 0; t < stage.ntrees; t++)\n"
				"{\n"
				"ClNode node = nodes[current_node];\n"
				"uchar4 feature;\n"
				"feature.x = features[4 * node.featureIdx];\n"
				"feature.y = features[4 * node.featureIdx + 1];\n"
				"feature.z = features[4 * node.featureIdx + 2];\n"
				"feature.w = features[4 * node.featureIdx + 3];\n"

				"int shift;\n"

				"int ty =(y + feature.y) * pitch + xc + feature.x;\n"
				"int temp =y* pitch + xc;\n"
				//mad24((y + feature.y) , pitch , xc + feature.x);\n"//(y + feature.y) * pitch + xc + feature.x;\n"  //////////////////////////////////
				"int fh = feature.w * pitch;\n"
				//mul24(feature.w , pitch);\n" //feature.w * pitch;
				"int fw = feature.z;\n"

				/*"if(ftid==0 && s==0 && t==1)\n"
				"{\n"
				//"printf(\"from kernel stages.................................................. :: %d\\n\",s);\n"
				"printf(\"from kernel t.................................................. :: %d\\n\",t);\n"
				"printf(\"from kernel node.featureIdx... : %d\\n\",node.featureIdx);\n"
				"printf(\"from kernel node.left... : %d\\n\",node.left);\n"
				"printf(\"from kernel node.right... : %d\\n\",node.right);\n"
				"printf(\"from kernel feature.x : %d\\n\",feature.x);\n"
				"printf(\"from kernel feature.y : %d\\n\",feature.y);\n"
				"printf(\"from kernel feature.z : %d\\n\",feature.z);\n"
				"printf(\"from kernel feature.w : %d\\n\",feature.w);\n"
				"printf(\"from kernel integral[temp] : %d\\n\",integral[temp]);\n"
				"printf(\"from kernel integral[ty] : %d\\n\",integral[ty]);\n"
				/*"printf(\"from kernel pitch : %d\\n\",pitch);\n"
				"printf(\"from kernel y : %d\\n\",y);\n"
				"printf(\"from kernel x : %d\\n\",x);\n"
				"printf(\"from kernel xc : %d\\n\",xc);\n"
				"printf(\"from kernel ty : %d\\n\",ty);\n"
				"printf(\"from kernel fh : %d\\n\",fh);\n"
				"printf(\"from kernel integral[ty] : %d\\n\",integral[ty]);\n"

				"printf(\"from kernel integral[ty + fw] : %d\\n\",integral[ty + fw]);\n"
		//"printf(\"from kernel integral[ty ] : %d\\n\",integral[3657]);\n"
		//"printf(\"from kernel integral[ty + fw] : %d\\n\",integral[3659]);\n"
				"printf(\"from kernel integral[ty + fw * 2] : %d\\n\",integral[ty + fw * 2]);\n"
				"printf(\"from kernel integral[ty + fw * 3] : %d\\n\",integral[ty + fw * 3]);\n"
				"printf(\"from kernel ty += fh : %d\\n\",ty + fh);\n"
				"printf(\"from kernel anchor3= fh : %d\\n\",integral[ty+fh]);\n"
				"printf(\"from kernel anchor3= fh : %d\\n\",integral[1849]);\n"
				"printf(\"from kernel anchor4= fh : %d\\n\",integral[ty+fh+fw]);\n"
				"printf(\"from kernel anchor5= fh : %d\\n\",integral[ty+fh+fw*2]);\n"
				"printf(\"from kernel anchor5' = fh : %d\\n\",integral[ty+fh+fw*3]);\n"
				"printf(\"from kernel anchor6= fh : %d\\n\",integral[ty+fh+fh]);\n"
				"printf(\"from kernel anchor7= fh : %d\\n\",integral[ty+fh+fh+fw]);\n"
				"printf(\"from kernel anchor8= fh : %d\\n\",integral[ty+fh+fh+fw*2]);\n"
				"printf(\"from kernel anchor8' = fh : %d\\n\",integral[ty+fh+fh+fw*3]);\n"
				"printf(\"from kernel anchor 10= fh : %d\\n\",integral[ty+fh+fh+fh]);\n"
				"printf(\"from kernel anchor11= fh : %d\\n\",integral[ty+fh+fh+fh+fw]);\n"
				"printf(\"from kernel anchor12= fh : %d\\n\",integral[ty+fh+fh+fh+fw*2]);\n"
				"printf(\"from kernel anchor1912' = fh : %d\\n\",integral[ty+fh+fh+fh+fw*3]);\n"*/

				//"}\n"


				"int anchors0=2;\n"
				"int anchors1=3;\n"
				"int anchors2=6;\n"
				"int anchors3=9;\n"
				"int anchors4=1;\n"
				"int anchors5=7;\n"
				"int anchors6=7;\n"
				"int anchors7=5;\n"
				"int anchors8=4;\n"
				"anchors0  = integral[ty];\n"
				"anchors1  = integral[ty + fw];\n"
				"anchors0 -= anchors1;\n"
				"anchors2  = integral[ty + fw * 2];\n"

				"anchors1 -= anchors2;\n"
				"anchors2 -= integral[ty + fw * 3];\n"

				"ty += fh;\n"
				"anchors3  = integral[ty];\n"
				"anchors4  = integral[ty + fw];\n"
				"anchors3 -= anchors4;\n"
				"anchors5  = integral[ty + fw * 2];\n"

				"anchors4 -= anchors5;\n"
				"anchors5 -= integral[ty + fw * 3];\n"

				"anchors0 -= anchors3;\n"
				"anchors1 -= anchors4;\n"
				"anchors2 -= anchors5;\n"
				"ty += fh;\n"
				"anchors6  = integral[ty];\n"
				"anchors7  = integral[ty + fw];\n"
				"anchors6 -= anchors7;\n"
				"anchors8  = integral[ty + fw * 2];\n"

				"anchors7 -= anchors8;\n"
				"anchors8 -= integral[ty + fw * 3];\n"

				"anchors3 -= anchors6;\n"
				"anchors4 -= anchors7;\n"
				"anchors5 -= anchors8;\n"
				"anchors0 -= anchors4;\n"
				"anchors1 -= anchors4;\n"
				"anchors2 -= anchors4;\n"
				"anchors3 -= anchors4;\n"
				"anchors5 -= anchors4;\n"
				"int response = (~(anchors0 >> 31)) & 4;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"response |= (~(anchors1 >> 31)) & 2;;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"response |= (~(anchors2 >> 31)) & 1;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel response: %d\\n\",response);\n"
				"shift = (~(anchors5 >> 31)) & 16;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift00: %d\\n\",shift);\n"
				"shift |= (~(anchors3 >> 31)) & 1;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift01: %d\\n\",shift);\n"
				"ty += fh;\n"
				"anchors0  = integral[ty];\n"
				"anchors1  = integral[ty + fw];\n"
				"anchors0 -= anchors1;\n"
				"anchors2  = integral[ty + fw * 2];\n"

				"anchors1 -= anchors2;\n"
				"anchors2 -= integral[ty + fw * 3];\n"

				"anchors6 -= anchors0;\n"
				"anchors7 -= anchors1;\n"
				"anchors8 -= anchors2;\n"
				"anchors6 -= anchors4;\n"
				"anchors7 -= anchors4;\n"
				"anchors8 -= anchors4;\n"
				"shift |= (~(anchors6 >> 31)) & 2;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift1: %d\\n\",shift);\n"
				"shift |= (~(anchors7 >> 31)) & 4;\n"
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift2 : %d\\n\",shift);\n"
				"shift |= (~(anchors8 >> 31)) & 8;\n" ///////////////////////////////////
				//"if(ftid>=0 && ftid<=7)\n"
				//"printf(\"from kernel shift3 : %d\\n\",shift);\n"
				"int c = response;\n"
				"int idx =  (subsets[ current_node * subsetSize + c] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"
				//"int idx =  (subsets[mad24(current_node , subsetSize , c)] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"

				/*	"if(ftid==125 && s==0)\n"
								"{\n"


								"printf(\"from kernel c : %d\\n\",c);\n"
								"printf(\"from kernel subsetSize : %d\\n\",subsetSize);\n"
								"printf(\"from kernel shift : %d\\n\",shift);\n"
								"printf(\"from kernel subsetvalue : %d\\n\",subsets[ current_node * subsetSize + c]);\n"
								"printf(\"from kernel idx : %d\\n\",idx);\n"
								"printf(\"from kernel current_node * subsetSize + c : %d\\n\",current_node * subsetSize + c);\n"
								"printf(\"from kernel 1 << shift : %d\\n\",1 << shift);\n"

								"printf(\"from kernel leaves : %f\\n\",leaves[idx]);\n"




								"}\n"*/

				"sum += leaves[idx];\n"
	/*	"if(ftid==0 && s==0 && t==1)\n"
								"{\n"
									"printf(\"leaves[idx] value : %f\\n\",leaves[idx]);\n"
								"}\n"*/
				"current_node += 1;\n"
				"current_leave += 2;\n"

				"}\n"
				"bool sum_check=(sum < stage.threshold);\n"
				"if (sum_check)\n"
				"bcalc = false;\n"

				"++s;\n"

				"}\n"   //while loop ends                                         //b4
				"if (bcalc)\n"
				"{\n"
				"bool window_check=(x >= (int)(frameW / scale + 0.5f) - windowW);\n"
				"if(window_check) return;\n"
				"int4 rect;\n"
				"rect.x = (int)(x * scale + 0.5f);\n"
				"rect.y = (int)(y * scale + 0.5f);\n"
				"rect.z = (int)(windowW * scale + 0.5f);\n"
				"rect.w = (int)(windowH * scale + 0.5f);\n"


				"int res = atomic_inc( classified );\n" //commented to see its impact
				//"test_classified_value[ftid] = 1;\n"
				"objects[res] = rect;\n"
				"}\n"
				"}\n" //end here


		"__kernel void __attribute__((reqd_work_group_size(32,1,1))) lbp_cascade(\n"
				"global Stage* stages,\n"
				"int nstages,\n"
				"global ClNode* nodes,\n"
				"global float* leaves,\n"
				"global int*subsets,\n"
				"global uchar* features,\n"
				"int subsetSize, \n"
				"int frameW, \n"
				"int frameH, \n"

				"int windowW, \n"
				"int windowH, \n"
				"float scale, \n"
				"const float factor,\n"
				"const int total, \n"
				"__read_only image2d_t integral, \n"	//texture version, yuli1202
				"const int pitch, \n"
				"global int4* objects, \n"
				"int object_col,\n"
				"global unsigned int* classified)\n"
				"{\n"
				"int ftid = get_group_id(0) * get_local_size(0) + get_local_id(0);\n"
				"if (ftid >= total) return;\n"
				"int step = (scale <= 2.f);\n"
				"int windowsForLine = ((int)( frameW / scale + 0.5f) - windowW ) >> step;\n"
				"int stotal = windowsForLine * (( (int)(frameH / scale + 0.5f) - windowH) >> step);\n"
				"int wshift = 0;\n"
				"int scaleTid = ftid;\n"
				"while (scaleTid >= stotal)\n"
				"{\n"
				"scaleTid -= stotal;\n"
				"wshift += (int)(frameW / scale + 0.5f) + 1;\n"
				"scale *= factor;\n"
				"step = (scale <= 2.f);\n"
				"windowsForLine = ( ((int)(frameW / scale + 0.5f) - windowW) >> step);\n"
				"stotal = windowsForLine * ( (int)(frameH / scale + 0.5f) - windowH) >> step;\n"
				"}\n"
				"int y = scaleTid / windowsForLine;\n"
				"int x = scaleTid % windowsForLine;\n"	//"int x = scaleTid - y * windowsForLine;\n"	//yuli1018
				"x <<= step;\n"
				"y <<= step;\n"
				"bool bcalc = true;\n"
				"int xc = x + wshift;\n"
				"int current_node = 0;\n"
				"int current_leave = 0;\n"
				"for (int s = 0; s < nstages && bcalc; ++s)\n"
				"{\n"
				"float sum = 0;\n"
				"Stage stage = stages[s];\n"
				"for (int t = 0; t < stage.ntrees; t++)\n"
				"{\n"
				"ClNode node = nodes[current_node];\n"
				"uchar4 feature;\n"
				"feature.x = features[4 * node.featureIdx];\n"
				"feature.y = features[4 * node.featureIdx + 1];\n"
				"feature.z = features[4 * node.featureIdx + 2];\n"
				"feature.w = features[4 * node.featureIdx + 3];\n"
				"int shift;\n"
				"int2 coord;\n"
				"coord.x = xc + feature.x ;\n"
				"coord.y = y + feature.y;\n"

				"int anchors[9];\n"
				//"anchors[0]  = integral[ty];\n"
				"anchors[0] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				//"anchors[1]  = integral[ty + fw];\n"
				"coord.x += feature.z ;\n"
				"anchors[1] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[0] -= anchors[1];\n"
				//"anchors[2]  = integral[ty + fw * 2];\n"
				"coord.x += feature.z ;\n"
				"anchors[2] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[1] -= anchors[2];\n"
				//"anchors[2] -= integral[ty + fw * 3];\n"
				"coord.x += feature.z ;\n"
				"anchors[2] -= read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202

				//"ty += fh;\n"
				"coord.x -= feature.z * 3 ;\n"
				"coord.y += feature.w ;\n"
				//"anchors[3]  = integral[ty];\n"
				"anchors[3] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				//"anchors[4]  = integral[ty + fw];\n"
				"coord.x += feature.z ;\n"
				"anchors[4] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[3] -= anchors[4];\n"
				//"anchors[5]  = integral[ty + fw * 2];\n"
				"coord.x += feature.z ;\n"
				"anchors[5] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[4] -= anchors[5];\n"
				//"anchors[5] -= integral[ty + fw * 3];\n"
				"coord.x += feature.z ;\n"
				"anchors[5] -= read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[0] -= anchors[3];\n"
				"anchors[1] -= anchors[4];\n"
				"anchors[2] -= anchors[5];\n"

				//"ty += fh;\n"
				"coord.x -= feature.z * 3 ;\n"
				"coord.y += feature.w ;\n"
				//"anchors[6]  = integral[ty];\n"
				"anchors[6] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				//"anchors[7]  = integral[ty + fw];\n"
				"coord.x += feature.z ;\n"
				"anchors[7] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[6] -= anchors[7];\n"
				//"anchors[8]  = integral[ty + fw * 2];\n"
				"coord.x += feature.z ;\n"
				"anchors[8] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[7] -= anchors[8];\n"
				//"anchors[8] -= integral[ty + fw * 3];\n"
				"coord.x += feature.z ;\n"
				"anchors[8] -= read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[3] -= anchors[6];\n"
				"anchors[4] -= anchors[7];\n"
				"anchors[5] -= anchors[8];\n"
				"anchors[0] -= anchors[4];\n"
				"anchors[1] -= anchors[4];\n"
				"anchors[2] -= anchors[4];\n"
				"anchors[3] -= anchors[4];\n"
				"anchors[5] -= anchors[4];\n"
				"int response = (~(anchors[0] >> 31)) & 4;\n"
				"response |= (~(anchors[1] >> 31)) & 2;;\n"
				"response |= (~(anchors[2] >> 31)) & 1;\n"
				"shift = (~(anchors[5] >> 31)) & 16;\n"
				"shift |= (~(anchors[3] >> 31)) & 1;\n"
				//"ty += fh;\n"
				"coord.x -= feature.z * 3 ;\n"
				"coord.y += feature.w ;\n"
				//"anchors[0]  = integral[ty];\n"
				"anchors[0] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				//"anchors[1]  = integral[ty + fw];\n"
				"coord.x += feature.z ;\n"
				"anchors[1] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[0] -= anchors[1];\n"
				//"anchors[2]  = integral[ty + fw * 2];\n"
				"coord.x += feature.z ;\n"
				"anchors[2] = read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[1] -= anchors[2];\n"
				//"anchors[2] -= integral[ty + fw * 3];\n"
				"coord.x += feature.z ;\n"
				"anchors[2] -= read_imageui(integral, sampler,coord).x; \n"	//texture version, yuli1202
				"anchors[6] -= anchors[0];\n"
				"anchors[7] -= anchors[1];\n"
				"anchors[8] -= anchors[2];\n"
				"anchors[6] -= anchors[4];\n"
				"anchors[7] -= anchors[4];\n"
				"anchors[8] -= anchors[4];\n"
				"shift |= (~(anchors[6] >> 31)) & 2;\n"
				"shift |= (~(anchors[7] >> 31)) & 4;\n"
				"shift |= (~(anchors[8] >> 31)) & 8;\n"
				"int c = response;\n"
				//"int c = evaluator(integral...);\n" end
				"int idx =  (subsets[ current_node * subsetSize + c] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"
				"sum += leaves[idx];\n"
				"current_node += 1;\n"
				"current_leave += 2;\n"
				"}\n"
				"if (sum < stage.threshold)\n"
				"bcalc = false;\n"
				"}\n"
				"if (bcalc)\n"
				"{\n"
				"if(x >= (int)(frameW / scale + 0.5f) - windowW) return;\n"
				"int4 rect;\n"
				"rect.x = (int)(x * scale + 0.5f);\n"
				"rect.y = (int)(y * scale + 0.5f);\n"
				"rect.z = (int)(windowW * scale + 0.5f);\n"
				"rect.w = (int)(windowH * scale + 0.5f);\n"
				"int res = atomic_inc( classified );\n"
				"objects[res] = rect;\n"
				"}\n"
				"}\n"
				;

size_t divUp(int x, int x1) {return (int)(ceilf((float)x / x1)) * x1; }

cv::Size operator -(const cv::Size& a, const cv::Size& b)
{
	return cv::Size(a.width - b.width, a.height - b.height);
}

cv::Size operator +(const cv::Size& a, const int& i)
{
	return cv::Size(a.width + i, a.height + i);
}

cv::Size operator *(const cv::Size& a, const float& f)
{
	return cv::Size(cvRound(a.width * f), cvRound(a.height * f));
}

cv::Size operator /(const cv::Size& a, const float& f)
{
	return cv::Size(cvRound(a.width / f), cvRound(a.height / f));
}

bool operator <=(const cv::Size& a, const cv::Size& b)
						{
	return a.width <= b.width && a.height <= b.width;
						}

/*-------to calculate time-------------*/

double get_wall_timelbp(){
	struct timeval time;
	if (gettimeofday(&time,NULL)){
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

/*--------------------*/


struct PyrLavel
{
	PyrLavel(int _order, float _scale, cv::Size frame, cv::Size window, cv::Size minObjectSize)
	{
		do
		{
			order = _order;
			scale = pow(_scale, order);
			sFrame = frame / scale;
			workArea = sFrame - window + 1;
			sWindow = window * scale;
			_order++;
		} while (sWindow <= minObjectSize);
	}

	bool isFeasible(cv::Size maxObj)
	{
		return workArea.width > 0 && workArea.height > 0 && sWindow <= maxObj;
	}

	PyrLavel next(float factor, cv::Size frame, cv::Size window, cv::Size minObjectSize)
	{
		return PyrLavel(order + 1, factor, frame, window, minObjectSize);
	}

	int order;
	float scale;
	cv::Size sFrame;
	cv::Size workArea;
	cv::Size sWindow;
};

struct Stage
{
	int    first;
	int    ntrees;
	float  threshold;
};

struct ClNode
{
	int   left;
	int   right;
	int   featureIdx;
};

// to convert other data types to string
template <typename T>
std::string to_stringlbp(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

/*-------declaring array to hold time*/
double kernelExTime[3]={0.0,0.0,0.0};
string AvgkernelExTime[6][2];
string wakeupkernelExTime[2][2];
double gettime_tocalc2[6];
int loopcnt[2]={0,0};




struct LbpCascade 
{
public:

	LbpCascade(){}
	~LbpCascade(){}

	unsigned int process(ocl::oclMat& image, cl_mem candidatebuffer, int outputsz, float scaleFactor, bool /*findLargestObject*/,
			bool /*visualizeInPlace*/, cv::Size minObjectSize, cv::Size maxObjectSize,
			cv::ocl::Context* clCxt,bool calTime,int it)
	{
		CV_Assert(scaleFactor > 1 && image.depth() == CV_8U);

		if (maxObjectSize == cv::Size())
			maxObjectSize = image.size();

		allocateBuffers(image.size());
		cl_command_queue qu = reinterpret_cast<cl_command_queue>(clCxt->oclCommandQueue());

		unsigned int classified = 0;
		//cudaSafeCall( cudaMemcpy(dclassified.ptr(), &classified, sizeof(int), cudaMemcpyHostToDevice) );
		cl_mem dclassified = ocl::openCLCreateBuffer(clCxt, CL_MEM_READ_WRITE, sizeof(int));
		int size_of_features=features.size();
		uchar *newFeatures=(uchar*)malloc(sizeof(uchar)*size_of_features);
		for(int i=0;i<size_of_features;i++)
		{
			newFeatures[i]=features[i];
		}

		cl_mem features_host = ocl::openCLCreateBuffer(clCxt, CL_MEM_READ_ONLY, size_of_features*sizeof(uchar));
		clEnqueueWriteBuffer(qu, dclassified, 1, 0,
				sizeof(cl_int),
				&classified, 0, NULL, NULL);
		clEnqueueWriteBuffer(qu, features_host, 1, 0,
				size_of_features*sizeof(uchar),
				newFeatures, 0, NULL, NULL);

		cout<<"frames number...........: "<<it<<endl;
		
		/**********************checking which image formats are supported********************/
	    // Determine and show image format support
	                cl_uint uiNumSupportedFormats = 0;

	                // 2D
	                clGetSupportedImageFormats((cl_context)image.clCxt->oclContext(),
	                						   CL_MEM_WRITE_ONLY,
	                                           CL_MEM_OBJECT_IMAGE2D,
	                                           NULL, NULL, &uiNumSupportedFormats);

	                cl_image_format* ImageFormats = new cl_image_format[uiNumSupportedFormats];
	                clGetSupportedImageFormats((cl_context)image.clCxt->oclContext(),
	                						   CL_MEM_WRITE_ONLY,
	                                           CL_MEM_OBJECT_IMAGE2D,
	                                           uiNumSupportedFormats, ImageFormats, NULL);
	                printf("  ---------------------------------\n");
	                printf("  2D Image Formats Supported (%u)\n", uiNumSupportedFormats);
	                printf("  ---------------------------------\n");
	                printf("  %-6s%-16s%-22s\n\n", "#", "Channel Order", "Channel Type");
	                for(unsigned int i = 0; i < uiNumSupportedFormats; i++)
	                {
	                		/*	printf("  %-6u%-16s%-22s\n", (i + 1),
	                            oclImageFormatString(ImageFormats[i].image_channel_order),
	                            oclImageFormatString(ImageFormats[i].image_channel_data_type));
							*/
							printImageFormat(ImageFormats[i]);

	                }
	                printf("\n");
	                delete [] ImageFormats;

		//cl_mem tex_integral = clCreateImage((cl_context)image.clCxt->oclContext(), CL_MEM_WRITE_ONLY , &format, &imageDesc, 0,  &status);
		/******************************************checking ends****************************/

		//declaring texture memory to hold integral data

				int integralsize=(integralFactor * (image.cols + 1))*(image.rows + 1)*sizeof(cl_int);
				cl_mem integral_mem = ocl::openCLCreateBuffer(clCxt, CL_MEM_READ_ONLY,integralsize );
				cl_int status = 0;
				cl_image_format format;
				format.image_channel_data_type = CL_UNSIGNED_INT16;
				format.image_channel_order = CL_R;		
				/*cl_image_desc imageDesc;
				memset(&imageDesc, '\0', sizeof(cl_image_desc));
				imageDesc.image_type = CL_MEM_OBJECT_IMAGE2D;
				imageDesc.image_width = integralFactor * (image.cols + 1);
				imageDesc.image_height = image.rows + 1;
				cl_mem tex_integral = clCreateImage((cl_context)image.clCxt->oclContext(), CL_MEM_WRITE_ONLY , 
										&format, &imageDesc, 0,  &status);
				if (status != CL_SUCCESS )
					return 0;*/
				size_t image_width=integralFactor * (image.cols + 1);
				size_t image_height=image.rows + 1;
				size_t image_row_pitch;

				cl_mem tex_integral = clCreateImage2D((cl_context)image.clCxt->oclContext(),
						CL_MEM_WRITE_ONLY ,
						&format,
						40,
						40,
						0,
						NULL,
						&status);



				if (status != CL_SUCCESS )
					cout<<"Image memory creation unsuccessful...error code: "<<status<<endl;
				
//declaring texture memory ends

		PyrLavel level(0, scaleFactor, image.size(), NxM, minObjectSize);

		double kernelTime_col=0;

		int loopCount=0;
		int outloopCount=0;
		double lbptime=0.0;
		while (level.isFeasible(maxObjectSize))  //
		{
			int acc = level.sFrame.width + 1;
			float iniScale = level.scale;
			//integral = 0;

			cv::Size area = level.workArea;
			int step = 1 + (level.scale <= 2.f);

			int total = 0, prev  = 0;

			while (acc <= integralFactor * (image.cols + 1) && level.isFeasible(maxObjectSize))//
			{
				// create sutable matrix headers
				ocl::oclMat src  = resuzeBuffer(cv::Rect(0, 0, level.sFrame.width, level.sFrame.height));
				ocl::oclMat sint = integral(cv::Rect(prev, 0, level.sFrame.width + 1, level.sFrame.height + 1));
				//ocl::oclMat buff = integralBuffer; //no use in small local memory yuli1009

				ocl::resize(image, src, level.sFrame, 0, 0, CV_INTER_LINEAR);
				Mat	srcHost;
				src.download(srcHost);


				/*std::stringstream strFname4;
				printf("loop count %d",loopCount);
				strFname4 << "Integral_host"<<loopCount<<".txt";
				const char* filenames1=strFname4.str().c_str();
				FILE *fp1 = fopen(filenames1, "w");*/

			/*	fprintf(fp1,"Columnn %2d Rows: %d\n: ", srcHost.cols,srcHost.rows);
				for (int i = 0; i < srcHost.rows; i++)
				{
					fprintf(fp1,"\nrow %2d: ", i);
					for (int j = 0; j < srcHost.cols; j++)
					{
						fprintf(fp1," %5d ", srcHost.at<uchar>(i,j));
					}
				}
				fclose(fp1);  //comment out, save mem*/
				/*if(loopCount==3)
				{
				  imwrite("scaled_3.jpg", srcHost);
				}*/
				//------------small local memory begins here--------------------//
				const char * build_options = "-D STUMP_BASED=1";
				int vlen = 4; //int 4bytes??yuli1008
				int offset = src.offset / vlen;
				int pre_invalid = src.offset % vlen;
				ocl::oclMat t_sum;
				t_sum.create(src.cols, src.rows, CV_32FC1);
				int sum_offset = sint.offset / vlen;
				int dst_step = t_sum.step / vlen;
				vector<pair<size_t , const void *> > args;

				ocl::oclMat lm_sum_mem;

				lm_sum_mem.create(src.cols,src.rows, CV_32FC1);

				ocl::oclMat temp;
				temp.create(src.rows,src.cols, CV_32FC1);

				int npixels=src.rows*src.step;

				//cout<< "src rows width "<<src.rows<<endl;
				//cout<< "src cols width "<<src.cols<<endl;



			/*args.push_back( make_pair( sizeof(cl_mem) , (void *)&src.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&t_sum.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&offset ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&pre_invalid ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.cols ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&dst_step)); //yuli1008, small local
				size_t gt[3] = {(src.cols + 1) / 2 * 32, 1, 1}, lt[3] = {32, 1, 1}; *///yuli1009, small local

				/*args.push_back( make_pair( sizeof(cl_mem) , (void *)&src.data ));

				args.push_back( make_pair( sizeof(cl_mem) , (void *)&lm_sum_mem.data));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&temp.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&npixels));*/

				args.push_back( make_pair( sizeof(cl_mem) , (void *)&src.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.cols ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&lm_sum_mem.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&npixels));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&src.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&lm_sum_mem.step ));

				size_t gt[3] = {src.rows*src.step, 1, 1}, lt[3] = {32, 1, 1};
				/*cout<<"loopCount..................................."<<loopCount<<endl;
				cout<<"src rows......................."<<src.rows<<endl;
				cout<<"src step......................."<<src.step<<endl;
				cout<<"threads..................... :  "<<src.rows*src.step<<endl;*/
				if(calTime)
				{
					//double dtKERNL1=get_wall_timelbp();
					gettime_tocalc2[0]=get_wall_timelbp();
				}


				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "v2_integral_cols", gt, lt, args, -1, -1,build_options);
				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "v2_integral_cols_sum", gt, lt, args, -1, -1,build_options);

#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//			double dtKERNL2=get_wall_timelbp();
					gettime_tocalc2[1]=get_wall_timelbp();
					/*wakeupkernelExTime[0][0]="wakeup time for integral cols";
			wakeupkernelExTime[0][1]=to_stringlbp(dtKERNL2-dtKERNL1);*/
				}


				kernelExTime[0]=kernelExTime[0]+(gettime_tocalc2[1]-gettime_tocalc2[0]);

#ifdef DEBUG_INTEGRAL_COLS

			/*	cout<< "src rows width "<<src.rows<<endl;
				cout<< "src cols width "<<src.cols<<endl;
				cout<< "src src_offset "<<offset<<endl;
				cout<< "src pre_invalid "<<pre_invalid<<endl;
				cout<< "src rows step "<<src.step<<endl;
				cout<< "src dst_step "<<dst_step<<endl;
				cout<< "lm_sum_mem.step "<<lm_sum_mem.step<<endl;
				cout<< "lm_sum_mem.rows "<<lm_sum_mem.rows<<endl;
				cout<< "lm_sum_mem.cols "<<lm_sum_mem.cols<<endl;*/

				Mat t_sumhost;
				lm_sum_mem.download(t_sumhost);
				std::stringstream strFname3;
				printf("loop count %d",loopCount);
				strFname3 << "Integral_Cols"<<loopCount<<".txt";
				const char* filenames=strFname3.str().c_str();
				FILE *fp = fopen(filenames, "w");

				fprintf(fp,"Columnn %2d Rows: %d\n: ", t_sumhost.cols,t_sumhost.rows);
				for (int i = 0; i < t_sumhost.rows; i++)
				{
					fprintf(fp,"\nrow %2d: ", i);
					for (int j = 0; j < t_sumhost.cols; j++)
					{
						fprintf(fp," %5d ", t_sumhost.at<int>(i,j));
					}
				}
				fclose(fp);  //comment out, save mem*/

				//cout<< "lm_sum_mem. values "<<t_sumhost.at<int>(1,0)<<endl;





#endif


				int sum_step = sint.step / vlen;
				args.clear();
			/*	args.push_back( make_pair( sizeof(cl_mem) , (void *)&t_sum.data ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&sint.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&t_sum.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&t_sum.cols ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&t_sum.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&sum_step)); //yuli1009, small local
				args.push_back( make_pair( sizeof(cl_int) , (void *)&sum_offset)); //yuli1009, small local
				size_t gt2[3] = {(t_sum.cols + 1)/2 * 32, 1, 1}, lt2[3] = {32, 1, 1};

		*/		args.push_back( make_pair( sizeof(cl_mem) , (void *)&lm_sum_mem.data ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&lm_sum_mem.rows ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&lm_sum_mem.cols ));
				args.push_back( make_pair( sizeof(cl_mem) , (void *)&sint.data));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&npixels));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&lm_sum_mem.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&sint.step ));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&sum_offset));
				args.push_back( make_pair( sizeof(cl_int) , (void *)&loopCount));

				size_t gt2[3] = {lm_sum_mem.rows*lm_sum_mem.step, 1, 1}, lt2[3] = {32, 1, 1};

			/*	"__kernel void "
				"  v2_integral_rows_sum(__global int *src,\n"
				"int rows,int cols,__global int *lm_sum, int pixels,int steps,int o_steps)\n"
				*/


				if( calTime)
				{
					//double dtKERNLr1=get_wall_timelbp();
					gettime_tocalc2[2]=get_wall_timelbp();
				}
				//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "v2_integral_rows", gt2, lt2, args, -1, -1,build_options);
				ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "v2_integral_rows_sum", gt2, lt2, args, -1, -1,build_options);

#ifdef KERNEL_TIME
				clFinish(qu);
#endif
				if( calTime)
				{
					//double dtKERNLr2=get_wall_timelbp();
					gettime_tocalc2[3]=get_wall_timelbp();
				}


				kernelExTime[1]=kernelExTime[1]+(gettime_tocalc2[3]-gettime_tocalc2[2]);


#ifdef DEBUG_INTEGRAL_ROWS
			/*	cout<<endl;
				cout<<"...sum offset.......: "<<sum_offset<<endl;
				cout<<"...sum sum_step.......: "<<sum_step<<endl;
			//	cout<<"...expression from rows.......: "<<sum_offset+0*168+168+0+0<<endl;


				/*cout<< "lm_sum_mem rows width "<<lm_sum_mem.rows<<endl;
				cout<< "lm_sum_mem cols width "<<lm_sum_mem.cols<<endl;

				cout<< "lm_sum_mem rows step "<<lm_sum_mem.step<<endl;

				cout<< "sint.step "<<sint.step<<endl;
				cout<< "sint.rows "<<sint.rows<<endl;
				cout<< "sint.cols "<<sint.cols<<endl;*/

				Mat sinthost;
				sint.download(sinthost);
				std::stringstream strFname2;
				strFname2 << "Integral_Rows"<<loopCount<<".txt";
				const char* filename=strFname2.str().c_str();
				FILE *fp2 = fopen(filename, "w");

				fprintf(fp2,"Columnn %2d Rows: %d\n: ", sinthost.cols,sinthost.rows);
				for (int i = 0; i < sinthost.rows; i++)
				{
					fprintf(fp2,"\nrow %2d: ", i);
					for (int j = 0; j < sinthost.cols; j++)
					{
						fprintf(fp2," %5d ", sinthost.at<int>(i,j));
					}
				}
				fclose(fp2); ///comment out, save mem*/
#endif


				// calculate job
				int totalWidth = level.workArea.width / step;
				total += totalWidth * (level.workArea.height / step);

				// go to next pyramide level
				level = level.next(scaleFactor, image.size(), NxM, minObjectSize);
				area = level.workArea;

				step = (1 + (level.scale <= 2.f));
				prev = acc;
				acc += level.sFrame.width + 1;
		if( calTime)
				{
					loopCount++;

					loopcnt[0]=loopcnt[0]+1;

				}


			}
			//device::lbp::classifyPyramid(image.cols, image.rows, NxM.width - 1, NxM.height - 1, iniScale, scaleFactor,
			//total, stage_mat, stage_mat.cols / sizeof(Stage), nodes_mat,
			//leaves_mat, subsets_mat, features_mat, subsetSize, candidates, dclassified.ptr<unsigned int>(), integral);

			//adding texture memory, converting the existing integral to texture memory
						Mat	ingrHost;
						integral.download(ingrHost);

						int *integral_temp=(int*)malloc(integralsize);
						int c=0;
						for (int i = 0; i < ingrHost.rows; i++)
						{


							for (int j = 0; j < ingrHost.cols; j++)
							{
								integral_temp[c]=ingrHost.at<int>(i,j);
								c++;
							}

						}

						clEnqueueWriteBuffer(qu, integral_mem, 1, 0,
								integralsize,
								integral_temp, 0, NULL, NULL);

						size_t src_offset=0;
						const size_t region[3]={image_width,image_height,1};

						const size_t dst_origin[3]={0,0,0};
						cl_event w2;
						cl_int succ= clEnqueueCopyBufferToImage(qu,integral_mem,tex_integral,0 ,dst_origin,region,0,NULL,NULL);
						if(succ !=CL_SUCCESS)
							cout<<"erorrrrrrrrrrrrrrrrrrrrrrrr..copying buffer to image"<<endl;
						//clReleaseMemObject(integral_mem);
						//free(integral_temp);
						//adding texture memory completes


			const int block = 32; //yuli0826
			size_t localThreads[3] = { 32, 1, 1 };
			size_t globalThreads[3] = { divUp(total, block), 1, 1};
			//size_t globalThreads[3] = { 128, 1, 1};
			size_t gthread =globalThreads[0];
			int globalthreads_value=(int)(ceilf((float)total / (float)block)) * block;

			int is = (int)integral.step / sizeof(int) ;
			int NxMmiw = NxM.width - 1;
			int NxMmih = NxM.height - 1;
			int nstages = stage_mat.cols / sizeof(Stage);

			//cout<<"subsetSize: "<<subsetSize<<endl;

			vector<pair<size_t, const void *> > args;
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&stage_mat.data ));
			args.push_back ( make_pair(sizeof(cl_int) , (void *)&nstages ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&nodes_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&leaves_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&subsets_mat.data ));
			//args.push_back ( make_pair(sizeof(cl_mem) , (void *)&features_mat.data ));
			args.push_back ( make_pair(sizeof(cl_mem) , (void *)&features_host ));
			args.push_back ( make_pair(sizeof(cl_int) , (void *)&subsetSize ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&image.cols ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&image.rows ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&NxMmiw  )); //NxM.width-1, yuli0823
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&NxMmih  )); //NxM.height-1, yuli0823
			args.push_back ( make_pair(sizeof(cl_float), (void *)&iniScale ));
			args.push_back ( make_pair(sizeof(cl_float), (void *)&scaleFactor ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&total ));
			//args.push_back ( make_pair(sizeof(cl_mem) ,  (void *)&integral.data ));
			args.push_back ( make_pair(sizeof(cl_mem) ,  (void *)&tex_integral ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&is  ));
			args.push_back ( make_pair(sizeof(cl_mem)  , (void *)&candidatebuffer ));
			args.push_back ( make_pair(sizeof(cl_int)  , (void *)&outputsz  ));
			args.push_back ( make_pair(sizeof(cl_mem) ,  (void *)&dclassified));

			const char * build_options = "-D STUMP_BASED=1";
double dtlbp1=0.0;
			if( calTime)
			{
				// dtlbp1=get_wall_timelbp();
				gettime_tocalc2[4]=get_wall_timelbp();
			}

			ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "lbp_cascade", globalThreads, localThreads, args, -1, -1, build_options);
						//ocl::openCLExecuteKernel(clCxt, &lbpobjectdetect, "lbp_cascade_buf", globalThreads, localThreads, args, -1, -1, build_options);
#ifdef DEBUG_HOST_CODE

/*
			cout<<"integral cols number:"<<integral.cols<<endl;
			cout<<"size of gthread"<<gthread<<endl;
			cout<<"size of globalthreads_value"<<globalthreads_value<<endl;
*/

#endif
#ifdef KERNEL_TIME
			clFinish(qu);
#endif
double dtlbp2=0.0;
			if( calTime)
			{
				//dtlbp2=get_wall_timelbp();
				gettime_tocalc2[5]=get_wall_timelbp();
			}

			kernelExTime[2]=kernelExTime[2]+(gettime_tocalc2[5]-gettime_tocalc2[4]);

			lbptime=lbptime+gettime_tocalc2[5]-gettime_tocalc2[4];

			//cout<<"detection time: "<<gettime_tocalc2[5]-gettime_tocalc2[4]<<endl;
			//cout<<"detection time 2: "<<(dtlbp2-dtlbp1)<<endl;
//printf("detection value print :%f \n",dtlbp2-dtlbp1);



			if( calTime)
			{
				outloopCount++;
				loopcnt[1]=loopcnt[1]+1;
			}
			//clReleaseMemObject(tex_integral);

		}

		cout<<"detection time total: "<<lbptime<<endl;

		clEnqueueReadBuffer(qu, dclassified, 1, 0,
				sizeof(cl_uint),
				&classified, 0, NULL, NULL);

		return classified;

	}


	bool read(const string& classifierAsXml)
	{
		FileStorage fs(classifierAsXml, FileStorage::READ);
		return fs.isOpened() ? read(fs.getFirstTopLevelNode()) : false;
	}

private:

	void allocateBuffers(cv::Size frame)
	{
		if (frame == cv::Size())
			return;

		if (resuzeBuffer.empty() || frame.width > resuzeBuffer.cols || frame.height > resuzeBuffer.rows)
		{
			resuzeBuffer.create(frame, CV_8UC1);

			integral.create(frame.height + 1, integralFactor * (frame.width + 1), CV_32SC1);
			//         Size roiSize;
			//         roiSize.width = frame.width;
			//         roiSize.height = frame.height;
			//
			//int bufSize = frame.width * frame.height;        //yuli0828,  integral size???
			//         integralBuffer.create(1, bufSize, CV_8UC1);    //no use in small local memory yuli1009

		//	cout<<"integral buffer step: "<<integral.step<<endl;
		}
	}

	bool read(const FileNode &root)
	{
		const char *GPU_CC_STAGE_TYPE       = "stageType";
		const char *GPU_CC_FEATURE_TYPE     = "featureType";
		const char *GPU_CC_BOOST            = "BOOST";
		const char *GPU_CC_LBP              = "LBP";
		const char *GPU_CC_MAX_CAT_COUNT    = "maxCatCount";
		const char *GPU_CC_HEIGHT           = "height";
		const char *GPU_CC_WIDTH            = "width";
		const char *GPU_CC_STAGE_PARAMS     = "stageParams";
		const char *GPU_CC_MAX_DEPTH        = "maxDepth";
		const char *GPU_CC_FEATURE_PARAMS   = "featureParams";
		const char *GPU_CC_STAGES           = "stages";
		const char *GPU_CC_STAGE_THRESHOLD  = "stageThreshold";
		const float GPU_THRESHOLD_EPS       = 1e-5f;
		const char *GPU_CC_WEAK_CLASSIFIERS = "weakClassifiers";
		const char *GPU_CC_INTERNAL_NODES   = "internalNodes";
		const char *GPU_CC_LEAF_VALUES      = "leafValues";
		const char *GPU_CC_FEATURES         = "features";
		const char *GPU_CC_RECT             = "rect";

		std::string stageTypeStr = (string)root[GPU_CC_STAGE_TYPE];
		CV_Assert(stageTypeStr == GPU_CC_BOOST);

		string featureTypeStr = (string)root[GPU_CC_FEATURE_TYPE];
		CV_Assert(featureTypeStr == GPU_CC_LBP);

		NxM.width =  (int)root[GPU_CC_WIDTH];
		NxM.height = (int)root[GPU_CC_HEIGHT];
		CV_Assert( NxM.height > 0 && NxM.width > 0 );

		isStumps = ((int)(root[GPU_CC_STAGE_PARAMS][GPU_CC_MAX_DEPTH]) == 1) ? true : false;
		CV_Assert(isStumps);

		FileNode fn = root[GPU_CC_FEATURE_PARAMS];
		if (fn.empty())
			return false;

		ncategories = fn[GPU_CC_MAX_CAT_COUNT];  //GPU_CC_MAX_CAT_COUNT=256;

		subsetSize = (ncategories + 31) / 32;  //
		//"int idx =  (subsets[ current_node * subsetSize + c] & ( 1 << shift)) ? current_leave : current_leave + 1;\n"
		nodeStep = 3 + ( ncategories > 0 ? subsetSize : 1 );//

		//cout<<"ncategories "<<ncategories<<"subsetSize "<<subsetSize<<"nodeStep "<<nodeStep<<endl;
		//ncategories 256 subsetSize 8 nodeStep 11

		fn = root[GPU_CC_STAGES];
		if (fn.empty())
			return false;

		std::vector<Stage> stages;
		stages.reserve(fn.size());

		std::vector<int> cl_trees;
		std::vector<int> cl_nodes;
		std::vector<float> cl_leaves;
		std::vector<int> subsets;

		FileNodeIterator it = fn.begin(), it_end = fn.end();
		for (size_t si = 0; it != it_end; si++, ++it )
		{
			FileNode fns = *it;
			Stage st;
			st.threshold = (float)fns[GPU_CC_STAGE_THRESHOLD] - GPU_THRESHOLD_EPS;  //why???

			fns = fns[GPU_CC_WEAK_CLASSIFIERS];
			if (fns.empty())
				return false;

			st.ntrees = (int)fns.size();
			st.first = (int)cl_trees.size();

			stages.push_back(st);// (int, int, float)

			cl_trees.reserve(stages[si].first + stages[si].ntrees);

			// weak trees
			FileNodeIterator it1 = fns.begin(), it1_end = fns.end();
			for ( ; it1 != it1_end; ++it1 )
			{
				FileNode fnw = *it1;

				FileNode internalNodes = fnw[GPU_CC_INTERNAL_NODES];
				FileNode leafValues = fnw[GPU_CC_LEAF_VALUES];
				if ( internalNodes.empty() || leafValues.empty() )
					return false;

				int nodeCount = (int)internalNodes.size()/nodeStep;
				cl_trees.push_back(nodeCount);

				cl_nodes.reserve((cl_nodes.size() + nodeCount) * 3);
				cl_leaves.reserve(cl_leaves.size() + leafValues.size());

				if( subsetSize > 0 )
					subsets.reserve(subsets.size() + nodeCount * subsetSize);

				// nodes
				FileNodeIterator iIt = internalNodes.begin(), iEnd = internalNodes.end();

				for( ; iIt != iEnd; )
				{
					cl_nodes.push_back((int)*(iIt++));
					cl_nodes.push_back((int)*(iIt++));
					cl_nodes.push_back((int)*(iIt++));

					if( subsetSize > 0 )
						for( int j = 0; j < subsetSize; j++, ++iIt )
							subsets.push_back((int)*iIt);
				}

				// leaves
				iIt = leafValues.begin(), iEnd = leafValues.end();
				for( ; iIt != iEnd; ++iIt )
					cl_leaves.push_back((float)*iIt);
			}
		}

		fn = root[GPU_CC_FEATURES];
		if( fn.empty() )
			return false;
		//std::vector<uchar> features;
		features.reserve(fn.size() * 4);
		FileNodeIterator f_it = fn.begin(), f_end = fn.end();
		for (; f_it != f_end; ++f_it)
		{
			FileNode rect = (*f_it)[GPU_CC_RECT];
			FileNodeIterator r_it = rect.begin();
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
			features.push_back(saturate_cast<uchar>((int)*(r_it++)));
		}

		// copy data structures on gpu
		stage_mat.upload(cv::Mat(1, (int) (stages.size() * sizeof(Stage)), CV_8UC1, (uchar*)&(stages[0]) ));
		trees_mat.upload(cv::Mat(cl_trees).reshape(1,1));
		nodes_mat.upload(cv::Mat(cl_nodes).reshape(1,1));
		leaves_mat.upload(cv::Mat(cl_leaves).reshape(1,1));
		subsets_mat.upload(cv::Mat(subsets).reshape(1,1));
		features_mat.upload(cv::Mat(features).reshape(1,1));

		return true;
	}
	
	
		const char* oclImageFormatString(cl_uint uiImageFormat)
	{
	    // cl_channel_order
	    if (uiImageFormat == CL_R)return "CL_R";
	    if (uiImageFormat == CL_A)return "CL_A";
	    if (uiImageFormat == CL_RG)return "CL_RG";
	    if (uiImageFormat == CL_RA)return "CL_RA";
	    if (uiImageFormat == CL_RGB)return "CL_RGB";
	    if (uiImageFormat == CL_RGBA)return "CL_RGBA";
	    if (uiImageFormat == CL_BGRA)return "CL_BGRA";
	    if (uiImageFormat == CL_ARGB)return "CL_ARGB";
	    if (uiImageFormat == CL_INTENSITY)return "CL_INTENSITY";
	    if (uiImageFormat == CL_LUMINANCE)return "CL_LUMINANCE";

	    // cl_channel_type
	    if (uiImageFormat == CL_SNORM_INT8)return "CL_SNORM_INT8";
	    if (uiImageFormat == CL_SNORM_INT16)return "CL_SNORM_INT16";
	    if (uiImageFormat == CL_UNORM_INT8)return "CL_UNORM_INT8";
	    if (uiImageFormat == CL_UNORM_INT16)return "CL_UNORM_INT16";
	    if (uiImageFormat == CL_UNORM_SHORT_565)return "CL_UNORM_SHORT_565";
	    if (uiImageFormat == CL_UNORM_SHORT_555)return "CL_UNORM_SHORT_555";
	    if (uiImageFormat == CL_UNORM_INT_101010)return "CL_UNORM_INT_101010";
	    if (uiImageFormat == CL_SIGNED_INT8)return "CL_SIGNED_INT8";
	    if (uiImageFormat == CL_SIGNED_INT16)return "CL_SIGNED_INT16";
	    if (uiImageFormat == CL_SIGNED_INT32)return "CL_SIGNED_INT32";
	    if (uiImageFormat == CL_UNSIGNED_INT8)return "CL_UNSIGNED_INT8";
	    if (uiImageFormat == CL_UNSIGNED_INT16)return "CL_UNSIGNED_INT16";
	    if (uiImageFormat == CL_UNSIGNED_INT32)return "CL_UNSIGNED_INT32";
	    if (uiImageFormat == CL_HALF_FLOAT)return "CL_HALF_FLOAT";
	    if (uiImageFormat == CL_FLOAT)return "CL_FLOAT";

	    // unknown constant
	    return "Unknown";
	}
	
		void printImageFormat(cl_image_format format)
	{
	#define CASE(order) case order: cout << #order; break;
	  switch (format.image_channel_order)
	  {
	    CASE(CL_R);
	    CASE(CL_A);
	    CASE(CL_RG);
	    CASE(CL_RA);
	    CASE(CL_RGB);
	    CASE(CL_RGBA);
	    CASE(CL_BGRA);
	    CASE(CL_ARGB);
	    CASE(CL_INTENSITY);
	    CASE(CL_LUMINANCE);
	    CASE(CL_Rx);
	    CASE(CL_RGx);
	    CASE(CL_RGBx);
	   }
	#undef CASE

	  cout << " - ";

	#define CASE(type) case type: cout << #type; break;
	  switch (format.image_channel_data_type)
	  {
	    CASE(CL_SNORM_INT8);
	    CASE(CL_SNORM_INT16);
	    CASE(CL_UNORM_INT8);
	    CASE(CL_UNORM_INT16);
	    CASE(CL_UNORM_SHORT_565);
	    CASE(CL_UNORM_SHORT_555);
	    CASE(CL_UNORM_INT_101010);
	    CASE(CL_SIGNED_INT8);
	    CASE(CL_SIGNED_INT16);
	    CASE(CL_SIGNED_INT32);
	    CASE(CL_UNSIGNED_INT8);
	    CASE(CL_UNSIGNED_INT16);
	    CASE(CL_UNSIGNED_INT32);
	    CASE(CL_HALF_FLOAT);
	    CASE(CL_FLOAT);
	
	  }
	#undef CASE

	  cout << endl;
	}
	
	enum stage { BOOST = 0 };
	enum feature { LBP = 1, HAAR = 2 };
	static const stage stageType = BOOST;
	static const feature featureType = LBP;

	cv::Size NxM;
	bool isStumps;
	int ncategories;
	int subsetSize;
	int nodeStep;

	// OpenCL representation of classifier
	ocl::oclMat stage_mat;
	ocl::oclMat trees_mat;
	ocl::oclMat nodes_mat;
	ocl::oclMat leaves_mat;
	ocl::oclMat subsets_mat;
	ocl::oclMat features_mat;

	ocl::oclMat integral;
	ocl::oclMat integralBuffer;
	ocl::oclMat resuzeBuffer;
	std::vector<uchar> features;
	static const int integralFactor = 4;
};

