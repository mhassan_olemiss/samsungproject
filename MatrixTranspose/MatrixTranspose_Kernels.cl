/**********************************************************************************************************************
 */
__kernel void sharedMemoryTranspose(__global float *output, __global float *input, __local float* tile, int width, int height, const uint blockSize)
{
	uint globalIdx = get_global_id(0);
	uint globalIdy = get_global_id(1);
	
	uint localIdx = get_local_id(0);
	uint localIdy = get_local_id(1);
	
    /* copy from input to local memory */
	tile[localIdy*blockSize + localIdx] = input[globalIdy * width + globalIdx];

    /* wait until the whole block is filled */
	barrier(CLK_LOCAL_MEM_FENCE);

	uint groupIdx = get_group_id(0);
	uint groupIdy = get_group_id(1);

    /* calculate the corresponding target location for transpose  by inverting x and y values*/
	uint targetGlobalIdx = groupIdy * blockSize + localIdy;
	uint targetGlobalIdy = groupIdx * blockSize + localIdx;

    /* calculate the corresponding raster indices of source and target */
	uint targetIndex  = targetGlobalIdy * height     + targetGlobalIdx;
	uint sourceIndex  = localIdy       * blockSize + localIdx;
	
	output[targetIndex] = tile[sourceIndex];
}


__kernel void copy(__global float *odata, __global float *idata, __local float* tile, int width, int height, const uint TILE_DIM, const uint BLOCK_ROWS, int nreps)
{
	uint blockIdx = get_group_id(0);
	uint blockIdy = get_group_id(1);
	
	uint threadIdx = get_local_id(0);
	uint threadIdy = get_local_id(1);
    
    int xIndex = blockIdx * TILE_DIM + threadIdx;
    int yIndex = blockIdy * TILE_DIM + threadIdy;
    int index_in = xIndex + width * yIndex;
    
    for (int r=0; r < nreps; r++) 
    {
        /*for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) 
        {
            odata[index_out+i] = idata[index_in+i*width];
        }*/
        odata[index_in] = idata[index_in];
    }
}

__kernel void naiveTranspose(__global float *odata, __global float *idata, __local float* tile, int width, int height, const uint TILE_DIM, const uint BLOCK_ROWS, int nreps)
{
	uint blockIdx = get_group_id(0);
	uint blockIdy = get_group_id(1);
	
	uint threadIdx = get_local_id(0);
	uint threadIdy = get_local_id(1);
    
    int xIndex = blockIdx * TILE_DIM + threadIdx;
    int yIndex = blockIdy * TILE_DIM + threadIdy;
    int index_in = xIndex + width * yIndex;
    int index_out = yIndex + height * xIndex;
    
    for (int r=0; r < nreps; r++) 
    {
        /*for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) 
        {
            odata[index_out+i] = idata[index_in+i*width];
        }*/
        odata[index_out] = idata[index_in];
    }
}

__kernel void coalescedTranspose(__global float *odata, __global float *idata, __local float* tile, int width, int height, const uint TILE_DIM, const uint BLOCK_ROWS, int nreps)
{
	uint blockIdx = get_group_id(0);
	uint blockIdy = get_group_id(1);
	
	uint threadIdx = get_local_id(0);
	uint threadIdy = get_local_id(1);
    
    int xIndex = blockIdx * TILE_DIM + threadIdx;
    int yIndex = blockIdy * TILE_DIM + threadIdy;
    int index_in = xIndex + (yIndex) * width;
    
    xIndex = blockIdy * TILE_DIM + threadIdx;
    yIndex = blockIdx * TILE_DIM + threadIdy;
    int index_out = xIndex + (yIndex) * height;
    
    for (int r=0; r < nreps; r++) 
    {
        /*for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) 
        {
            tile[(threadIdy + i) * TILE_DIM + threadIdx] =
            idata[index_in + i * width];
        }
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) 
        {
            odata[index_out + i * height] =
            tile[(threadIdx) * TILE_DIM + threadIdy + i];
        }*/
        
        tile[threadIdy  * TILE_DIM + threadIdx] = idata[index_in];
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        odata[index_out] = tile[threadIdx * TILE_DIM + threadIdy];
    }
}

__kernel void diagonalTranspose(__global float *odata, __global float *idata, __local float* tile, int width, int height, const uint TILE_DIM, const uint BLOCK_ROWS, int nreps)
{
	uint blockIdx = get_group_id(0);
	uint blockIdy = get_group_id(1);
	
	uint threadIdx = get_local_id(0);
	uint threadIdy = get_local_id(1);
    
    uint gridDimx = get_num_groups(0);
    uint gridDimy = get_num_groups(1);

   int blockIdx_x, blockIdx_y;

    // do diagonal reordering
    if (width == height) 
    {
        blockIdx_y = blockIdx;
        blockIdx_x = (blockIdx+blockIdy) % gridDimx;
    } 
    else
    {
        int bid = blockIdx + gridDimx*blockIdy;
        blockIdx_y = bid % gridDimy;
        blockIdx_x = ((bid/gridDimy)+blockIdx_y) % gridDimx;
    }    
    blockIdx = blockIdx_x;
    blockIdy = blockIdx_y;

    int xIndex = blockIdx * TILE_DIM + threadIdx;
    int yIndex = blockIdy * TILE_DIM + threadIdy;
    int index_in = xIndex + (yIndex) * width;
    
    xIndex = blockIdy * TILE_DIM + threadIdx;
    yIndex = blockIdx * TILE_DIM + threadIdy;
    int index_out = xIndex + (yIndex) * height;
    
    for (int r=0; r < nreps; r++) 
    {
        /*for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) 
        {
            tile[(threadIdy + i) * TILE_DIM + threadIdx] =
            idata[index_in + i * width];
        }
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) 
        {
            odata[index_out + i * height] =
            tile[(threadIdx) * TILE_DIM + threadIdy + i];
        }*/
        
        tile[threadIdy  * TILE_DIM + threadIdx] = idata[index_in];
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        odata[index_out] = tile[threadIdx * TILE_DIM + threadIdy];
    }
    
    //odata[index_out] = idata[index_in];
}