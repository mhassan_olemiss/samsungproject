#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "SDKCommon.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define clSafe(status) _clSafe(__FILE__,__LINE__,status)
void _clSafe(const char* file, int line, int status);

cl_double     totalKernelTime;      /**< Time for kernel execution */
cl_int                  width = 2048;      /**< width of the input matrix */
cl_int                 height = 2048;      /**< height of the input matrix */
cl_float               *input;      /**< Input array */
cl_float              *output;      /**< Output Array */
cl_float  *verificationOutput;      /**< Output array for reference implementation */
cl_uint             blockSize = 4;      /**< use local memory of size blockSize x blockSize */
cl_uint				blockRows = 16;
cl_context            context;      /**< CL context */
cl_device_id         *devices;      /**< CL device list */
cl_mem            inputBuffer;      /**< CL memory buffer */
cl_mem           outputBuffer;      /**< CL memory output Buffer */
cl_command_queue commandQueue;      /**< CL command queue */

cl_program            program;      /**< CL program  */
cl_kernel              copyKernel; /**< CL kernel */
cl_kernel              naiveKernel; /**< CL kernel */
cl_kernel              coalescedKernel; /**< CL kernel */
cl_kernel              diagonalKernel; /**< CL kernel */

size_t       maxWorkGroupSize;      /**< Device Specific Information */
cl_uint         maxDimensions;
size_t *     maxWorkItemSizes;
cl_ulong     totalLocalMemory; 
cl_ulong      usedLocalMemory; 
cl_ulong availableLocalMemory; 
cl_ulong    neededLocalMemory;
size_t    kernelWorkGroupSize;    /**< Group Size returned by kernel */
int                iterations = 1;    /**< Number of iterations for kernel execution */
int nreps = 1;
streamsdk::SDKCommon* sampleCommon;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::string convertToString(const char *filename)
{
	size_t size;
	char*  str;
	std::string s;

	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if(f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = f.tellg();
		f.seekg(0, std::fstream::beg);

		str = new char[size+1];
		if(!str)
		{
			f.close();
			return NULL;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
	
		s = str;
		
		return s;
	}
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setupMatrixTranspose()
{
	// Allocate host memory
    int inputSizeBytes = width * height * sizeof(cl_float);
    input = (cl_float *) malloc(inputSizeBytes);
    output = (cl_float *) malloc(inputSizeBytes);
    verificationOutput = (cl_float *) malloc(inputSizeBytes);

	// Init input matrix.
	for(int i = 0; i < width * height; i++) input[i] = i;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setupCL()
{
    cl_int status = 0;
    size_t deviceListSize;

    cl_device_type dType;
    
    dType = CL_DEVICE_TYPE_GPU;

    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */
    cl_uint numPlatforms;
    cl_platform_id platform = NULL;
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
	clSafe(status);

	if (0 < numPlatforms) 
    {
        cl_platform_id* platforms = new cl_platform_id[numPlatforms];
        status = clGetPlatformIDs(numPlatforms, platforms, NULL);
		clSafe(status);

		for (unsigned i = 0; i < numPlatforms; ++i) 
        {
            char pbuf[100];
            status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(pbuf), pbuf, NULL);
			clSafe(status);

            platform = platforms[i];
            if (!strcmp(pbuf, "Advanced Micro Devices, Inc.")) break;
        }
        delete[] platforms;
    }

    /*
     * If we could find our platform, use it. Otherwise pass a NULL and get whatever the
     * implementation thinks we should be using.
     */
    cl_context_properties cps[3] = 
    {
        CL_CONTEXT_PLATFORM, 
        (cl_context_properties)platform, 
        0
    };
    /* Use NULL for backward compatibility */
    cl_context_properties* cprops = (NULL == platform) ? NULL : cps;

    context = clCreateContextFromType(cprops, dType, NULL, NULL, &status);
	clSafe(status);

    /* First, get the size of device list data */
    status = clGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL, &deviceListSize);
	clSafe(status);

    /* Now allocate memory for device list based on the size we got earlier */
    devices = (cl_device_id *)malloc(deviceListSize);

    /* Now, get the device list data */
    status = clGetContextInfo(context, CL_CONTEXT_DEVICES, deviceListSize, devices, NULL);
	clSafe(status);

    /* Get Device specific Information */
    status = clGetDeviceInfo(devices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), (void *)&maxWorkGroupSize, NULL);
	clSafe(status);

    status = clGetDeviceInfo(devices[0], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), (void *)&maxDimensions, NULL);
	clSafe(status);

    maxWorkItemSizes = (size_t *)malloc(maxDimensions*sizeof(size_t));
    
    status = clGetDeviceInfo(devices[0], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t)*maxDimensions, (void *)maxWorkItemSizes, NULL);
	clSafe(status);

    status = clGetDeviceInfo(devices[0], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), (void *)&totalLocalMemory, NULL);
	clSafe(status);

    cl_command_queue_properties prop = 0;
    prop |= CL_QUEUE_PROFILING_ENABLE;

    commandQueue = clCreateCommandQueue(context, devices[0], prop, &status);
	clSafe(status);

    inputBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_float) * width * height, input, &status);
	clSafe(status);

    outputBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_float) * width * height, output, &status);
	clSafe(status);

    const char * source = strdup(convertToString("./MatrixTranspose_Kernels.cl").c_str());
    size_t sourceSize[] = { strlen(source) };
    program = clCreateProgramWithSource(context, 1, &source, sourceSize, &status);
	clSafe(status);

	free((void*)source);
    
    /* create a cl program executable for all the devices specified */
    status = clBuildProgram(program, 1, devices, NULL, NULL, NULL);
	clSafe(status);

    /* get a kernel object handle for a kernel with the given name */
    copyKernel = clCreateKernel(program, "copy", &status);
	clSafe(status);

    /* get a kernel object handle for a kernel with the given name */
    naiveKernel = clCreateKernel(program, "naiveTranspose", &status);
	clSafe(status);

    coalescedKernel = clCreateKernel(program, "coalescedTranspose", &status);
	clSafe(status);

    diagonalKernel = clCreateKernel(program, "diagonalTranspose", &status);
	clSafe(status);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void runCLKernels(cl_kernel kernel, int nreps)
{
    cl_int   status;
    cl_event events[2];

    size_t globalThreads[2]= {width, height};
    size_t localThreads[2] = {blockSize, blockSize};

    /*** Set appropriate arguments to the kernel ***/
    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&outputBuffer);
	clSafe(status);

    status = clSetKernelArg(kernel, 1, sizeof(cl_mem),  (void *)&inputBuffer);
	clSafe(status);

    status = clSetKernelArg(kernel, 2, sizeof(cl_float)*blockSize*blockSize, NULL);
	clSafe(status);
   
    status = clSetKernelArg(kernel, 3, sizeof(cl_int), (void*)&width);
	clSafe(status);
   
    status = clSetKernelArg(kernel, 4, sizeof(cl_int), (void*)&height);
	clSafe(status);

    status = clSetKernelArg(kernel, 5, sizeof(cl_int), (void*)&blockSize);
	clSafe(status);
  
    status = clSetKernelArg(kernel, 6, sizeof(cl_int), (void*)&blockRows);
	clSafe(status);

    status = clSetKernelArg(kernel, 7, sizeof(cl_int), (void*)&nreps);
	clSafe(status);

    /* 
     * Enqueue a kernel run call.
     */
    status = clEnqueueNDRangeKernel(
				commandQueue, kernel, 
				2, NULL, globalThreads, localThreads, 0, NULL, &events[0]);
	clSafe(status);
    
    /* wait for the kernel call to finish execution */
    status = clWaitForEvents(1, &events[0]);
	clSafe(status);

    /* Enqueue readBuffer*/
    status = clEnqueueReadBuffer(
				commandQueue, outputBuffer, 
				CL_TRUE, 0, width * height * sizeof(cl_float), output,
                0,
                NULL,
                &events[1]);
	clSafe(status);
    
    /* Wait for the read buffer to finish execution */
    status = clWaitForEvents(1, &events[1]);
	clSafe(status);
    
    clReleaseEvent(events[1]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void matrixTransposeCPUReference(cl_float* output, cl_float* input, const cl_uint width, const cl_uint height)
{
    for(cl_uint j=0; j < height; j++)
    {
        for(cl_uint i=0; i < width; i++)
        {
            output[i*height + j] = input[j*width + i];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup()
{  
	sampleCommon = new streamsdk::SDKCommon();

    /* width should be multiples of blockSize */
    if(width%blockSize !=0)
    {
        width = (width/blockSize + 1)*blockSize;
    }

    height = width;
    
    setupMatrixTranspose();

    setupCL();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float run(cl_kernel kernel)
{
	runCLKernels(kernel, nreps);

    int timer = sampleCommon->createTimer();
    sampleCommon->resetTimer(timer);
    sampleCommon->startTimer(timer);   

    //printf("Executing kernel for %d iterations\n", iterations);
    //printf("-------------------------------------------\n");

    for(int i = 0; i < iterations; i++)
    {
        /* Arguments are set and execution call is enqueued on command buffer */
		runCLKernels(kernel, nreps);
    }

    sampleCommon->stopTimer(timer);
    totalKernelTime = (double)(sampleCommon->readTimer(timer)) / iterations;

	float bandwidth = (width * height * 2) / totalKernelTime / (1000 * 1000 * 1000) * nreps;
	return bandwidth;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void verifyResults()
{
	int refTimer = sampleCommon->createTimer();
	matrixTransposeCPUReference(verificationOutput, input, width, height);

	/* compare the results and see if they match */
	if(sampleCommon->compare(output, verificationOutput, width*height))
	{
		std::cout<<"Passed!\n";
	}
	else
	{
		std::cout<<"Failed\n";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void cleanup()
{
    /* Releases OpenCL resources (Context, Memory etc.) */
    cl_int status;

    clReleaseKernel(naiveKernel);
    clReleaseKernel(copyKernel);
    clReleaseKernel(coalescedKernel);
    clReleaseKernel(diagonalKernel);
    clReleaseProgram(program);
    clReleaseMemObject(inputBuffer);
    clReleaseMemObject(outputBuffer);
    clReleaseCommandQueue(commandQueue);
    clReleaseContext(context);

	/* release program resources (input memory etc.) */
    free(input);
    free(output);
    free(verificationOutput);
    free(devices);
    free(maxWorkItemSizes);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	char str[512];
	FILE* out = fopen("./results.csv", "w");
	fprintf(out, "WIDTH, HEIGHT, BLOCK_WIDTH, BLOCK_HEIGHT, LOOPS_OUT_KERNEL, LOOPS_IN_KERNEL, COPY_BANDWIDTH, NAIVE_BANDWIDTH, COALESCED_BANDWIDTH, DIAGONAL_BANDWIDTH\n");

	iterations = 10;
	nreps = 100;

	int sz = 1024;

	//for(int sz = 64; sz <= 2048; sz *= 2)
	{
		width = sz;
		height = sz;

		setup();

		blockSize = 4;

		//for(blockSize = 1; blockSize <= 4; blockSize *=2)
		//{
			float copyBw = run(copyKernel);
			float naiveBw = run(naiveKernel);
			float coalescedBw = run(coalescedKernel);
			float diagonalBw = run(diagonalKernel);

			sprintf(str, "%d, %d, %d, %d, %d, %d, %f, %f, %f, %f", width, height, blockSize, blockSize, iterations, nreps, copyBw, naiveBw, coalescedBw, diagonalBw);
			puts(str);
			strcat(str, "\n");
			fputs(str, out);
		//}
		cleanup();
	}

	//verifyResults();
close(out);
}

#define HANDLE_STATUS(id) case id: { printf(#id"\n"); break; }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _clSafe(const char* file, int line, int status)
{
	if(status == CL_SUCCESS) return;
	
	printf("At %s:%d ", file, line);
	
	switch(status)
	{
		HANDLE_STATUS(CL_SUCCESS                                  )
		HANDLE_STATUS(CL_DEVICE_NOT_FOUND                         )
		HANDLE_STATUS(CL_DEVICE_NOT_AVAILABLE                     )
		HANDLE_STATUS(CL_COMPILER_NOT_AVAILABLE                   )
		HANDLE_STATUS(CL_MEM_OBJECT_ALLOCATION_FAILURE            )
		HANDLE_STATUS(CL_OUT_OF_RESOURCES                         )
		HANDLE_STATUS(CL_OUT_OF_HOST_MEMORY                       )
		HANDLE_STATUS(CL_PROFILING_INFO_NOT_AVAILABLE             )
		HANDLE_STATUS(CL_MEM_COPY_OVERLAP                         )
		HANDLE_STATUS(CL_IMAGE_FORMAT_MISMATCH                    )
		HANDLE_STATUS(CL_IMAGE_FORMAT_NOT_SUPPORTED               )
		HANDLE_STATUS(CL_BUILD_PROGRAM_FAILURE                    )
		HANDLE_STATUS(CL_MAP_FAILURE                              )
		HANDLE_STATUS(CL_INVALID_VALUE                            )
		HANDLE_STATUS(CL_INVALID_DEVICE_TYPE                      )
		HANDLE_STATUS(CL_INVALID_PLATFORM                         )
		HANDLE_STATUS(CL_INVALID_DEVICE                           )
		HANDLE_STATUS(CL_INVALID_CONTEXT                          )
		HANDLE_STATUS(CL_INVALID_QUEUE_PROPERTIES                 )
		HANDLE_STATUS(CL_INVALID_COMMAND_QUEUE                    )
		HANDLE_STATUS(CL_INVALID_HOST_PTR                         )
		HANDLE_STATUS(CL_INVALID_MEM_OBJECT                       )
		HANDLE_STATUS(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR          )
		HANDLE_STATUS(CL_INVALID_IMAGE_SIZE                       )
		HANDLE_STATUS(CL_INVALID_SAMPLER                          )
		HANDLE_STATUS(CL_INVALID_BINARY                           )
		HANDLE_STATUS(CL_INVALID_BUILD_OPTIONS                    )
		HANDLE_STATUS(CL_INVALID_PROGRAM                          )
		HANDLE_STATUS(CL_INVALID_PROGRAM_EXECUTABLE               )
		HANDLE_STATUS(CL_INVALID_KERNEL_NAME                      )
		HANDLE_STATUS(CL_INVALID_KERNEL_DEFINITION                )
		HANDLE_STATUS(CL_INVALID_KERNEL                           )
		HANDLE_STATUS(CL_INVALID_ARG_INDEX                        )
		HANDLE_STATUS(CL_INVALID_ARG_VALUE                        )
		HANDLE_STATUS(CL_INVALID_ARG_SIZE                         )
		HANDLE_STATUS(CL_INVALID_KERNEL_ARGS                      )
		HANDLE_STATUS(CL_INVALID_WORK_DIMENSION                   )
		HANDLE_STATUS(CL_INVALID_WORK_GROUP_SIZE                  )
		HANDLE_STATUS(CL_INVALID_WORK_ITEM_SIZE                   )
		HANDLE_STATUS(CL_INVALID_GLOBAL_OFFSET                    )
		HANDLE_STATUS(CL_INVALID_EVENT_WAIT_LIST                  )
		HANDLE_STATUS(CL_INVALID_EVENT                            )
		HANDLE_STATUS(CL_INVALID_OPERATION                        )
		HANDLE_STATUS(CL_INVALID_GL_OBJECT                        )
		HANDLE_STATUS(CL_INVALID_BUFFER_SIZE                      )
		HANDLE_STATUS(CL_INVALID_MIP_LEVEL                        )
		HANDLE_STATUS(CL_INVALID_GLOBAL_WORK_SIZE                 )
	}
	
	exit(1);
}
